package com.bonobows.shadowhunters.server.model;

import java.util.Random;

/**
 * Created by Milihhard on 04/10/2017.
 */
public class De {

  private int faces;
  //attribut d'association : Jeu (peut être vide ou en avoir plusieurs).

  public De() {
    this.faces = 6;
  }

  public De(int faces) {
    this.faces = faces;
  }

  public int getFaces() {
    return faces;
  }

  public int lancerDe() {
    Random rand = new Random();
    return rand.nextInt(this.faces) + 1;
  }
}
