package com.bonobows.shadowhunters.server.exception;

public class MauvaisNombreJoueurException extends Exception {
  public MauvaisNombreJoueurException() {
    super("le nombre de joueurs n'est pas bon");
  }
}
