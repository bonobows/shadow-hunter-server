package com.bonobows.shadowhunters.server.model.socket;

import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.common.model.commande.Commande;
import com.bonobows.shadowhunters.server.model.command.CommandeManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Created by Milihhard on 04/10/2017.
 */
public class SocketManager {

  private ArrayList<SocketJoueur> sockets;

  private List<CyclicBarrier> cyclicBarriers;

  private CommandeManager commandeManager;

  public SocketManager() {
    this.sockets = new ArrayList<>();
    this.cyclicBarriers = new ArrayList<>();
    for (int i = 0; i < sockets.size(); i++) {
      this.cyclicBarriers.add(new CyclicBarrier(1));
    }
  }

  public SocketManager(ArrayList<SocketJoueur> sockets) {
    this.sockets = sockets;
    this.cyclicBarriers = new ArrayList<>();
    for (int i = 0; i < sockets.size(); i++) {
      this.cyclicBarriers.add(new CyclicBarrier(1));
    }
  }

  public void setCommandeManager(CommandeManager commandeManager) {
    this.commandeManager = commandeManager;
  }

  public void sendToEveryBody(Commande commande) {
    for (SocketJoueur s : sockets) {
      s.sendMessage(commande);
    }
  }

  public void sendToPlayer(Commande commande, int joueur) {
    sockets.get(joueur - 1).sendMessage(commande);
  }

  public void sendToOtherPlayers(Commande commande, int joueur) {
    for (int i = 1; i <= sockets.size(); i++) {
      if (i != joueur) {
        sockets.get(i - 1).sendMessage(commande);
      }
    }
  }

  public void sendCommandToAPlayerWhichIsDifferent(
      Commande commandeJoueur, Commande commandeAutresJoueurs, int joueur) {
    this.sendToPlayer(commandeJoueur, joueur);
    this.sendToOtherPlayers(commandeAutresJoueurs, joueur);
  }

  public void addSocketJoueur(SocketJoueur s) {
    this.sockets.add(s);
    this.cyclicBarriers.add(new CyclicBarrier(1));
  }

  public void receiveMessage(String message, int joueur) {
    try {
      CyclicBarrier cyclicBarrier = cyclicBarriers.get(joueur - 1);
      cyclicBarrier.await();
      cyclicBarrier.reset();
    } catch (InterruptedException | BrokenBarrierException e) {
      e.printStackTrace();
    }
    if (commandeManager != null) {
      try {
        Commande commande = Commande.jsonToCommand(message);

        this.commandeManager.interpreterCommande(commande, joueur);
      } catch (IOException e) {
        System.out.println("Le serveur a fait de la merde");
      }
    } else {
      System.out.println("joueur " + (joueur) + " : " + message);
      this.sendToOtherPlayers(
          CommandeFactory.logInfo("joueur " + (joueur) + " : " + message), joueur);
    }
  }

  public void waitResponse(int joueur) {
    try {
      cyclicBarriers.get(joueur - 1).await();
    } catch (InterruptedException | BrokenBarrierException e) {
      e.printStackTrace();
    }
  }

  public InputStream getPlayerInputStream(int joueur) {
    try {
      return this.sockets.get(joueur - 1).getSocket().getInputStream();
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  public void close(int joueur) throws IOException {
    sockets.get(joueur - 1).getSocket().close();
  }
}
