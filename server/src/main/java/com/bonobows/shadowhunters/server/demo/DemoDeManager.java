package com.bonobows.shadowhunters.server.demo;

import com.bonobows.shadowhunters.server.controller.DeManager;

public class DemoDeManager extends DeManager {

  private int[] lannces6 = {4, 2, 1, 6, 6, 3, 4};
  private int[] lannces4 = {2, 3, 1, 4, 3, 2, 1};
  private int index = 0;

  @Override
  public int[] lancerDesDetail() {
    int[] detail = new int[3];
    detail[0] = lannces6[index];
    detail[1] = lannces4[index];
    detail[2] = detail[0] + detail[1];
    index = (index + 1) % lannces4.length;
    return detail;
  }

  @Override
  public int lancerDe6MoinsD4(int modificateur) {
    int resultat = 0;
    while (resultat <= 0) {
      resultat = Math.abs(this.lancerD6() - this.lancerD4()) + modificateur;
    }
    return resultat;
  }
}
