package com.bonobows.shadowhunters.server;

/*
 * This Java source file was generated by the Gradle 'init' task.
 */
public class App {

  public static void main(String[] args) {
    System.out.println(new App().getGreeting());
    Server serv = new Server();
    serv.start();
  }

  public String getGreeting() {
    return "Hello world.";
  }
}
