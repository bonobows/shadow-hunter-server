package com.bonobows.shadowhunters.server.controller.lieu;

import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.CarteLumiereTenebre;
import com.bonobows.shadowhunters.common.model.CarteVision;
import com.bonobows.shadowhunters.common.model.EffetVision;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import com.bonobows.shadowhunters.server.controller.CarteController;
import com.bonobows.shadowhunters.server.controller.CarteVisionManager;
import com.bonobows.shadowhunters.server.controller.Controller;
import com.bonobows.shadowhunters.server.controller.JeuController;
import com.bonobows.shadowhunters.server.model.Jeu;
import com.bonobows.shadowhunters.server.model.socket.SocketManager;

import java.util.ArrayList;
import java.util.List;

public class LieuController extends Controller {

  private CarteController carteController;
  private JeuController jeuController;

  public LieuController(Jeu jeu, SocketManager socketManager) {
    super(jeu, socketManager);
  }

  public void setCarteController(CarteController carteController) {
    this.carteController = carteController;
    this.carteController.setLieuController(this);
  }

  public void setJeuController(JeuController jeuController) {
    this.jeuController = jeuController;
  }

  public void stepLieu() {
    if (jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Boussole mystique")) {
      stepLieuAvecBoussole();
    } else {
      int[] lancerDe;
      do {
        lancerDe = jeu.des.lancerDesDetail();
        // le joueur ne peut pas se déplacer au meme endroit
      } while (this.jeu.terrain.getLieuByLancerDe(lancerDe[2]) != null
          && this.jeu.terrain
          .getLieuByLancerDe(lancerDe[2])
          .equals(this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur())));
      // pour les test (avoir que des 7 aux lancés)
      //lancerDe[2] = 7;
      this.socketManager.sendToEveryBody(CommandeFactory.de(lancerDe[0], lancerDe[1], lancerDe[2]));
      if (lancerDe[2] == 7) {
        socketManager.sendToPlayer(CommandeFactory.lieuChoix(), jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
      } else {
        stepLieuResponse(jeu.terrain.getLieuByLancerDe(lancerDe[2]));
      }
    }
  }

  private void stepLieuAvecBoussole() {
    int[] lancerDe;
    int[] lancerDe2;
    do {
      lancerDe = jeu.des.lancerDesDetail();
      // le joueur ne peut pas se déplacer au meme endroit
    } while (this.jeu.terrain.getLieuByLancerDe(lancerDe[2]) != null
        && this.jeu.terrain
        .getLieuByLancerDe(lancerDe[2])
        .equals(this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur())));
    do {
      do {
        lancerDe2 = jeu.des.lancerDesDetail();
        // le joueur ne peut pas se déplacer au meme endroit
      } while (this.jeu.terrain.getLieuByLancerDe(lancerDe2[2]) != null
          && this.jeu.terrain
          .getLieuByLancerDe(lancerDe2[2])
          .equals(this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur())));
    } while (lancerDe[2] == lancerDe2[2]);

    this.socketManager.sendToEveryBody(CommandeFactory.resBoussole(lancerDe, lancerDe2));
    if (lancerDe[2] == 7 || lancerDe2[2] == 7) {
      socketManager.sendToOtherPlayers(
          CommandeFactory.logInfo("Sa boussole mystique lui a " + "généré un 7 !"), jeu
              .getTourJoueur() + 1);
      socketManager.sendToPlayer(
          CommandeFactory.logInfo(
              "Votre boussole mystique vous a généré "
                  + "au moins un 7 (pas besoin de choisir entre ses résultats)."),
          jeu.getTourJoueur() + 1);
      socketManager.sendToPlayer(CommandeFactory.lieuChoix(), jeu.getTourJoueur() + 1);
      this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
    } else {
      // ask
      socketManager.sendToPlayer(
          CommandeFactory.resDeChoix(lancerDe[2], lancerDe2[2]),
          jeu.getTourJoueur() + 1);
    }
  }

  public void stepResBoussoleChoisi(int choix) {
    stepLieuResponse(jeu.terrain.getLieuByLancerDe(choix));
  }

  public void stepLieuChoix(NomLieu typeLieu) {
    System.out.println(jeu.terrain.getLieuTypeLieu(typeLieu));
    stepLieuResponse(jeu.terrain.getLieuTypeLieu(typeLieu));
  }

  public void stepLieuResponse(Lieu l) {
    if (l.equals(this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur()))) {
      socketManager.sendToPlayer(CommandeFactory.lieuChoix(), jeu.getTourJoueur() + 1);
      this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
    } else {
      jeu.terrain.moveJoueur(getJoueurTourJoueur(), l);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(getJoueurTourJoueur()));
      this.socketManager.sendToEveryBody(CommandeFactory.move(
          jeu.getTourJoueur() + 1,
          this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur())));
      System.out.println(
          getPersonnage(jeu.getTourJoueur() + 1).getNom()
              + " (Joueur"
              + jeu.getTourJoueur() + 1
              + ") est sur le lieu "
              + this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur()).getNom());

      stepEffetLieu(this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur()));
    }
  }

  private void stepEffetLieu(Lieu l) {
    switch (l.getNom()) {
      case CIMETIERE: // piocher une carte tenebre
        jeu.derniereCarteTiree = jeu.cartesTenebres.piocher();
        if (jeu.derniereCarteTiree instanceof Equipement) {
          getJoueurTourJoueur().addEquipement((Equipement) jeu.derniereCarteTiree);
          this.socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(getJoueurTourJoueur()));
        }
        this.socketManager.sendToPlayer(
            CommandeFactory.effetLieu(l.getNom(), jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        this.socketManager.sendToOtherPlayers(
            CommandeFactory.showCardLt(jeu.derniereCarteTiree), jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      case FORET: // 1 soin ou 2 Blessures
        this.socketManager.sendToPlayer(CommandeFactory.effetLieu(l.getNom()),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      case MONASTERE: // piocher une carte lumiere
        jeu.derniereCarteTiree = jeu.cartesLumieres.piocher();
        if (jeu.derniereCarteTiree instanceof Equipement) {
          getJoueurTourJoueur().addEquipement((Equipement) jeu.derniereCarteTiree);
          this.socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(getJoueurTourJoueur()));
        }
        this.socketManager.sendToPlayer(
            CommandeFactory.effetLieu(l.getNom(), jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        this.socketManager.sendToOtherPlayers(
            CommandeFactory.showCardLt(jeu.derniereCarteTiree), jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      case SANCTUAIRE: // voler une eqt
        this.socketManager.sendToPlayer(CommandeFactory.effetLieu(l.getNom()),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      case ANTRE_HERMITE: // piocher une carte vision
        jeu.derniereCarteTiree = jeu.cartesVisions.piocher();
        this.socketManager.sendToPlayer(
            CommandeFactory.effetLieu(l.getNom(), jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      case PORTE_OUTREMONDE: // piocher une carte vision ou tenebre ou lumiere
        this.socketManager.sendToPlayer(CommandeFactory.effetLieu(l.getNom()),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      default:
        break;
    }
  }

  public void stepEffetLieuResponse(NomLieu typeLieu, int choix) {
    switch (typeLieu) {
      case CIMETIERE:
        stepEffetLieuCimetiere();
        break;
      case FORET:
        stepEffetLieuForet(choix);
        break;
      case MONASTERE:
        // ah bah c'est la même routine en fait,
        // cimetiere et sanctuaire, vu que le distinguo
        // entre Lumiere et tenebre se fait dans la pioche source de la derniereCarteTiree
        stepEffetLieuCimetiere();
        break;
      case SANCTUAIRE:
        stepEffetLieuSanctuaire();
        break;
      case ANTRE_HERMITE:
        stepEffetLieuaAntreHermite(choix);
        break;
      case PORTE_OUTREMONDE:
        stepEffetLieuPorteOutremonde(choix);
        break;
      default:
        break;
    }
  }

  private void stepEffetLieuForet(int choix) {
    if (choix < 0) { //Soin
      Joueur soigne = jeu.joueurs.get(Math.abs(choix) - 1);
      soigne.retirerBlessures(1);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(soigne));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Le joueur "
                  + (jeu.getTourJoueur() + 1)
                  + " a enlevé 1 Blessure au joueur "
                  + Math.abs(choix)));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Le joueur "
                  + Math.abs(choix)
                  + " a"
                  + soigne.getNbBlessures()
                  + " Blessure(s)."));
    } else if (choix > 0) { //Degats
      Joueur attaque = jeu.joueurs.get(Math.abs(choix) - 1);
      if (attaque.possedeEqtdeNom("Broche de chance !")) {
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                "Le joueur "
                    + (jeu.getTourJoueur() + 1)
                    + " a voulu utiliser la foret hantée pour blesser le joueur "
                    + Math.abs(choix)
                    + " mais il possède la broche de chance !"));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                "Le joueur "
                    + Math.abs(choix)
                    + " a "
                    + attaque.getNbBlessures()
                    + " Blessures."));
      } else {
        int def = attaque.getModificateurDefense();
        attaque.ajouterBlessures(2 - def);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(attaque));
        if (def > 2) { // valeur d'affichage cohérente
          def = 2;
        }
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                "Le joueur "
                    + (jeu.getTourJoueur() + 1)
                    + " a ajouté +2 -"
                    + def
                    + " Blessures au joueur "
                    + choix));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                "J" + choix + " a " + attaque.getNbBlessures() + " Blessures."));
        if (attaque.isDead()) {
          jeuController.stepMort(attaque);
        }
      }
    }
    stepFinEffetLieu();
  }

  public void stepEffetLieuaAntreHermite(int choix) {
    for (int i = 1; i <= jeu.joueurs.size(); i++) {
      if (i == choix) {
        CarteVision derniereCarteVision = (CarteVision) jeu.derniereCarteTiree;
        boolean hasEquipement = jeu.joueurs.get(choix - 1).getEqts().size() > 0;
        List<ActionVision> actionVisions =
            CarteVisionManager.getActionEffet(
                derniereCarteVision, jeu.joueurs.get(choix - 1), hasEquipement);
        if (actionVisions.contains(ActionVision.DONNER_EQUIPEMENT)) {
          this.socketManager.sendToPlayer(
              CommandeFactory.vision(
                  derniereCarteVision,
                  jeu.getTourJoueur() + 1,
                  actionVisions,
                  jeu.joueurs.get(choix - 1).getEqts()),
              i);
        } else {
          this.socketManager.sendToPlayer(
              CommandeFactory.vision(
                  derniereCarteVision,
                  jeu.getTourJoueur() + 1,
                  actionVisions),
              i);
        }
      } else if (i != jeu.getTourJoueur() + 1) {
        this.socketManager.sendToPlayer(
            CommandeFactory.logInfo(
                "Le joueur "
                    + (jeu.getTourJoueur() + 1)
                    + " a donné une carte vision au Joueur "
                    + choix),
            i);
      }
    }
    //    this.stepFinEffetLieu();
  }

  public void stepEffetLieuSanctuaire() {
    boolean volerEstPossible = false;
    int nbPossesseurs = 0;
    ArrayList<Integer> possibilites = new ArrayList<>();

    for (int j = 0; j < jeu.joueurs.size(); j++) {
      // on cherche à savoir si au moins un joueur possède au moins 1 eqt
      if (j != jeu.getTourJoueur() && jeu.joueurs.get(j).getEqts() != null) {
        if (!jeu.joueurs.get(j).getEqts().isEmpty()) {
          volerEstPossible = true;
          nbPossesseurs++;
        }
      }
    }

    if (volerEstPossible) {
      if (nbPossesseurs == 1) {
        // Pas besoin de demander de choisir un joueur si un seul joueur possède de quoi voler
        int uniquePosseur = -1;
        for (int j = 0; j < jeu.joueurs.size(); j++) {
          if (j != jeu.getTourJoueur() && jeu.joueurs.get(j).getEqts() != null) {
            if (!jeu.joueurs.get(j).getEqts().isEmpty()) {
              uniquePosseur = j;
              break;
            }
          }
        }
        jeuController.stepCibleVolChoisie(uniquePosseur);
      } else { // On envoi une liste des identifiants de joueurs possiblement volables
        for (int j = 0; j < jeu.joueurs.size(); j++) {
          if (j != jeu.getTourJoueur()
              && jeu.joueurs.get(j).getEqts() != null
              && !jeu.joueurs.get(j).getEqts().isEmpty()) {
            possibilites.add(j + 1);
          }
        }
        this.socketManager.sendToPlayer(CommandeFactory.cibleVolChoix(possibilites),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
      }
    } else {
      this.socketManager.sendToPlayer(
          CommandeFactory.logInfo(
              "Dommage aucun joueur ne possède le moindre équipement ! "
                  + "(impossible de voler un équipement.)"),
          jeu.getTourJoueur() + 1);
      this.stepFinEffetLieu();
    }
  }

  public void stepAntreHermiteChoix(ActionVision choix, int joueur, Equipement equipementChoisi) {
    CarteVision derniereCarteVision = (CarteVision) jeu.derniereCarteTiree;
    Joueur joueurDeLaCarte = jeu.joueurs.get(joueur - 1);
    switch (choix) {
      case SE_SOIGNER:
        joueurDeLaCarte.retirerBlessures(1);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(joueurDeLaCarte));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo("Le joueur " + joueur + " s'est soigné"));
        break;
      case PRENDRE_DEGAT:
        if (derniereCarteVision.getTypeEffet().equals(EffetVision.DEUX_BLESSURES)) {
          joueurDeLaCarte.ajouterBlessures(2);
          socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(joueurDeLaCarte));
          socketManager.sendToEveryBody(
              CommandeFactory.logInfo("Le joueur " + joueur + " a pris 2 Blessures"));
        } else {
          joueurDeLaCarte.ajouterBlessures(1);
          socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(joueurDeLaCarte));
          socketManager.sendToEveryBody(
              CommandeFactory.logInfo("Le joueur " + joueur + " a pris 1 Blessure"));
        }
        if (joueurDeLaCarte.isDead()) {
          jeuController.stepMort(joueurDeLaCarte);
        }
        break;
      case MONTRER_CARTE:
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                "Le joueur " + joueur + " montré sa carte au joueur " + (jeu.getTourJoueur() + 1)));
        socketManager.sendToPlayer(
            CommandeFactory.personnageVision(joueurDeLaCarte.getPersonnage()),
            jeu.getTourJoueur() + 1);
        break;
      case DONNER_EQUIPEMENT:
        jeu.joueurs.get(joueur - 1).getEqts().remove(equipementChoisi);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(jeu.joueurs.get(joueur - 1)));
        jeu.joueurs.get(getTourJoueur() - 1).addEquipement(equipementChoisi);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(jeu.joueurs.get(getTourJoueur() - 1)));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                "Le joueur "
                    + joueur
                    + " donne un équipement au joueur "
                    + (jeu.getTourJoueur() + 1)
                    + " ("
                    + equipementChoisi.getNom()
                    + ")"));
        break;
      case RIEN:
      default:
        socketManager.sendToEveryBody(CommandeFactory.logInfo("Il ne se passe rien"));
    }
    this.stepFinEffetLieu();
  }

  public void stepEffetLieuCimetiere() {

    switch (((CarteLumiereTenebre) jeu.derniereCarteTiree).getTypeOfCard()) {
      case "Equipement":
        this.socketManager.sendToOtherPlayers(
            CommandeFactory.logInfo(
                "Le joueur "
                    + (jeu.getTourJoueur() + 1)
                    + " a "
                    + "obtenu un équipement : "
                    + jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        // il ne  manquerait pas le fait de prévenir le joueur qu'il a eu un eqt ?
        socketManager.sendToEveryBody(CommandeFactory.updateJoueur(getJoueurTourJoueur()));
        this.stepFinEffetLieu();
        break;
      case "Immediat":
        this.socketManager.sendToOtherPlayers(
            CommandeFactory.logInfo(
                "Le joueur "
                    + (jeu.getTourJoueur() + 1)
                    + " a "
                    + "obtenu une carte immédiate : "
                    + jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        if (((CarteLumiereTenebre) jeu.derniereCarteTiree).isLumiere()) {
          if (!carteController.manageImmediatLumiere()) {
            this.stepFinEffetLieu();
          }
        } else if (!carteController.manageImmediatTenebre()) {
          this.stepFinEffetLieu();
        }
        break;
      default:
        System.out.println(
            "!! erreur : la carte tirée n'est pas detectée comme l'un des deux "
                + "types de cartes (immediat ou eqt)"
                + "("
                + jeu.derniereCarteTiree.getClass().toString()
                + ")");
        this.stepFinEffetLieu();
        break;
    }
  }

  public void stepEffetLieuPorteOutremonde(int choix) {
    switch (choix) {
      case 0:
        // le joueur a choisi de ne pas  utiliser l'effet du lieu (c'est bien là qu'il faut le
        // traiter ?)
        this.stepFinEffetLieu();
        break;
      case 1:
        // le joueur a choisi de piocher une carte Vision
        jeu.derniereCarteTiree = jeu.cartesVisions.piocher();
        System.out.println(jeu.derniereCarteTiree);
        this.socketManager.sendToPlayer(
            CommandeFactory.effetLieu(NomLieu.ANTRE_HERMITE, jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      case 2:
        // le joueur a choisi de piocher une carte Lumiere
        jeu.derniereCarteTiree = jeu.cartesLumieres.piocher();
        if (jeu.derniereCarteTiree instanceof Equipement) {
          jeu.joueurs.get(jeu.getTourJoueur()).addEquipement((Equipement) jeu.derniereCarteTiree);

        }
        this.socketManager.sendToPlayer(
            CommandeFactory.effetLieu(NomLieu.MONASTERE, jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      case 3:
        // le joueur a choisi de piocher une carte Tenebre
        jeu.derniereCarteTiree = jeu.cartesTenebres.piocher();
        if (jeu.derniereCarteTiree instanceof Equipement) {
          jeu.joueurs.get(jeu.getTourJoueur()).addEquipement((Equipement) jeu.derniereCarteTiree);
          socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(getJoueurTourJoueur()));
        }
        this.socketManager.sendToPlayer(
            CommandeFactory.effetLieu(NomLieu.CIMETIERE, jeu.derniereCarteTiree),
            jeu.getTourJoueur() + 1);
        this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
        break;
      default:
        System.out.println("!! erreur, le choix de pioche reçu (coté serveur) != 0, 1, 2, 3");
        this.stepFinEffetLieu();
    }
  }

  public void stepFinEffetLieu() {
    jeuController.stepAttac();
  }
}
