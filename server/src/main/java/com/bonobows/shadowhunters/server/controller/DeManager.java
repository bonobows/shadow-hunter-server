package com.bonobows.shadowhunters.server.controller;

import com.bonobows.shadowhunters.server.model.De;

public class DeManager {
  private De de4;
  private De de6;

  public DeManager() {
    this.de4 = new De(4);
    this.de6 = new De();
  }

  public int lancerD4() {
    return this.de4.lancerDe();
  }

  public int lancerD6() {
    return this.de6.lancerDe();
  }

  public int lancerDes() {
    return this.lancerD4() + lancerD6();
  }

  public int[] lancerDesDetail() {
    int[] detail = new int[3];
    detail[0] = this.lancerD6();
    detail[1] = this.lancerD4();
    detail[2] = detail[0] + detail[1];
    return detail;
  }

  public int lancerDe6MoinsD4(int modificateur) {
    return Math.abs(this.lancerD6() - this.lancerD4()) + modificateur;
  }
}
