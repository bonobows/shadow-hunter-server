package com.bonobows.shadowhunters.server.model.command;

import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.argument.Argument;
import com.bonobows.shadowhunters.common.model.commande.Commande;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import com.bonobows.shadowhunters.common.tool.JsonTools;
import com.bonobows.shadowhunters.server.controller.JeuController;
import com.bonobows.shadowhunters.server.controller.lieu.LieuController;

import java.io.IOException;
import java.util.Map;

public class CommandeManager {

  private JeuController jeuController;
  private LieuController lieuController;

  public CommandeManager(JeuController jeuController) {
    this.jeuController = jeuController;
    this.lieuController = jeuController.getLieuController();
  }

  public void interpreterCommande(Commande commande, int joueur) {
    switch (commande.getCode()) {
      case LOG:
        interpreterLog(commande);
        break;
      case ATTAC:
        interpreterAttac(commande);
        break;
      case TURN:
        interpreterTurn(joueur);
        break;
      case LIEU_CHOIX:
        interpreterLieuChoix(commande, joueur);
        break;
      case RES_BOUSSOLE_CHOISI:
        interpreterResBoussoleChoisi(commande);
        break;
      case EFFET_LIEU:
        interpreterEffetLieu(commande);
        break;
      case SE_REVELER:
        interpreterSeReveler(commande);
        break;
      case UTILISER_CAPACITE:
        interpreterUtiliserCapacite(commande);
        break;
      case CAPACITE_FRANKLIN:
        interpreterCapaciteFranklin(commande);
        break;
      case CAPACITE_GEORGES:
        interpreterCapaciteGeorges(commande);
        break;
      case CIBLE_VOL:
        interpreterChoixCibleVol(commande);
        break;
      case CAPACITE_LOUP_GAROU:
        interpreterCapaciteLoupGarou(commande);
        break;
      case EQT_CHOISI:
        interpreterChoixEqtAVoler(commande);
        break;
      case VISION:
        interpreterVision(commande, joueur);
        break;
      case CAPACITE_EMI:
        interpreterEmi(commande);
        break;
      case CARTE_IMMEDIATE:
        interpreterCarteImmediate(commande);
        break;
      case CHAT:
        interpreterChat(commande, joueur);
        break;
      case SE_PRESENTER:
        interpreterSePresenter(commande, joueur);
        break;
      case DEMARRER_PARTIE:
        interpreterDemarrerPartie();
        break;
      default:
        System.out.println("Je ne comprends pas la commande");
        System.out.println(commande.getCode());
    }
  }

  private void interpreterDemarrerPartie() {
    jeuController.demarrerPartie();
  }

  private void interpreterEmi(Commande commande) {
    String choix = (String) commande.getArg(Argument.CapaciteEmi.CAPACITE_EMI);
    jeuController.stepCapaciteEmi(choix);
  }

  private void interpreterLog(Commande commande) {
    System.out.println(
        commande.getArg(Argument.Log.LOG_LEVEL).toString()
            + " : "
            + commande.getArg(Argument.Log.LOG_MESSAGE));
  }

  private void interpreterAttac(Commande commande) {
    Map<Argument, Object> args = commande.getArgs();
    // boolean result = true;
    if (args.size() == 1 && args.containsKey(Argument.Attac.ATTAQUE_ATTAQUE)) {
      Integer attaque = (Integer) args.get(Argument.Attac.ATTAQUE_ATTAQUE);
      jeuController.stepAttacResponse(attaque);
    } else {
      jeuController.resolveLastSteps();
    }

  }

  private void interpreterTurn(int joueur) {
    if (joueur == lieuController.getTourJoueur()) {
      lieuController.stepLieu();
    }
  }

  private void interpreterLieuChoix(Commande commande, int joueur) {
    if (joueur == jeuController.getTourJoueur()) {
      try {
        lieuController.stepLieuChoix(
            (NomLieu) JsonTools.toObject(commande, Argument.LieuChoix.LIEU_CHOIX, NomLieu.class));
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private void interpreterResBoussoleChoisi(Commande commande) {
    lieuController.stepResBoussoleChoisi(
        Integer.valueOf(commande.getArg(Argument.ArgDe.DE_CHOIX_RES_BOUSSOLE).toString()));
  }

  private void interpreterEffetLieu(Commande commande) {
    try {
      int choix;
      if (commande.getArg(Argument.EffetLieu.EFFET_LIEU_CHOIX) != null) {
        // dans  les cas des cimetiere et monastere, la reponse n'as  pas de choix.
        choix = (int) commande.getArg(Argument.EffetLieu.EFFET_LIEU_CHOIX);
      } else {
        choix = 0;
      }
      this.lieuController.stepEffetLieuResponse(
          (NomLieu) JsonTools.toObject(commande, Argument.EffetLieu.EFFET_LIEU_TYPE, NomLieu.class),
          choix);
    } catch (IOException e) {
      e.printStackTrace();
      this.lieuController.stepFinEffetLieu();
    }
  }

  private void interpreterSeReveler(Commande commande) {
    this.jeuController.stepSeRevelerOuNon(
        (String) commande.getArgs().get(Argument.SeReveler.SE_REVELER_REPONSE));
  }

  private void interpreterUtiliserCapacite(Commande commande) {
    this.jeuController.stepUtiliserCapacite(
        (String) commande.getArgs().get(Argument.UtiliserCapacite.UTILISER_CAPACITE_REPONSE));
  }

  private void interpreterCapaciteFranklin(Commande commande) {
    this.jeuController.stepCapaciteFranklin(
        (int) commande.getArg(Argument.CapaciteFranklin.CAPACITE_FRANKLIN_CHOIX));
  }

  private void interpreterCapaciteGeorges(Commande commande) {
    this.jeuController.stepCapaciteGeorges(
        (int) commande.getArg(Argument.CapaciteGeorges.CAPACITE_GEORGES_CHOIX));
  }

  private void interpreterChoixCibleVol(Commande commande) {
    this.jeuController.stepCibleVolChoisie(
        Integer.parseInt(commande.getArg(Argument.Sanctuaire.SANCTUAIRE_JOUEUR_CIBLE).toString()));
  }

  private void interpreterChoixEqtAVoler(Commande commande) {
    this.jeuController.stepEqtAVolerChoisi(
        Integer.parseInt(commande.getArg(Argument.Sanctuaire.SANCTUAIRE_EQT_CHOISI).toString()));
  }

  private void interpreterVision(Commande commande, int joueur) {
    try {
      ActionVision actionVision =
          (ActionVision)
              JsonTools.toObject(commande, Argument.Vision.VISION_CHOISI, ActionVision.class);
      System.out.println("choix de la vision : " + actionVision);
      Equipement equipementChoisi = null;
      if (commande.getArg(Argument.Vision.VISION_EQUIPEMENT_CHOISI) != null) {
        equipementChoisi =
            (Equipement)
                JsonTools.toObject(
                    commande, Argument.Vision.VISION_EQUIPEMENT_CHOISI, Equipement.class);
      }
      lieuController.stepAntreHermiteChoix(actionVision, joueur, equipementChoisi);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void interpreterCapaciteLoupGarou(Commande commande) {
    this.jeuController.stepCapaciteLoupGarou(
        (int) commande.getArg(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU_ATTAQUANT),
        (int) commande.getArg(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU),
        (String) commande.getArgs().get(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU_CHOIX));
  }

  private void interpreterCarteImmediate(Commande commande) {
    if (commande.getArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_EQUIPEMENT) != null) {
      jeuController.stepPeauBanane(
          (Integer) commande.getArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_EQUIPEMENT),
          (Integer) commande.getArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_JOUEUR));
    } else if (commande.getArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_JOUEUR) != null) {
      jeuController.stepCarteImmediateChoixJoueur(
          (Integer) commande.getArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_JOUEUR));
    }
  }

  private void interpreterChat(Commande commande, int joueur) {
    jeuController.chat((String) commande.getArg(Argument.Chat.CHAT_MESSAGE), joueur);
  }

  private void interpreterSePresenter(Commande commande, int joueur) {
    this.jeuController.sePresenter(
        (String) commande.getArgs().get(Argument.SePresenter.SE_PRESENTER_PSEUDO),
        joueur);
  }
}
