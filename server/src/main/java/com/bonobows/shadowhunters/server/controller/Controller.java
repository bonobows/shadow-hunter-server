package com.bonobows.shadowhunters.server.controller;

import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.server.model.Jeu;
import com.bonobows.shadowhunters.server.model.socket.SocketManager;

public abstract class Controller {
  protected Jeu jeu;
  protected SocketManager socketManager;

  protected Controller(Jeu jeu, SocketManager socketManager) {
    this.jeu = jeu;
    this.socketManager = socketManager;
  }

  protected Personnage getPersonnage(int index) {
    return this.jeu.joueurs.get(index - 1).getPersonnage();
  }

  public int getTourJoueur() {
    return jeu.getTourJoueur() + 1;
  }

  public Joueur getJoueurTourJoueur() {
    return jeu.joueurs.get(getTourJoueur() - 1);
  }
}
