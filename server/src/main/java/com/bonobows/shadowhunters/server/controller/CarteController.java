package com.bonobows.shadowhunters.server.controller;

import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.common.model.Faction;
import com.bonobows.shadowhunters.common.model.Immediat;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.server.controller.lieu.LieuController;
import com.bonobows.shadowhunters.server.model.Jeu;
import com.bonobows.shadowhunters.server.model.socket.SocketManager;

public class CarteController extends Controller {

  private LieuController lieuController;
  private JeuController jeuController;

  public CarteController(Jeu jeu, SocketManager socketManager) {
    super(jeu, socketManager);
  }

  public void setLieuController(LieuController lieuController) {
    this.lieuController = lieuController;
  }

  public void setJeuController(JeuController jeuController) {
    this.jeuController = jeuController;
  }

  public boolean manageImmediatTenebre() {
    Immediat immediat = (Immediat) jeu.derniereCarteTiree;
    Joueur joueur = this.getJoueurTourJoueur();
    if (immediat.isTenebre()) {
      jeu.cartesTenebres.defausser(immediat);
      switch (immediat.getNom()) {
        case "Succube tentatrice":
          lieuController.stepEffetLieuSanctuaire();
          break;
        case "Chauve-souris vampiré":
          socketManager.sendToPlayer(
              CommandeFactory.carteImmediate((long) jeu.getTourJoueur() + 1),
              jeu.getTourJoueur() + 1);
          break;
        case "Poupée démoniaque":
          socketManager.sendToPlayer(
              CommandeFactory.carteImmediate((long) jeu.getTourJoueur() + 1),
              jeu.getTourJoueur() + 1);
          break;
        case "Peau de banane":
          if (getJoueurTourJoueur().getEqts().size() == 0) {
            socketManager.sendToEveryBody(
                CommandeFactory.logInfo(
                    getJoueurTourJoueur().getIdJoueur()
                        + " a glissé sur une "
                        + "banane, il subit 1 dégat"));
            if (getJoueurTourJoueur().isDead()) {
              jeuController.stepMort(getJoueurTourJoueur());
            }
            return false;
          } else {
            socketManager.sendToPlayer(
                CommandeFactory.carteImmediate(getJoueurTourJoueur().getEqts()),
                jeu.getTourJoueur() + 1);
          }
          break;
        case "Araignée sanguinaire":
          socketManager.sendToPlayer(
              CommandeFactory.carteImmediate((long) jeu.getTourJoueur() + 1),
              jeu.getTourJoueur() + 1);
          break;
        case "Dynamite":
          int zoneBoom = new DeManager().lancerDes();
          if (zoneBoom == 7) {
            socketManager.sendToEveryBody(
                CommandeFactory.logInfo("Personne n'est touché par la dynamite"));
          } else {
            Lieu lieu = jeu.terrain.getLieuByLancerDe(zoneBoom);
            if (lieu != null) {
              socketManager.sendToEveryBody(
                  CommandeFactory.logInfo(
                      "Les personnages sur le secteur "
                          + lieu.getNom()
                          + " prennent 3 "
                          + "Blessures :"));
              for (Joueur j : lieu.getJoueurs()) {
                if (j.possedeEqtdeNom("Amulette")) { // handle Amulette
                  socketManager.sendToEveryBody(
                      CommandeFactory.logInfo(
                          " - "
                              + j.getIdJoueur()
                              + " a été protégé par son Amulette ! ("
                              + j.getNbBlessures()
                              + "  Blessures)"));
                } else {
                  int def = j.getModificateurDefense();
                  j.ajouterBlessures(3 - def);
                  socketManager.sendToEveryBody(
                      CommandeFactory.updateJoueur(j));
                  socketManager.sendToEveryBody(
                      CommandeFactory.logInfo(
                          " - " + j.getIdJoueur() + " (" + j.getNbBlessures() + "  blessures)"));
                  if (j.isDead()) {
                    jeuController.stepMort(j);
                  }
                }
              }
            }
          }
          return false;
        case "Rituel diabolique":
          if (joueur.isRevele() && Faction.SHADOW.equals(joueur.getPersonnage().getFaction())) {
            joueur.setNbBlessuresToZero();
            socketManager.sendToEveryBody(
                CommandeFactory.updateJoueur(joueur));
            socketManager.sendToEveryBody(
                CommandeFactory.logInfo(
                    joueur.getIdJoueur() + " s'est " + "soigné de toutes ses blessures."));
          } else {
            socketManager.sendToEveryBody(CommandeFactory.logInfo("Rien ne se passe."));
          }
          return false;
        default:
          return false;
      }
      return true;
    } else {
      return false;
    }
  }

  public boolean manageImmediatLumiere() {
    Immediat immediat = (Immediat) jeu.derniereCarteTiree;
    Joueur joueur = this.getJoueurTourJoueur();
    if (immediat.isLumiere()) {
      jeu.cartesLumieres.defausser(immediat);
      switch (immediat.getNom()) {
        case "Miroir Divin":
          if (joueur.getPersonnage().getNom() != "Métamorphe"
              && joueur.getPersonnage().getFaction() == Faction.SHADOW) {
            socketManager.sendToEveryBody(
                CommandeFactory.logInfo(
                    joueur.getIdJoueur() + " se revel être " + joueur.getPersonnage().getNom()));
            joueur.revele();
            socketManager.sendToEveryBody(
                CommandeFactory.updateJoueur(joueur));
          } else {
            socketManager.sendToEveryBody(CommandeFactory.logInfo("Rien ne se passe."));
          }
          return false;
        case "Eau Benite":
          joueur.retirerBlessures(2);
          socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(joueur));
          socketManager.sendToEveryBody(
              CommandeFactory.logInfo(joueur.getIdJoueur() + " s'est " + "soigné de 2 Blessures."));
          return false;
        case "Ange Gardien":
          joueur.setAngeGardien(true);
          socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(joueur));
          return false;
        case "Eclair Purificateur":
          socketManager.sendToEveryBody(
              CommandeFactory.logInfo("Les autre personnage prennent 2 " + "Blessures :"));
          for (Joueur j : jeu.joueurs) {
            if (j.getIdJoueur() != joueur.getIdJoueur()) {
              j.ajouterBlessures(2);
              socketManager.sendToEveryBody(
                  CommandeFactory.updateJoueur(j));
              socketManager.sendToEveryBody(
                  CommandeFactory.logInfo(
                      " - J" + j.getIdJoueur() + " (" + j.getNbBlessures() + "  Blessures)"));
              if (j.isDead()) {
                jeuController.stepMort(j);
              }
            }
          }
          return false;
        case "Premiers Secours":
          socketManager.sendToPlayer(
              CommandeFactory.carteImmediate((long) jeu.getTourJoueur() + 1),
              jeu.getTourJoueur() + 1);
          break;
        case "Benediction":
          socketManager.sendToPlayer(
              CommandeFactory.carteImmediate((long) jeu.getTourJoueur() + 1),
              jeu.getTourJoueur() + 1);
          break;
        case "Savoir Encestral":
          return false;
        case "Avement Supreme":
          if (joueur.isRevele() && Faction.HUNTER.equals(joueur.getPersonnage().getFaction())) {
            joueur.setNbBlessuresToZero();
            socketManager.sendToEveryBody(
                CommandeFactory.updateJoueur(joueur));
            socketManager.sendToEveryBody(
                CommandeFactory.logInfo(
                    joueur.getIdJoueurStringify() + " a soigné toutes ses Blessures."));
          } else {
            socketManager.sendToEveryBody(CommandeFactory.logInfo("Rien ne se passe."));
          }
          return false;
        case "Barre De Chocolat":
          if (joueur.isRevele() && joueur.getPersonnage().getNom() == "Metamorphe"
              || joueur.getPersonnage().getNom() == "Emi"
              || joueur.getPersonnage().getNom() == "Agnes"
              || joueur.getPersonnage().getNom() == "Allie"
              || joueur.getPersonnage().getNom() == "Ellen"
              || joueur.getPersonnage().getNom() == "Momie") {
            joueur.setNbBlessuresToZero();
            socketManager.sendToEveryBody(
                CommandeFactory.updateJoueur(joueur));
            socketManager.sendToEveryBody(
                CommandeFactory.logInfo(
                    joueur.getIdJoueur() + " s'est " + "soigné de toutes ses blessures."));
          } else {
            socketManager.sendToEveryBody(CommandeFactory.logInfo("Rien ne se passe."));
          }
          return false;
        default:
          return false;
      }
      return true;
    } else {
      return false;
    }
  }
}
