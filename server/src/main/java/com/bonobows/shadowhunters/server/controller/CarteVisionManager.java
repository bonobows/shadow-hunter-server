package com.bonobows.shadowhunters.server.controller;

import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.CarteVision;
import com.bonobows.shadowhunters.common.model.EffetVision;
import com.bonobows.shadowhunters.common.model.Faction;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;

import java.util.ArrayList;
import java.util.List;

public class CarteVisionManager {

  public static boolean isEffetDeLaCarteVision(CarteVision carteVision, Joueur joueur) {
    Personnage personnage = joueur.getPersonnage();
    switch (carteVision.getTypeCondition()) {
      case NOTEST:
        return true;
      case SHADOW:
        return personnage.getFaction() == Faction.SHADOW;
      case HUNTER:
        return personnage.getFaction() == Faction.HUNTER;
      case NEUTRE:
        return personnage.getFaction() == Faction.NEUTRE;
      case SHADOW_HUNTER:
        return personnage.getFaction() == Faction.SHADOW
            || personnage.getFaction() == Faction.HUNTER;
      case SHADOW_NEUTRE:
        return personnage.getFaction() == Faction.SHADOW
            || personnage.getFaction() == Faction.NEUTRE;
      case HUNTER_NEUTRE:
        return personnage.getFaction() == Faction.HUNTER
            || personnage.getFaction() == Faction.NEUTRE;
      case PV_MOINS_ONZE:
        return personnage.getPv() <= 11;
      case PV_PLUS_DOUZE:
        return personnage.getPv() >= 12;
      default:
        return false;
    }
  }

  public static List<ActionVision> getActionEffet(
      CarteVision carteVision,
      Joueur joueur,
      boolean hasEquipement
  ) {
    List<ActionVision> actionsVisions = new ArrayList<>();
    boolean estMetamorphe = "Métamorphe".equals(joueur.getPersonnage().getNom());
    if (isEffetDeLaCarteVision(carteVision, joueur) || estMetamorphe) {
      switch (carteVision.getTypeEffet()) {
        case SUPREME:
          actionsVisions.add(ActionVision.MONTRER_CARTE);
          break;
        case DEUX_BLESSURES:
          actionsVisions.add(ActionVision.PRENDRE_DEGAT);
          break;
        case BLESSURE:
          actionsVisions.add(ActionVision.PRENDRE_DEGAT);
          break;
        case BOE:
          actionsVisions.add(ActionVision.PRENDRE_DEGAT);
          if (hasEquipement) {
            actionsVisions.add(ActionVision.DONNER_EQUIPEMENT);
          }
          break;
        case CONDITIONNEL:
          if (joueur.getNbBlessures() == 0) {
            actionsVisions.add(ActionVision.PRENDRE_DEGAT);
          } else {
            actionsVisions.add(ActionVision.SE_SOIGNER);
          }
          break;
        default:
          actionsVisions.add(ActionVision.RIEN);
      }
    } else {
      if (carteVision.getTypeEffet() != EffetVision.SUPREME) {
        actionsVisions.add(ActionVision.RIEN);
      }
    }
    if (estMetamorphe && !actionsVisions.get(0).equals(ActionVision.RIEN)) {
      actionsVisions.add(ActionVision.RIEN);
    }
    return actionsVisions;
  }
}
