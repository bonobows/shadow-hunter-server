package com.bonobows.shadowhunters.server.model;

import com.bonobows.shadowhunters.server.controller.JeuController;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class Step {
  private JeuController jeuController;
  private Method step;
  private List<Object> args;

  public Step(JeuController j, Method step, List<Object> args) {
    this.jeuController = j;
    this.step = step;
    this.args = args;
  }

  public void callStep() {
    try {
      this.step.invoke(jeuController, args.toArray());
    } catch (IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }
}
