package com.bonobows.shadowhunters.server;

public class BuildConfig {

  public static boolean DEMO_MODE;

  static {
    String demo = System.getProperty("DEMO", "false");
    DEMO_MODE = Boolean.parseBoolean(demo);
  }

}
