package com.bonobows.shadowhunters.server.demo;

import com.bonobows.shadowhunters.common.factory.LieuFactory;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;

import java.util.ArrayList;

public class DemoTerrain extends Terrain {

  public DemoTerrain() {
    this.lieux = new ArrayList<>();
    lieux.add(LieuFactory.generateSanctuaire());
    lieux.add(LieuFactory.generateMonastere());
    lieux.add(LieuFactory.generateForet());
    lieux.add(LieuFactory.generatePorteOutremonde());
    lieux.add(LieuFactory.generateCimetiere());
    lieux.add(LieuFactory.generateAntreHermite());
  }
}
