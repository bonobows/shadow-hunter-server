package com.bonobows.shadowhunters.server.model;

import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Pioche;
import com.bonobows.shadowhunters.common.model.Utilisateur;

import java.util.List;

public class Partie {
  private int nbJoueurs;
  // attributs d'associtations cf diag classes
  private List<Utilisateur> utilisateurs;
  private List<Joueur> joueurs;
  //  private JeuController jeu;
  private List<Pioche> pioches;
}
