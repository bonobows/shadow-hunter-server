package com.bonobows.shadowhunters.server;

import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.server.controller.JeuController;
import com.bonobows.shadowhunters.server.model.Jeu;
import com.bonobows.shadowhunters.server.model.command.CommandeManager;
import com.bonobows.shadowhunters.server.model.socket.SocketJoueur;
import com.bonobows.shadowhunters.server.model.socket.SocketManager;
import com.bonobows.shadowhunters.server.tool.ReceiveMessage;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.util.ArrayList;

/**
 * Created by Milihhard on 02/10/2017.
 */
public class Server {

  ServerSocket socket;
  private ArrayList<ReceiveMessage> receiveMessages;
  private JeuController jeuController;
  private SocketManager socketManager;

  public Server() {
    receiveMessages = new ArrayList<>();
    int startPort = 2009;
    try {
      socket = new ServerSocket(startPort);
      socket.setSoTimeout(1000);
    } catch (IOException e) {
      e.printStackTrace();
    }
    this.socketManager = new SocketManager();
    Jeu jeu = new Jeu();

    this.jeuController = new JeuController(jeu, socketManager);
    CommandeManager commandeManager = new CommandeManager(jeuController);
    this.socketManager.setCommandeManager(commandeManager);
  }

  public Server(int nbJoueur) {
  }

  public void start() {
    int i = 1;
    int nbJoueursMax = 8;
    int nbJoueursMin = 4;
    while (i < nbJoueursMax + 1 && !jeuController.isPartieDemarrer()) {
      System.out.println("En attente du joueur " + i);
      try {
        SocketJoueur s = new SocketJoueur(socket.accept());
        System.out.println("connexion avec le joueur " + i + " accepté");
        socketManager.addSocketJoueur(s);
        receiveMessages.add(new ReceiveMessage(socketManager, i));
        Thread t = new Thread(receiveMessages.get(i - 1));
        t.start();

        this.jeuController.addJoueur();

        if (i == nbJoueursMin) {
          socketManager.sendToPlayer(CommandeFactory.demarrerPartie(), 1);
        }
        i++;
      } catch (IOException e) {
        if (!(e instanceof InterruptedIOException)) {
          e.printStackTrace();
        }
      }
    }
    this.jeuController.initJeu(i - 1);
    this.jeuController.demarrerJeu();
  }
}
