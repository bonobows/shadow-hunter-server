package com.bonobows.shadowhunters.server.demo;

import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.server.exception.MauvaisNombreJoueurException;
import com.bonobows.shadowhunters.server.factory.PersonnageFactory;

import java.util.ArrayList;

public class DemoPersonnageFactory extends PersonnageFactory {

  private DemoPersonnageFactory() {
    super();
  }

  public static ArrayList<Personnage> generatePersonnage(int nbJoueur)
      throws MauvaisNombreJoueurException {
    if (nbJoueur != 4) {
      throw new MauvaisNombreJoueurException();
    }

    ArrayList<Personnage> personnages = new ArrayList<>();
    personnages.add(Hunters.Franklin);
    personnages.add(Hunters.Emi);
    personnages.add(Shadows.LoupGarou);
    personnages.add(Shadows.Vampire);
    return personnages;
  }
}
