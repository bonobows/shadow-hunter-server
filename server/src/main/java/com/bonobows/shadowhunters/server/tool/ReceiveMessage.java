package com.bonobows.shadowhunters.server.tool;

import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.server.model.socket.SocketManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Milihhard on 02/10/2017.
 */
public class ReceiveMessage implements Runnable {

  private BufferedReader in;
  private int joueur;
  private SocketManager socketManager;

  public ReceiveMessage(SocketManager socketManager, int joueur) {

    this.joueur = joueur;
    this.socketManager = socketManager;
    try {
      this.in = new BufferedReader(new InputStreamReader(
          this.socketManager.getPlayerInputStream(joueur), "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      this.in = new BufferedReader(new InputStreamReader(
          this.socketManager.getPlayerInputStream(joueur)));
    }
  }

  public void run() {

    while (true) {
      try {
        String message = in.readLine();
        this.socketManager.receiveMessage(message, joueur);

      } catch (IOException e) {
        System.out.println("Le joueur " + joueur + " s'est déconnecté");
        socketManager.sendToOtherPlayers(
            CommandeFactory.logWarning("Le joueur " + joueur + " s'est déconnecté"), joueur);
        try {
          socketManager.close(joueur);
        } catch (IOException e1) {
          e1.printStackTrace();
        }
        return;
      }
    }
  }
}
