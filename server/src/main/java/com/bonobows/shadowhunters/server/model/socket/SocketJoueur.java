package com.bonobows.shadowhunters.server.model.socket;

import com.bonobows.shadowhunters.common.model.commande.Commande;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Milihhard on 04/10/2017.
 */
public class SocketJoueur {

  private Socket socket;

  public SocketJoueur(Socket socket) {
    this.socket = socket;
  }

  public void sendMessage(Commande commande) {
    try {
      PrintWriter out = null;
      out = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream(),
          "UTF-8"));
      out.println(commande.commandeToJson());
      out.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public Socket getSocket() {
    return this.socket;
  }
}
