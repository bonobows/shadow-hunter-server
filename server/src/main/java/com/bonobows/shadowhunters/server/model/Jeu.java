package com.bonobows.shadowhunters.server.model;

import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.Pioche;
import com.bonobows.shadowhunters.common.model.TypeCarte;
import com.bonobows.shadowhunters.common.model.Utilisateur;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;
import com.bonobows.shadowhunters.server.BuildConfig;
import com.bonobows.shadowhunters.server.controller.DeManager;
import com.bonobows.shadowhunters.server.demo.DemoDeManager;
import com.bonobows.shadowhunters.server.demo.DemoPersonnageFactory;
import com.bonobows.shadowhunters.server.demo.DemoPioche;
import com.bonobows.shadowhunters.server.demo.DemoTerrain;
import com.bonobows.shadowhunters.server.exception.MauvaisNombreJoueurException;
import com.bonobows.shadowhunters.server.factory.PersonnageFactory;

import java.util.ArrayList;
import java.util.List;

public class Jeu {
  public List<Joueur> joueurs;
  public Terrain terrain;
  public DeManager des;
  public Pioche cartesVisions;
  public Pioche cartesLumieres;
  public Pioche cartesTenebres;
  private List<Utilisateur> utilisateurs;
  private Partie partie;
  private List<Carte> cartes;
  private int tourJoueur;
  public Carte derniereCarteTiree;

  public Jeu() {
    if (BuildConfig.DEMO_MODE) {
      this.terrain = new DemoTerrain();
      this.des = new DemoDeManager();
      this.cartesVisions = new DemoPioche(TypeCarte.VISION);
      this.cartesTenebres = new DemoPioche(TypeCarte.TENEBRE);
      this.cartesLumieres = new DemoPioche(TypeCarte.LUMIERE);
    } else {
      this.terrain = new Terrain();
      this.des = new DeManager();
      this.cartesVisions = new Pioche(TypeCarte.VISION);
      this.cartesTenebres = new Pioche(TypeCarte.TENEBRE);
      this.cartesLumieres = new Pioche(TypeCarte.LUMIERE);
    }

    this.utilisateurs = new ArrayList<>();
    this.joueurs = new ArrayList<>();
    this.cartes = new ArrayList<>();
    this.partie = new Partie();
    this.tourJoueur = 0;
  }

  public int getTourJoueur() {
    return tourJoueur;
  }

  public void setTourJoueur(int tourJoueur) {
    if (tourJoueur >= 0 && tourJoueur < this.joueurs.size()) {
      this.tourJoueur = tourJoueur;
    }
  }

  public void incrementTourJoueur() {
    this.tourJoueur++;
    if (tourJoueur >= joueurs.size()) {
      tourJoueur = 0;
    }
  }

  public void initPersonnages() throws MauvaisNombreJoueurException {
    int nbJoueurs = joueurs.size();
    List<Personnage> personnages;

    if (BuildConfig.DEMO_MODE) {
      personnages = DemoPersonnageFactory.generatePersonnage(nbJoueurs);
      joueurs.get(0).ajouterBlessures(11);
      joueurs.get(1).ajouterBlessures(6);
      joueurs.get(2).ajouterBlessures(6);
      joueurs.get(3).ajouterBlessures(6);

      joueurs.get(0).addEquipement(new Equipement(
          "Broche de chance",
          "Un joueur dans la Forêt Hantée ne peut pas "
              + "utiliser le pouvoir du Lieu pour vous infliger des Blessures "
              + "(mais il peut toujours vous guérir).",
          TypeCarte.LUMIERE));
    } else {
      personnages = PersonnageFactory.generatePersonnage(nbJoueurs);
    }

    for (int i = 0; i < nbJoueurs; i++) {
      this.joueurs.get(i).setPersonnage(personnages.get(i));
    }
  }
}
