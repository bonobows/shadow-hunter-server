package com.bonobows.shadowhunters.server.factory;

import com.bonobows.shadowhunters.common.model.Faction;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.server.exception.MauvaisNombreJoueurException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class PersonnageFactory {

  protected PersonnageFactory() {}

  public static ArrayList<Personnage> generatePersonnage(int nbJoueur)
      throws MauvaisNombreJoueurException {
    ArrayList<Personnage> personnages = new ArrayList<>();
    int nbJoueurShadow = 0;
    int nbJoueurHunter = 0;
    int nbJoueurNeutre = 0;
    switch (nbJoueur) {
      case 4:
        nbJoueurHunter = 2;
        nbJoueurShadow = 2;
        break;
      case 5:
        nbJoueurHunter = 2;
        nbJoueurShadow = 2;
        nbJoueurNeutre = 1;
        break;
      case 6:
        nbJoueurHunter = 2;
        nbJoueurShadow = 2;
        nbJoueurNeutre = 2;
        break;
      case 7:
        nbJoueurHunter = 3;
        nbJoueurShadow = 3;
        nbJoueurNeutre = 1;
        break;
      case 8:
        nbJoueurHunter = 3;
        nbJoueurShadow = 3;
        nbJoueurNeutre = 2;
        break;
      default:
        throw new MauvaisNombreJoueurException();
    }
    personnages.addAll(PersonnageFactory.createHunters(nbJoueurHunter));
    personnages.addAll(PersonnageFactory.createShadows(nbJoueurShadow));
    personnages.addAll(PersonnageFactory.createNeutres(nbJoueurNeutre));
    Collections.shuffle(personnages);
    return personnages;
  }

  public static ArrayList<Personnage> createHunters(int nb) {
    if (nb == 0) {
      return new ArrayList<>();
    }
    ArrayList<Personnage> personnagesHunter = new ArrayList<>();
    personnagesHunter.add(Hunters.Emi);
    personnagesHunter.add(Hunters.Georges);
    personnagesHunter.add(Hunters.Franklin);
    int nbDansDeck = 3;
    while (nbDansDeck - nb > 0) {
      Random rand = new Random();
      personnagesHunter.remove(rand.nextInt(personnagesHunter.size()));
      nb++;
    }
    return personnagesHunter;
  }

  public static ArrayList<Personnage> createShadows(int nb) {
    if (nb == 0) {
      return new ArrayList<>();
    }
    ArrayList<Personnage> personnagesShadow = new ArrayList<>();
    personnagesShadow.add(Shadows.LoupGarou);
    personnagesShadow.add(Shadows.Vampire);
    personnagesShadow.add(Shadows.Metamorphe);
    int nbDansDeck = 3;
    while (nbDansDeck - nb > 0) {
      Random rand = new Random();
      personnagesShadow.remove(rand.nextInt(personnagesShadow.size()));
      nb++;
    }
    return personnagesShadow;
  }

  public static ArrayList<Personnage> createNeutres(int nb) {
    if (nb == 0) {
      return new ArrayList<>();
    }
    ArrayList<Personnage> personnagesNeutre = new ArrayList<>();
    personnagesNeutre.add(Neutres.Allie);
    personnagesNeutre.add(Neutres.Bob);
    personnagesNeutre.add(Neutres.Charles);
    personnagesNeutre.add(Neutres.Daniel);
    int nbDansDeck = 4;
    while (nbDansDeck - nb > 0) {
      Random rand = new Random();
      personnagesNeutre.remove(rand.nextInt(personnagesNeutre.size()));
      nb++;
    }
    return personnagesNeutre;
  }

  public static class Hunters {
    public static Personnage Emi = new Personnage(
        10,
        "Emi",
        "Téléportation : pour vous déplacer vous pouvez lancer "
            + "normalement les dés ou vous déplacer sur la carte Lieu adjacente.",
        "Tuer les shadows",
        Faction.HUNTER
    );

    public static Personnage Georges = new Personnage(
        14,
        "Georges",
        "Démolition : Au début de votre tour choississez un "
            + "joueur et infliger-lui autant de Blessures que le résultat d'un dé à 4 faces. "
            + "Utilisation unique.",
        "Tuer les shadows",
        Faction.HUNTER
    );

    public static Personnage Franklin = new Personnage(
        12,
        "Franklin",
        "Foudre : Au début de votre tour choississez un joueur "
            + "et infliger-lui autant de Blessures que le résultat d'un dé à 6 faces. "
            + "Utilisation unique.",
        "Tuer les shadows",
        Faction.HUNTER
    );
  }

  public static class Shadows {
    public static Personnage LoupGarou = new Personnage(
        14,
        "Loup-Garou",
        "Contre-attaque : après avoir subi l'attaque d'un joueur "
            + "vous pouvez contre attaquer immédiatement.",
        "Tuer les hunters",
        Faction.SHADOW
    );

    public static Personnage Vampire = new Personnage(
        13,
        "Vampire",
        "Morsure : si vous attaquez un joueur et lui infligez des Blessures, "
            + "soignez immédiatement 2 de vos Blessures.",
        "Tuer les hunterq",
        Faction.SHADOW
    );

    public static Personnage Metamorphe = new Personnage(
        11,
        "Métamorphe",
        "Imitation : vous pouvez mentir (sans avoir à révéler votre identité) "
            + "lorsqu'on vous donne une carte Vision.",
        "Tuer les hunters",
        Faction.SHADOW
    );
  }

  public static class Neutres {
    public static final Personnage Allie = new Personnage(
        8,
        "Allie",
        "Amour maternel : soignez toutes vos Blessures. Utilisation unique",
        "Être encore en vie lorsque la partie se termine",
        Faction.NEUTRE
    );

    public static final Personnage Bob = new Personnage(
        10,
        "Bob",
        "Braquage : Si vous infliger au moins 2 Blessures à un personnage "
            + "lors d'une attaque, vous pouvez lui voler une carte équipement "
            + "au lieu de lui infliger les Blessures",
        "Posséder 5 cartes équipement ou plus",
        Faction.NEUTRE
    );

    public static final Personnage Charles = new Personnage(
        11,
        "Charles",
        "Festin sanglant",
        "Tuer un autre personnage par une attaque alors qu'il y a déjà eu 3 morts ou plus",
        Faction.NEUTRE
    );

    public static Personnage Daniel = new Personnage(
        13,
        "Daniel",
        "Désespoir",
        "Être le premier à mourir "
            + "OU être en vie quand tous les personnages Shadow sont morts",
        Faction.NEUTRE
    );
  }
}
