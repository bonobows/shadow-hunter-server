package com.bonobows.shadowhunters.server.demo;

import com.bonobows.shadowhunters.common.model.Pioche;
import com.bonobows.shadowhunters.common.model.TypeCarte;

public class DemoPioche extends Pioche {

  public DemoPioche(TypeCarte typeCartes) {
    super(typeCartes);
  }

  @Override
  public void melanger() {
  }
}
