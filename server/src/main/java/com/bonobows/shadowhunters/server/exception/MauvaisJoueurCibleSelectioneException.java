package com.bonobows.shadowhunters.server.exception;

public class MauvaisJoueurCibleSelectioneException extends Exception {
  public MauvaisJoueurCibleSelectioneException() {
    super("Le joueur cible n'est pas valide.");
  }
}

