package com.bonobows.shadowhunters.server.controller;

import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Couleur;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Faction;
import com.bonobows.shadowhunters.common.model.Immediat;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.Utilisateur;
import com.bonobows.shadowhunters.common.model.commande.CodeCommande;
import com.bonobows.shadowhunters.common.model.commande.Commande;
import com.bonobows.shadowhunters.common.model.commande.LogLevel;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.server.controller.lieu.LieuController;
import com.bonobows.shadowhunters.server.exception.MauvaisJoueurCibleSelectioneException;
import com.bonobows.shadowhunters.server.exception.MauvaisNombreJoueurException;
import com.bonobows.shadowhunters.server.factory.PersonnageFactory;
import com.bonobows.shadowhunters.server.model.Jeu;
import com.bonobows.shadowhunters.server.model.Step;
import com.bonobows.shadowhunters.server.model.socket.SocketManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/** Created by Milihhard on 04/10/2017. */
public class JeuController extends Controller {

  private static JeuController instance = null;
  private boolean casVolPostMeurtre = false;
  private int cibleVol;
  private Stack<Step> stepsToResolve;
  private LieuController lieuController;
  private CarteController carteController;
  private boolean partieDemarrer = false;
  private List<Joueur> joueursMort;

  private Stack<Couleur> couleurs;

  public JeuController(Jeu jeu, SocketManager socketManager) {
    super(jeu, socketManager);
    this.stepsToResolve = new Stack<>();
    this.joueursMort = new ArrayList<>();

    carteController = new CarteController(jeu, socketManager);
    lieuController = new LieuController(jeu, socketManager);
    carteController.setJeuController(this);
    lieuController.setJeuController(this);
    lieuController.setCarteController(carteController);

    couleurs = new Stack<>();
    couleurs.push(Couleur.MARRON);
    couleurs.push(Couleur.BLEU);
    couleurs.push(Couleur.JAUNE);
    couleurs.push(Couleur.ORANGE);
    couleurs.push(Couleur.ROUGE);
    couleurs.push(Couleur.BLANC);
    couleurs.push(Couleur.VERT);
    couleurs.push(Couleur.VIOLET);
    Collections.shuffle(couleurs);
  }

  public LieuController getLieuController() {
    return lieuController;
  }

  public boolean isPartieDemarrer() {
    return partieDemarrer;
  }

  public void demarrerPartie() {
    this.partieDemarrer = true;
  }

  public void addJoueur() {

    int nbJoueurs = this.jeu.joueurs.size();
    this.jeu.joueurs.add(new Joueur(couleurs.pop()));
    jeu.joueurs.get(nbJoueurs).setIdJoueur(nbJoueurs + 1);
    this.socketManager.sendToPlayer(CommandeFactory.joueur(nbJoueurs + 1), nbJoueurs + 1);
  }

  private void initJoueur(int nbJoueurs) {
    this.partieDemarrer = true;

    try {
      jeu.initPersonnages();

      for (int i = 0; i < nbJoueurs; i++) {
        System.out.println(
            String.format(
                "Le joueur %d a reçu le personnage %s",
                i + 1, this.jeu.joueurs.get(i).getPersonnage()));
      }
    } catch (MauvaisNombreJoueurException e) {
      e.printStackTrace();
    }
  }

  public void initJeu(int nbJoueurs) {
    this.initJoueur(nbJoueurs);

    this.socketManager.sendToEveryBody(CommandeFactory.logInfo("Début de la partie"));
    for (int i = 1; i <= this.jeu.joueurs.size(); i++) {
      this.socketManager.sendToPlayer(
          CommandeFactory.personnage(jeu.joueurs.get(i - 1).getPersonnage()), i);
    }
    // On envoie les joueurs aus clients
    socketManager.sendToEveryBody(CommandeFactory.sendJoueurs(this.jeu.joueurs));
    System.out.println(jeu.terrain);
    this.socketManager.sendToEveryBody(CommandeFactory.terrain(jeu.terrain));
  }

  public void demarrerJeu() {
    jeu.setTourJoueur(0);
    this.stepNewTurn();
  }

  public void stepMort(Joueur joueurMort) {

    for (Joueur joueur : this.jeu.joueurs) {
      if (joueur.getPersonnage().equals(PersonnageFactory.Neutres.Daniel)) {
        if (!joueur.isRevele()) {
          joueur.revele();
          socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(joueur));
        }
        break;
      }
    }

    joueursMort.add(joueurMort);
    jeu.terrain.removeDeadJoueur(joueurMort);
    this.socketManager.sendToEveryBody(CommandeFactory.mortJoueur(joueurMort));
    int j = this.jeu.joueurs.indexOf(joueurMort) + 1;
    socketManager.sendCommandToAPlayerWhichIsDifferent(
        CommandeFactory.logInfo(
            "Vous avez été tué par le joueur " + (jeu.getTourJoueur() + 1) + "."),
        CommandeFactory.logInfo(
            "Le joueur " + j + " a été tué par le joueur " + (jeu.getTourJoueur() + 1) + "."),
        j);
    casVolPostMeurtre = true;
    if (jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Crucifix en argent")) {
      // si l'assaillant possède le cricifix en argent, il prend tous les eqts du joueur mort
      cibleVol = j;
      stepEqtAVolerChoisi(42);
    } else {
      stepCibleVolChoisie(j - 1);
      // la fin du tour se passe dans stepEqtAVolerChoisi
    }
  }

  public void sePresenter(String pseudo, int indiceJoueur) {
    System.out.println("pseudo " + indiceJoueur + " " + pseudo);
    jeu.joueurs.get(indiceJoueur - 1).setUtilisateur(new Utilisateur(pseudo));
    // socketManager.sendToEveryBody(CommandeFactory.updateJoueur(
    //     jeu.joueurs.get(indiceJoueur - 1)));
    socketManager.sendToEveryBody(CommandeFactory.sendJoueurs(this.jeu.joueurs));
  }

  public void stepAttac() {
    List<Joueur> joueursAPortee =
        jeu.terrain.getJoueursPeutAttaque(
            jeu.joueurs.get(jeu.getTourJoueur()),
            jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Revolver des ténèbres"));
    this.socketManager.sendToPlayer(
        CommandeFactory.attac(
            joueursAPortee,
            jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Sabre hanté Masamune"),
            jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Mitrailleuse funeste")),
        jeu.getTourJoueur() + 1);
    this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
  }

  private void stepNewTurn() {
    this.sendTurn();
    jeu.joueurs.get(jeu.getTourJoueur()).setAngeGardien(false);
    if (jeu.joueurs.get(jeu.getTourJoueur()).isRevele()) {
      stepSeRevelerOuNon("n");
    } else {
      this.seReveler();
      this.socketManager.waitResponse(jeu.getTourJoueur() + 1);
    }
  }

  public void stepPeauBanane(int e, int joueurChoisi) {
    Equipement equipementDonne = getJoueurTourJoueur().getEqts().get(e - 1);
    getJoueurTourJoueur().getEqts().remove(equipementDonne);
    socketManager.sendToEveryBody(
        CommandeFactory.updateJoueur(getJoueurTourJoueur()));
    jeu.joueurs.get(joueurChoisi - 1).addEquipement(equipementDonne);
    socketManager.sendToEveryBody(
        CommandeFactory.updateJoueur(jeu.joueurs.get(joueurChoisi - 1)));
    socketManager.sendToEveryBody(
        CommandeFactory.logInfo(
            getJoueurTourJoueur().getIdJoueur()
                + ""
                + " a donné sa carte équipement ("
                + equipementDonne
                + ") à "
                + jeu.joueurs.get(joueurChoisi - 1).getIdJoueur()));
    lieuController.stepFinEffetLieu();
  }

  public void stepCarteImmediateChoixJoueur(int choixJoueur) {
    Immediat immediat = (Immediat) jeu.derniereCarteTiree;
    Joueur joueurVise = jeu.joueurs.get(choixJoueur - 1);

    if ("Araignée sanguinaire".equals(immediat.getNom())) {
      if (joueurVise.possedeEqtdeNom("Amulette")) { //handle Amulette
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                joueurVise.getIdJoueur()
                    + " a été protégé par son Amulette ! ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
        getJoueurTourJoueur().ajouterBlessures(2);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(getJoueurTourJoueur()));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                getJoueurTourJoueur().getIdJoueur()
                    + " a reçu 2 blessures ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
      } else if (getJoueurTourJoueur().possedeEqtdeNom("Amulette")) {
        joueurVise.ajouterBlessures(2);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(joueurVise));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                joueurVise.getIdJoueur()
                    + " a reçu 2 Blessures ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                getJoueurTourJoueur().getIdJoueur()
                    + " a été protégé par son Amulette ! ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
      } else {
        joueurVise.ajouterBlessures(2);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(joueurVise));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                joueurVise.getIdJoueur()
                    + " a reçu 2 Blessures ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
        getJoueurTourJoueur().ajouterBlessures(2);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(getJoueurTourJoueur()));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                getJoueurTourJoueur().getIdJoueur()
                    + " a reçu 2 Blessures ("
                    + getJoueurTourJoueur().getNbBlessures()
                    + " Blessures au total)"));
      }
    } else if ("Chauve-souris vampiré".equals(immediat.getNom())) {
      if (joueurVise.possedeEqtdeNom("Amulette")) { //handle Amulette
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                joueurVise.getIdJoueur()
                    + " a été protégé par son Amulette ! ("
                    + joueurVise.getNbBlessures()
                    + " blessures au total)"));
      } else {
        joueurVise.ajouterBlessures(2);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(joueurVise));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                joueurVise.getIdJoueur()
                    + " a reçu 2 blessures ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
      }
      getJoueurTourJoueur().retirerBlessures(1);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(getJoueurTourJoueur()));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              getJoueurTourJoueur().getIdJoueur()
                  + " a été soigné de 1 blessure ("
                  + getJoueurTourJoueur().getNbBlessures()
                  + " Blessures au total)"));
    } else if ("Premiers Secours".equals(immediat.getNom())) {
      joueurVise.setNbBlessures(7);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(joueurVise));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              joueurVise.getIdJoueurStringify()
                  + " vient de passer à 7 Blessures"));
    } else if ("Benediction".equals(immediat.getNom())) {
      int de6 = jeu.des.lancerD6();
      socketManager.sendToEveryBody(CommandeFactory.logInfo("Résultat du dé : " + de6));
      joueurVise.retirerBlessures(de6);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(joueurVise));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              joueurVise.getIdJoueurStringify()
                  + " est soigné de : "
                  + de6
                  + " sa vie et a ("
                  + joueurVise.getNbBlessures()
                  + " Blessures au total)"));
    } else {
      int de6 = jeu.des.lancerD6();
      socketManager.sendToEveryBody(CommandeFactory.logInfo("Résultat du dé : " + de6));
      if (de6 <= 4) {
        joueurVise.ajouterBlessures(3);
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                joueurVise.getIdJoueurStringify()
                    + " a reçu 3 Blessures ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
      } else {
        getJoueurTourJoueur().ajouterBlessures(3);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(getJoueurTourJoueur()));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo(
                getJoueurTourJoueur().getIdJoueurStringify()
                    + " a reçu 3 Blessures ("
                    + joueurVise.getNbBlessures()
                    + " Blessures au total)"));
      }
    }
    if (joueurVise.isDead()) {
      stepMort(joueurVise);
    }
    if (getJoueurTourJoueur().isDead()) {
      stepMort(joueurVise);
    }
    lieuController.stepFinEffetLieu();
  }

  public void stepCibleVolChoisie(int choix) {
    cibleVol = choix + 1;
    String nomEqt;

    if (casVolPostMeurtre) {
      this.socketManager.sendToPlayer(
          CommandeFactory.logInfo("Vous pouvez dépouiller le cadavre de votre récente victime !"),
          jeu.getTourJoueur() + 1);
    }

    if (jeu.joueurs.get(cibleVol - 1).getEqts().size() == 1) {
      // Pas besoin de demander quel eqt voler si la cible n'en a qu'un.
      nomEqt = jeu.joueurs.get(cibleVol - 1).getEqts().get(0).getNom();

      getJoueurTourJoueur()
          .getEqts()
          .add(jeu.joueurs.get(cibleVol - 1).getEqts().get(0));
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(getJoueurTourJoueur()));
      jeu.joueurs.get(cibleVol - 1).getEqts().remove(0);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(jeu.joueurs.get(cibleVol - 1)));

      this.socketManager.sendToPlayer(
          CommandeFactory.logInfo(
              "Le joueur "
                  + (jeu.getTourJoueur() + 1)
                  + " vous a volé votre seul équipement ("
                  + nomEqt
                  + ")."),
          cibleVol);
      this.socketManager.sendToPlayer(
          CommandeFactory.logInfo(
              "Vous avez volé le seul équipement du joueur " + cibleVol + " : " + nomEqt + " !"),
          jeu.getTourJoueur() + 1);
      this.socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Joueur "
                  + (jeu.getTourJoueur() + 1)
                  + " a volé le seul équipement du joueur "
                  + cibleVol
                  + "."));
      if (this.casVolPostMeurtre) {
        this.casVolPostMeurtre = false;
        stepEndTurn();
      } else {
        lieuController.stepFinEffetLieu();
      }
    } else if (jeu.joueurs.get(cibleVol - 1).getEqts().size() > 1) {
      this.socketManager.sendToPlayer(
          CommandeFactory.goChoixVol(jeu.joueurs.get(cibleVol - 1).getEqts()),
          jeu.getTourJoueur() + 1);
    } else {
      this.socketManager.sendToPlayer(
          CommandeFactory.logInfo(
              "Dommage votre victime ne possède pas le moindre équipement ! "
                  + "(impossible de dépouiller la victime)"),
          jeu.getTourJoueur() + 1);
      if (this.casVolPostMeurtre) {
        this.casVolPostMeurtre = false;
        stepEndTurn();
      } else {
        lieuController.stepFinEffetLieu();
      }
    }
  }

  public void stepAttacResponse(int joueur) {
    int degats = jeu.des.lancerDe6MoinsD4(0);
    if (this.jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Sabre hanté Masamune")) {
      degats = jeu.des.lancerD4();
    }
    try {
      stepAttacResponse(joueur, degats);
      Joueur attaquant = jeu.joueurs.get(jeu.getTourJoueur());

      if (joueur != -1
          && attaquant.isRevele()
          && attaquant.getPersonnage().equals(PersonnageFactory.Shadows.Vampire)) {
        attaquant.retirerBlessures(2);
        socketManager.sendToEveryBody(
            CommandeFactory.updateJoueur(attaquant));
        socketManager.sendToPlayer(
            CommandeFactory.log(
                LogLevel.INFORMATION,
                "Le vampire attaque ! Il se soigne jusqu'à 2 "
                    + "Blessures. Il a maintenant "
                    + attaquant.getNbBlessures()
                    + " Blessures"),
            jeu.getTourJoueur() + 1);
      }
    } catch (MauvaisJoueurCibleSelectioneException e) {
      socketManager.sendToPlayer(
          CommandeFactory.log(
              LogLevel.ERROR, "Veuillez sélectionner un joueur existant (et pas vous)"),
          jeu.getTourJoueur() + 1);
      socketManager.sendToPlayer(new Commande(CodeCommande.ATTAC), jeu.getTourJoueur() + 1);
    }
  }

  private void stepAttacResponse(int joueur, int degats)
      throws MauvaisJoueurCibleSelectioneException {
    List<Joueur> joueursAPortee =
        jeu.terrain.getJoueursPeutAttaque(
            jeu.joueurs.get(jeu.getTourJoueur()),
            jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Revolver des ténèbres"));
    List<Integer> joueursAAttaquer = new ArrayList<>();
    if (joueur == 42) {
      for (int i = 1; i <= jeu.joueurs.size(); i++) {
        if (i != jeu.getTourJoueur() + 1 && joueursAPortee.contains(jeu.joueurs.get(i - 1))) {
          joueursAAttaquer.add(i);
        }
      }
    } else {
      joueursAAttaquer.add(joueur);
    }

    for (Integer j : joueursAAttaquer) {
      if (j > 0 && j <= jeu.joueurs.size()) {
        if (j == jeu.getTourJoueur() + 1) {
          throw new MauvaisJoueurCibleSelectioneException();
        }
        Joueur attaque = jeu.joueurs.get(j - 1);
        if (attaque.isDead()) {
          throw new MauvaisJoueurCibleSelectioneException();
        }

        // bonus atk joueur en cours
        int atk = getJoueurTourJoueur().getModificateurDegat(degats);
        // bonus def victime
        int def = jeu.joueurs.get(j - 1).getModificateurDefense();

        // affichage cohérent def can't be supp than dgt + atk
        if (def > (degats + atk)) {
          def = degats + atk;
        }

        // Test si ange Gardien
        if (attaque.getAngeGardien()) {
          socketManager.sendToEveryBody(
              CommandeFactory.logInfo(
                  "Le joueur "
                      + j
                      + " est protégé par l'Ange Gardien il ne subit pas de Blessure"
                      + "."));

        } else {
          attaque.ajouterBlessures(degats + atk - def);
          socketManager.sendToEveryBody(
              CommandeFactory.updateJoueur(attaque));
          socketManager.sendToEveryBody(
              CommandeFactory.logInfo(
                  "Le joueur "
                      + (jeu.getTourJoueur() + 1)
                      + " a attaqué le joueur "
                      + j
                      + " (+"
                      + degats
                      + " +"
                      + atk
                      + " -"
                      + def
                      + " Blessures)"));
          socketManager.sendToEveryBody(
              CommandeFactory.logInfo(
                  "Le joueur " + j + " est à " + attaque.getNbBlessures() + " Blessure(s)."));
        }

        if (attaque.isDead()) {
          this.stepTestFinDeJeu(true);
          try {
            List<Object> list =
                new ArrayList<Object>() {
                  {
                    add(attaque);
                  }
                };
            stepsToResolve.push(
                new Step(this, this.getClass().getMethod("stepMort", Joueur.class), list));
          } catch (NoSuchMethodException e) {
            e.printStackTrace();
          }
        }
        if (attaque.isRevele()
            && "Loup-Garou".equals(attaque.getPersonnage().getNom())
            && !attaque.isDead()) {
          try {
            List<Object> list =
                new ArrayList<Object>() {
                  {
                    add(getTourJoueur());
                    add(attaque.getIdJoueur());
                  }
                };
            stepsToResolve.push(
                new Step(
                    this,
                    this.getClass()
                        .getMethod("stepLoupGarouContreAtac", Integer.class, Integer.class),
                    list));
          } catch (NoSuchMethodException e) {
            e.printStackTrace();
          }
        }
      }
    }

    resolveLastSteps();
  }

  public void stepLoupGarouContreAtac(Integer attaquant, Integer joueur) {
    socketManager.sendToEveryBody(CommandeFactory.logInfo("Le Loup-Garou a été attaqué !"));
    socketManager.sendToPlayer(CommandeFactory.capaciteLoupGarou(attaquant), joueur);
  }

  public void stepEqtAVolerChoisi(int choix) {
    String nomEqt;
    Boolean atLeastOne = false;
    ArrayList<Equipement> eqtsAVoler = new ArrayList<>();

    if (casVolPostMeurtre
        && this.jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Crucifix en argent")) {
      eqtsAVoler.addAll(this.jeu.joueurs.get(cibleVol - 1).getEqts());
    } else {
      eqtsAVoler.add(jeu.joueurs.get(cibleVol - 1).getEqts().get(choix));
    }

    for (Equipement e : eqtsAVoler) {
      // seul dans le cas du crucifix il y a plusieurs eqts à voler
      nomEqt = e.getNom();

      jeu.joueurs.get(jeu.getTourJoueur()).getEqts().add(e);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(jeu.joueurs.get(jeu.getTourJoueur())));
      jeu.joueurs.get(cibleVol - 1).getEqts().remove(e);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(jeu.joueurs.get(cibleVol - 1)));

      this.socketManager.sendToPlayer(
          CommandeFactory.logInfo(
              "Le joueur "
                  + (jeu.getTourJoueur() + 1)
                  + " vous a volé votre équipement "
                  + nomEqt
                  + " !"),
          cibleVol);
      this.socketManager.sendToPlayer(
          CommandeFactory.logInfo(
              "Vous avez volé l'équipement " + nomEqt + " du joueur " + cibleVol + " !"),
          jeu.getTourJoueur() + 1);
      this.socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Joueur "
                  + (jeu.getTourJoueur() + 1)
                  + " a volé l'équipement "
                  + nomEqt
                  + " du joueur "
                  + cibleVol
                  + "."));
    }

    if (casVolPostMeurtre) {
      casVolPostMeurtre = false;
      Iterator<Equipement> itler = this.jeu.joueurs.get(cibleVol - 1).getEqts().iterator();
      // on rend l'argent (d'ailleurs dans le cas du crucifix il n'y a plus rien à rendre)

      while (itler.hasNext()) {
        Equipement e = itler.next();
        atLeastOne = true;
        if (e.isLumiere()) {
          jeu.cartesLumieres.defausser(e);
          itler.remove(); // facultatif ?
        } else {
          jeu.cartesTenebres.defausser(e);
          itler.remove();
        }
      }
      if (atLeastOne) { // atLeastOne pourquoi cette merde fonctionne
        resolveLastSteps();
      }
    } else {
      lieuController.stepFinEffetLieu();
    }
  }

  public void stepSeRevelerOuNon(String reponse) {
    if (reponse.equals("o")) {
      getJoueurTourJoueur().revele();
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(getJoueurTourJoueur()));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Le joueur "
                  + (jeu.getTourJoueur() + 1)
                  + " a choisi de se révéler. C'est un/une "
                  + jeu.joueurs.get(jeu.getTourJoueur()).getPersonnage().getNom()
                  + "."));
    }
    Personnage personnage = jeu.joueurs.get(jeu.getTourJoueur()).getPersonnage();

    if (jeu.joueurs.get(jeu.getTourJoueur()).isRevele()
        && !jeu.joueurs.get(jeu.getTourJoueur()).getHaveUseCapaciteSpecial()
        && (personnage.equals(PersonnageFactory.Hunters.Franklin)
        || personnage.equals(PersonnageFactory.Hunters.Georges)
        || personnage.equals(PersonnageFactory.Neutres.Allie)
        || (personnage.equals(PersonnageFactory.Hunters.Emi)
        && jeu.joueurs.get(jeu.getTourJoueur()).getLieuCourrant() != null))) {
      this.socketManager.sendToPlayer(CommandeFactory.utiliserCapacite(), jeu.getTourJoueur() + 1);
    } else {
      this.socketManager.sendToPlayer(CommandeFactory.lancerDes(), jeu.getTourJoueur() + 1);
    }
  }

  public void stepVerifLoupGarou(int joueur, int attaquant) {
    List<Joueur> joueursAPortee =
        jeu.terrain.getJoueursPeutAttaque(
            jeu.joueurs.get(jeu.getTourJoueur()),
            jeu.joueurs.get(jeu.getTourJoueur()).possedeEqtdeNom("Revolver des ténèbres"));
    List<Integer> verifier = new ArrayList<>();
    if (joueur == 42) { // Mitrailleuse case
      for (int i = 1; i <= this.jeu.joueurs.size(); i++) {
        if (i != jeu.getTourJoueur() + 1 && joueursAPortee.contains(jeu.joueurs.get(i - 1))) {
          verifier.add(i);
        }
      }
    } else {
      verifier.add(joueur);
    }
    Joueur joueurAttaque;
    for (Integer joueursAVerifier : verifier) { // most of cases there is only one player in list
      joueurAttaque = jeu.joueurs.get(joueursAVerifier - 1);
      if (joueurAttaque.isRevele()
          && joueurAttaque.getPersonnage().equals(PersonnageFactory.Shadows.LoupGarou)
          && !joueurAttaque.isDead()) {
        socketManager.sendToEveryBody(CommandeFactory.logInfo("Le Loup-Garou a été attaqué !"));
        socketManager.sendToPlayer(CommandeFactory.capaciteLoupGarou(attaquant), joueursAVerifier);
      }
    }
  }

  public void stepUtiliserCapacite(String reponse) {
    if (reponse.equals("o")) {
      Joueur joueurActuel = jeu.joueurs.get(jeu.getTourJoueur());
      joueurActuel.setHaveUseCapaciteSpecial(true);
      this.socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "J"
                  + (jeu.getTourJoueur() + 1)
                  + " ("
                  + joueurActuel.getPersonnage().getNom()
                  + ") utilise sa capacite special !"));
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(jeu.joueurs.get(jeu.getTourJoueur())));

      if (joueurActuel.getPersonnage().equals(PersonnageFactory.Neutres.Allie)) {
        // capacite Allie
        joueurActuel.retirerBlessures(joueurActuel.getNbBlessures());
        socketManager.sendToEveryBody(CommandeFactory.updateJoueur(joueurActuel));
        socketManager.sendToEveryBody(
            CommandeFactory.logInfo("Allie a soigné toutes ses Blessures"));
        this.socketManager.sendToPlayer(CommandeFactory.lancerDes(), jeu.getTourJoueur() + 1);
      } else if (joueurActuel.getPersonnage().equals(PersonnageFactory.Hunters.Franklin)) {
        // capacite Franklin
        this.socketManager.sendToPlayer(
            CommandeFactory.capaciteFranklin(), jeu.getTourJoueur() + 1);
      } else if (joueurActuel.getPersonnage().equals(PersonnageFactory.Hunters.Georges)) {
        // capacite george
        this.socketManager.sendToPlayer(
            CommandeFactory.capaciteGeorges(),
            jeu.getTourJoueur() + 1);
      } else if (joueurActuel.getPersonnage().equals(PersonnageFactory.Hunters.Emi)) {
        this.socketManager.sendToPlayer(CommandeFactory.capaciteEmi(), jeu.getTourJoueur() + 1);
      }
    } else {
      this.socketManager.sendToPlayer(CommandeFactory.lancerDes(), jeu.getTourJoueur() + 1);
    }
  }

  public void stepCapaciteFranklin(int choix) {
    int resultD6 = jeu.des.lancerD6();
    this.socketManager.sendToEveryBody(
        CommandeFactory.logInfo(
            "Franklin (J"
                + (jeu.getTourJoueur() + 1)
                + ") a fait "
                + resultD6
                + " Blessures a J"
                + choix));
    jeu.joueurs.get(choix - 1).ajouterBlessures(resultD6);
    socketManager.sendToEveryBody(CommandeFactory.updateJoueur(jeu.joueurs.get(choix - 1)));
    this.socketManager.sendToPlayer(CommandeFactory.lancerDes(), jeu.getTourJoueur() + 1);
  }

  public void stepCapaciteGeorges(int choix) {
    int resultD4 = jeu.des.lancerD4();
    this.socketManager.sendToEveryBody(
        CommandeFactory.logInfo(
            "Georges (J"
                + (jeu.getTourJoueur() + 1)
                + ") a fait "
                + resultD4
                + " Blessures a J"
                + choix));
    jeu.joueurs.get(choix - 1).ajouterBlessures(resultD4);
    socketManager.sendToEveryBody(CommandeFactory.updateJoueur(jeu.joueurs.get(choix - 1)));
    this.socketManager.sendToPlayer(CommandeFactory.lancerDes(), jeu.getTourJoueur() + 1);
  }

  public void stepCapaciteEmi(String choix) {
    if (choix.equals("a")) {
      Lieu lieuActuel = this.jeu.terrain.getLieuJoueur(getJoueurTourJoueur());
      if (lieuActuel != null) {
        Lieu lieuVoisin = this.jeu.terrain.getLieuVoisin(lieuActuel);
        lieuController.stepLieuResponse(lieuVoisin);
      } else {
        this.socketManager.sendToPlayer(
            CommandeFactory.logInfo(
                "Vous n'êtes pas sur le plateau. Vous êtes obligé de " + "lancer les dés."),
            jeu.getTourJoueur() + 1);
        lieuController.stepLieu();
      }
    } else {
      lieuController.stepLieu();
    }
  }

  public void stepTestFinDeJeu(boolean attaque) {
    int hunterSurvivant = 0;
    int shadoSurvivant = 0;
    boolean partieTerminee = false;
    Joueur joueurAllie = null;
    Joueur joueurDaniel = null;
    Joueur joueurBob = null;
    Joueur joueurCharles = null;
    for (Joueur joueur : this.jeu.joueurs) {
      if (!joueur.isDead()) {
        if (joueur.getPersonnage().getFaction() == Faction.HUNTER) {
          hunterSurvivant++;
        } else if (joueur.getPersonnage().getFaction() == Faction.SHADOW) {
          shadoSurvivant++;
        } else {
          if (joueur.getPersonnage() == PersonnageFactory.Neutres.Allie) {
            joueurAllie = joueur;
          } else if (joueur.getPersonnage() == PersonnageFactory.Neutres.Daniel) {
            joueurDaniel = joueur;
          } else if (joueur.getPersonnage() == PersonnageFactory.Neutres.Charles) {
            joueurCharles = joueur;
          } else if (joueur.getPersonnage() == PersonnageFactory.Neutres.Bob) {
            joueurBob = joueur;
          }
        }
      }
    }
    if (hunterSurvivant == 0 && shadoSurvivant == 0
        && joueursMort.size() == this.jeu.joueurs.size()) {
      this.socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Les Shadows et les Hunters sont mort ! \n" + "Personne n'a gagné !"));
      partieTerminee = true;
    } else if (hunterSurvivant == 0) {
      this.socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Les Hunters sont mort ! \n" + "Les Shadows ont gagné !"));
      partieTerminee = true;
    } else if (shadoSurvivant == 0) {
      this.socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "Les Shadows sont mort ! \n" + "Les hunters ont gagné !"));
      partieTerminee = true;
    }

    if ((joueursMort.size() > 0
        && joueursMort.get(0).getPersonnage().equals(PersonnageFactory.Neutres.Daniel))
        || (shadoSurvivant == 0 && joueurDaniel != null && !joueurDaniel.isDead())) {
      // Daniel est mort le premier OU n'est pas mort et tous les Shadows sont morts
      CommandeFactory.logInfo("Daniel a gagné !");
      partieTerminee = true;
    }

    if (joueursMort.size() >= 3 && attaque) {
      Joueur attaquant = jeu.joueurs.get(jeu.getTourJoueur());
      if (attaquant.getPersonnage().equals(PersonnageFactory.Neutres.Charles)) {
        //
        CommandeFactory.logInfo("Charles a gagné !");
        partieTerminee = true;
      }
    }

    if (joueurBob != null && joueurBob.getEqts().size() >= 5) {
      // Bob possède 5 cartes équipement
      // La partie est donc terminée
      partieTerminee = true;
      CommandeFactory.logInfo("Bob possède 5 équiments, il a gagné !");
    }

    if (partieTerminee) {
      if (joueurAllie != null && !joueurAllie.isDead()) {
        // Allie n'est pas morte à la fin de la partie
        // Elle a donc gagné
        this.socketManager.sendToEveryBody(
            CommandeFactory.logInfo("Allie est toujours vivante, elle a aussi gagné !"));
      }

      stepFinPartie();
    }
  }

  public void stepCapaciteLoupGarou(int attaquant, int loupGarou, String choix) {
    if (choix.equals("o")) {
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo("Le Loup-Garou contre attaque J" + attaquant + " !"));
      Joueur joueurjAttaquant = jeu.joueurs.get(attaquant - 1);
      int degats = jeu.des.lancerDe6MoinsD4(0) - joueurjAttaquant.getModificateurDefense();
      joueurjAttaquant.ajouterBlessures(degats);
      socketManager.sendToEveryBody(
          CommandeFactory.updateJoueur(joueurjAttaquant));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo("J" + attaquant + " a subit " + degats + " Blessures !"));
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              "J" + attaquant + " a " + joueurjAttaquant.getNbBlessures() + " Blessures."));
    }
    resolveLastSteps();
  }

  public void resolveLastSteps() {
    System.out.println("resolve steps");
    if (!stepsToResolve.empty()) {
      Step step = stepsToResolve.pop();
      System.out.println("There is a step! " + step);
      step.callStep();
    } else {
      stepEndTurn();
    }
  }

  private void stepEndTurn() {
    this.stepTestFinDeJeu(false);
    this.nextTurn();
    this.stepNewTurn();
  }

  private void sendTurn() {
    this.socketManager.sendToEveryBody(CommandeFactory.turn(jeu.getTourJoueur() + 1));
  }

  private void seReveler() {
    this.socketManager.sendToPlayer(CommandeFactory.seReveler(), jeu.getTourJoueur() + 1);
  }

  private void nextTurn() {
    Carte carte = jeu.derniereCarteTiree;
    if (jeu.derniereCarteTiree != null && "Savoir Encestral".equals(carte.getNom())) {
      socketManager.sendToEveryBody(
          CommandeFactory.logInfo(
              this.jeu.joueurs.get(jeu.getTourJoueur()).getIdJoueur() + " rejoues un autre tour"));
      jeu.derniereCarteTiree = null;
    } else {
      do {
        jeu.incrementTourJoueur();
      } while (this.jeu.joueurs.get(jeu.getTourJoueur()).isDead());
    }
  }

  private void stepFinPartie() {
    this.socketManager.sendToEveryBody(CommandeFactory.finPartie());
    System.out.println("Fin de la partie --> Déconnexion");
    System.exit(0);
  }

  public void chat(String message, int joueurChat) {
    socketManager.sendToOtherPlayers(CommandeFactory.chat(message, joueurChat), joueurChat);
  }
}
