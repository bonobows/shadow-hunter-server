package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.bonobows.shadowhunters.server.model.De;
import org.junit.Before;
import org.junit.Test;

public class DeTest {

  De de;
  int nbLancer = 100;

  @Before
  public void setUp() throws Exception {
    de = new De();
  }

  @Test
  public void bonNombrefaceParDefaut() throws Exception {
    assertEquals(6, de.getFaces());
  }

  @Test
  public void lancerDe() throws Exception {
    for (int i = 0; i < nbLancer; i++) {
      int lance = de.lancerDe();
      assertTrue(lance >= 1 && lance <= 6);
    }
  }
}
