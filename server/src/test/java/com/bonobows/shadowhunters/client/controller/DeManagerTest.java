package com.bonobows.shadowhunters.client.controller;

import static org.junit.Assert.assertTrue;

import com.bonobows.shadowhunters.server.controller.DeManager;
import org.junit.Before;
import org.junit.Test;

public class DeManagerTest {

  DeManager deManager;
  int nbLancer = 100;

  @Before
  public void setUp() throws Exception {
    deManager = new DeManager();
  }

  @Test
  public void lancerD4() throws Exception {
    for (int i = 0; i < nbLancer; i++) {
      int lance = deManager.lancerD4();
      assertTrue(lance >= 1 && lance <= 4);
    }
  }

  @Test
  public void lancerD6() throws Exception {
    for (int i = 0; i < nbLancer; i++) {
      int lance = deManager.lancerD6();
      assertTrue(lance >= 1 && lance <= 6);
    }
  }

  @Test
  public void lancerDes() throws Exception {
    for (int i = 0; i < nbLancer; i++) {
      int lance = deManager.lancerDes();
      assertTrue(lance >= 1 && lance <= 10);
    }
  }

  @Test
  public void lancerDe6MoinsD4() throws Exception {
    for (int i = 0; i < nbLancer; i++) {
      int lance = deManager.lancerDe6MoinsD4(0);
      assertTrue(lance >= 0 && lance <= 5);
    }
  }
}