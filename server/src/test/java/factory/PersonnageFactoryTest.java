package factory;

import static org.junit.Assert.assertEquals;

import com.bonobows.shadowhunters.server.exception.MauvaisNombreJoueurException;
import com.bonobows.shadowhunters.server.factory.PersonnageFactory;
import org.junit.Test;

public class PersonnageFactoryTest {

  @Test(expected = MauvaisNombreJoueurException.class)
  public void generatePersonnageException() throws Exception {
    PersonnageFactory.generatePersonnage(2);
  }

  @Test
  public void generatePersonnage() throws Exception {
    assertEquals(4, PersonnageFactory.generatePersonnage(4).size());
  }

}