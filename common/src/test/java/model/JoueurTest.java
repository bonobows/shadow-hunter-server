package model;

import static org.junit.Assert.assertEquals;

import com.bonobows.shadowhunters.common.model.Couleur;
import com.bonobows.shadowhunters.common.model.Joueur;
import org.junit.Before;
import org.junit.Test;

public class JoueurTest {
  @Before
  public void setUp() throws Exception {}

  @Test
  public void testAjouterBlessures() throws Exception {
    int nbBlessuresExpected = 5;

    Joueur joueur = new Joueur(Couleur.VIOLET);
    joueur.ajouterBlessures(5);
    assertEquals(nbBlessuresExpected, joueur.getNbBlessures());
  }
}
