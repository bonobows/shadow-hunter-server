package factory;

import static org.junit.Assert.assertEquals;

import com.bonobows.shadowhunters.common.factory.CarteVisionFactory;
import org.junit.Test;

public class CarteVisionFactoryTest {
  @Test
  public void generatePiocheVision() throws Exception {
    assertEquals(16, CarteVisionFactory.generatePiocheVision().size());
  }
}
