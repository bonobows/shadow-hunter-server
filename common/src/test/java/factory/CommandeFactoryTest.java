package factory;

import static org.junit.Assert.assertEquals;

import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.common.factory.LieuFactory;
import com.bonobows.shadowhunters.common.model.Faction;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.argument.Argument;
import com.bonobows.shadowhunters.common.model.commande.CodeCommande;
import com.bonobows.shadowhunters.common.model.commande.Commande;
import com.bonobows.shadowhunters.common.model.commande.LogLevel;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;
import com.bonobows.shadowhunters.common.tool.JsonTools;
import org.junit.Test;


public class CommandeFactoryTest {
  @Test
  public void log() throws Exception {
    Commande c = CommandeFactory.log(LogLevel.WARNING, "Test Warning");
    assertEquals(CodeCommande.LOG, c.getCode());
    //    assertEquals(LogLevel.WARNING, c.getArgs().get(Argument.Log.LOG_LEVEL));
    assertEquals("Test Warning", c.getArgs().get(Argument.Log.LOG_MESSAGE));
  }

  @Test
  public void logInfo() throws Exception {
    Commande c = CommandeFactory.logInfo("Test Info");
    assertEquals(CodeCommande.LOG, c.getCode());
    //    assertEquals(LogLevel.INFORMATION, c.getArgs().get(Argument.Log.LOG_LEVEL));
    assertEquals("Test Info", c.getArgs().get(Argument.Log.LOG_MESSAGE));
  }

  @Test
  public void logWarning() throws Exception {
    Commande c = CommandeFactory.logWarning("Test Warning");
    assertEquals(CodeCommande.LOG, c.getCode());
    //    assertEquals(LogLevel.WARNING, c.getArgs().get(Argument.Log.LOG_LEVEL));
    assertEquals("Test Warning", c.getArgs().get(Argument.Log.LOG_MESSAGE));
  }

  @Test
  public void logDebug() throws Exception {
    Commande c = CommandeFactory.logDebug("Test Debug");
    assertEquals(CodeCommande.LOG, c.getCode());
    //    assertEquals(LogLevel.DEBUG, c.getArgs().get(Argument.Log.LOG_LEVEL));
    assertEquals("Test Debug", c.getArgs().get(Argument.Log.LOG_MESSAGE));
  }

  @Test
  public void logError() throws Exception {
    Commande c = CommandeFactory.logError("Test Error");
    assertEquals(CodeCommande.LOG, c.getCode());
    //    assertEquals(LogLevel.ERROR, c.getArgs().get(Argument.Log.LOG_LEVEL));
    assertEquals("Test Error", c.getArgs().get(Argument.Log.LOG_MESSAGE));
  }

  @Test
  public void terrain() throws Exception {
    Terrain terrain = new Terrain();
    Commande c = CommandeFactory.terrain(terrain);
    assertEquals(CodeCommande.TERRAIN, c.getCode());
    assertEquals(
        terrain,
        ((Terrain) JsonTools.toObject(c, Argument.Terrain.TERRAIN_TERRAIN, Terrain.class)));
  }

  @Test
  public void turn() throws Exception {
    Commande c = CommandeFactory.turn(1);
    assertEquals(CodeCommande.TURN, c.getCode());
    assertEquals(1, c.getArgs().get(Argument.Tour.TOUR_JOUEUR));
  }

  @Test
  public void joueur() throws Exception {
    Commande c = CommandeFactory.joueur(1);
    assertEquals(CodeCommande.JOUEUR, c.getCode());
    assertEquals(1, c.getArgs().get(Argument.Joueur.JOUEUR_JOUEUR));
  }

  @Test
  public void personnage() throws Exception {
    Personnage p = new Personnage(10, "Test", "desc", "obj", Faction.SHADOW);
    Commande c = CommandeFactory.personnage(p);
    assertEquals(CodeCommande.PERSONNAGE, c.getCode());
    assertEquals(
        p, JsonTools.toObject(c, Argument.Personnage.PERSONNAGE_PERSONNAGE, Personnage.class));
  }

  @Test
  public void move() throws Exception {
    Lieu l = LieuFactory.generateCimetiere();
    Commande c = CommandeFactory.move(1, l);
    assertEquals(CodeCommande.MOVE, c.getCode());
    assertEquals(1, c.getArgs().get(Argument.Move.MOVE_JOUEUR));
    assertEquals(l, JsonTools.toObject(c, Argument.Move.MOVE_LIEU, Lieu.class));
  }
}
