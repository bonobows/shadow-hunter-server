package factory;

import static org.junit.Assert.assertEquals;

import com.bonobows.shadowhunters.common.factory.LieuFactory;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LieuFactoryTest {
  @Test
  public void genererLieux() throws Exception {
    assertEquals(6, LieuFactory.genererLieux().size());
  }

  @Test
  public void generateCimetiere() throws Exception {
    Lieu l = LieuFactory.generateCimetiere();
    List<Integer> valeursDe = new ArrayList<>();
    valeursDe.add(8);
    assertEquals(valeursDe, l.getValeurDe());
    assertEquals(NomLieu.CIMETIERE, l.getNom());
  }

  @Test
  public void generateSanctuaire() throws Exception {
    Lieu l = LieuFactory.generateSanctuaire();
    List<Integer> valeursDe = new ArrayList<>();
    valeursDe.add(10);
    assertEquals(valeursDe, l.getValeurDe());
    assertEquals(NomLieu.SANCTUAIRE, l.getNom());
  }

  @Test
  public void generateForet() throws Exception {
    Lieu l = LieuFactory.generateForet();
    List<Integer> valeursDe = new ArrayList<>();
    valeursDe.add(9);
    assertEquals(valeursDe, l.getValeurDe());
    assertEquals(NomLieu.FORET, l.getNom());
  }

  @Test
  public void generateMonastere() throws Exception {
    Lieu l = LieuFactory.generateMonastere();
    List<Integer> valeursDe = new ArrayList<>();
    valeursDe.add(6);
    assertEquals(valeursDe, l.getValeurDe());
    assertEquals(NomLieu.MONASTERE, l.getNom());
  }

  @Test
  public void generatePorteOutremonde() throws Exception {
    Lieu l = LieuFactory.generatePorteOutremonde();
    List<Integer> valeursDe = new ArrayList<>();
    valeursDe.add(4);
    valeursDe.add(5);
    assertEquals(valeursDe, l.getValeurDe());
    assertEquals(NomLieu.PORTE_OUTREMONDE, l.getNom());
  }

  @Test
  public void generateAntreHermite() throws Exception {
    Lieu l = LieuFactory.generateAntreHermite();
    List<Integer> valeursDe = new ArrayList<>();
    valeursDe.add(2);
    valeursDe.add(3);
    assertEquals(valeursDe, l.getValeurDe());
    assertEquals(NomLieu.ANTRE_HERMITE, l.getNom());
  }
}
