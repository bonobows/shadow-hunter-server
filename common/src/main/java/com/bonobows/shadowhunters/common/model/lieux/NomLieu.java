package com.bonobows.shadowhunters.common.model.lieux;

public enum NomLieu {
  CIMETIERE("Cimetière"),
  SANCTUAIRE("Sanctuaire ancien"),
  FORET("Forêt hantée"),
  MONASTERE("Monastère"),
  PORTE_OUTREMONDE("Porte de l'Outremonde"),
  ANTRE_HERMITE("Antre de l'hermite");

  private String text;

  NomLieu(String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return text;
  }
}
