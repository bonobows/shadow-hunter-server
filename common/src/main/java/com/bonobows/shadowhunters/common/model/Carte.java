package com.bonobows.shadowhunters.common.model;

public abstract class Carte {
  protected String nom;
  protected String effet;
  protected TypeCarte type;
  // attributs associations cf diag classes
  // 1 jeu

  public Carte(String nom, String effet, TypeCarte type) {
    this.nom = nom;
    this.effet = effet;
    this.type = type;
  }

  // 1 Pioche

  public Carte() {}

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getEffet() {
    return effet;
  }

  public void setEffet(String effet) {
    this.effet = effet;
  }

  public TypeCarte getType() {
    return type;
  }

  public void setType(TypeCarte type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "type=" + type + ", nom='" + nom + '\'' + ", effet='" + effet + '\'';
  }
}
