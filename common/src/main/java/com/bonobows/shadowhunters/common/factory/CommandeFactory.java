package com.bonobows.shadowhunters.common.factory;

import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.CarteLumiereTenebre;
import com.bonobows.shadowhunters.common.model.CarteVision;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.argument.Argument;
import com.bonobows.shadowhunters.common.model.commande.CodeCommande;
import com.bonobows.shadowhunters.common.model.commande.Commande;
import com.bonobows.shadowhunters.common.model.commande.LogLevel;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class CommandeFactory {

  private CommandeFactory() {}

  public static Commande log(LogLevel logLevel, String message) {
    Commande commande = new Commande(CodeCommande.LOG);
    commande.addArg(Argument.Log.LOG_LEVEL, toJson(logLevel));
    commande.addArg(Argument.Log.LOG_MESSAGE, message);
    return commande;
  }

  public static Commande logInfo(String message) {
    return CommandeFactory.log(LogLevel.INFORMATION, message);
  }

  public static Commande logWarning(String message) {
    return CommandeFactory.log(LogLevel.WARNING, message);
  }

  public static Commande logDebug(String message) {
    return CommandeFactory.log(LogLevel.DEBUG, message);
  }

  public static Commande logError(String message) {
    return CommandeFactory.log(LogLevel.ERROR, message);
  }

  public static Commande attac(
      List<Joueur> joueursAPortee, boolean possedeSabre, boolean possedeMitrailleuse) {
    Commande commande = new Commande(CodeCommande.ATTAC);
    List<String> str = new ArrayList<>();
    for (Joueur j : joueursAPortee) {
      str.add(toJson(j));
    }
    commande.addArg(Argument.Attac.JOUEURS_A_PORTEE, toJson(str));
    commande.addArg(Argument.Attac.POSSEDE_SABRE, possedeSabre);
    commande.addArg(Argument.Attac.POSSEDE_MITRAILLEUSE, possedeMitrailleuse);

    return commande;
  }

  public static Commande attac(int attaque) {
    Commande commande = new Commande(CodeCommande.ATTAC);
    commande.addArg(Argument.Attac.ATTAQUE_ATTAQUE, attaque);
    return commande;
  }

  public static Commande attac(int attaquant, int attaque) {
    Commande commande = new Commande(CodeCommande.ATTAC);
    commande.addArg(Argument.Attac.ATTAQUE_ATTAQUANT, attaquant);
    commande.addArg(Argument.Attac.ATTAQUE_ATTAQUE, attaque);
    return commande;
  }

  public static Commande sendJoueurs(List<Joueur> joueursAPortee) {
    Commande commande = new Commande(CodeCommande.SEND_JOUEURS);
    List<String> str = new ArrayList<>();
    for (Joueur j : joueursAPortee) {
      str.add(toJson(j));
    }
    commande.addArg(Argument.SendJoueurs.SEND_JOEURS_JOUEURS, toJson(str));
    return commande;
  }

  public static Commande terrain(Terrain terrain) {
    Commande commande = new Commande(CodeCommande.TERRAIN);
    commande.addArg(Argument.Terrain.TERRAIN_TERRAIN, toJson(terrain));
    return commande;
  }

  public static Commande turn() {
    return new Commande(CodeCommande.TURN);
  }

  public static Commande turn(int joueur) {
    Commande commande = new Commande(CodeCommande.TURN);
    commande.addArg(Argument.Tour.TOUR_JOUEUR, joueur);
    return commande;
  }

  public static Commande seReveler() {
    Commande commande = new Commande(CodeCommande.SE_REVELER);
    return commande;
  }

  public static Commande seReveler(String reponse) {
    Commande commande = new Commande(CodeCommande.SE_REVELER);
    commande.addArg(Argument.SeReveler.SE_REVELER_REPONSE, reponse);
    return commande;
  }

  public static Commande lancerDes() {
    Commande commande = new Commande(CodeCommande.LANCER_DES);
    return commande;
  }

  public static Commande utiliserCapacite() {
    Commande commande = new Commande(CodeCommande.UTILISER_CAPACITE);
    return commande;
  }

  public static Commande utiliserCapacite(String reponse) {
    Commande commande = new Commande(CodeCommande.UTILISER_CAPACITE);
    commande.addArg(Argument.UtiliserCapacite.UTILISER_CAPACITE_REPONSE, reponse);
    return commande;
  }

  public static Commande capaciteFranklin() {
    Commande commande = new Commande(CodeCommande.CAPACITE_FRANKLIN);
    return commande;
  }

  public static Commande capaciteFranklin(int choix) {
    Commande commande = new Commande(CodeCommande.CAPACITE_FRANKLIN);
    commande.addArg(Argument.CapaciteFranklin.CAPACITE_FRANKLIN_CHOIX, choix);
    return commande;
  }

  public static Commande capaciteGeorges() {
    Commande commande = new Commande(CodeCommande.CAPACITE_GEORGES);
    return commande;
  }

  public static Commande capaciteGeorges(int choix) {
    Commande commande = new Commande(CodeCommande.CAPACITE_GEORGES);
    commande.addArg(Argument.CapaciteGeorges.CAPACITE_GEORGES_CHOIX, choix);
    return commande;
  }

  public static Commande capaciteLoupGarou(int attaquant) {
    Commande commande = new Commande(CodeCommande.CAPACITE_LOUP_GAROU);
    commande.addArg(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU_ATTAQUANT, attaquant);
    return commande;
  }

  public static Commande capaciteLoupGarou(int attaquant, int loupGarou, String choix) {
    Commande commande = new Commande(CodeCommande.CAPACITE_LOUP_GAROU);
    commande.addArg(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU_ATTAQUANT, attaquant);
    commande.addArg(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU, loupGarou);
    commande.addArg(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU_CHOIX, choix);
    return commande;
  }

  public static Commande capaciteEmi() {
    return new Commande(CodeCommande.CAPACITE_EMI);
  }

  public static Commande capaciteEmi(char choix) {
    Commande commandeEmi = new Commande(CodeCommande.CAPACITE_EMI);
    commandeEmi.addArg(Argument.CapaciteEmi.CAPACITE_EMI, choix);
    return commandeEmi;
  }

  public static Commande joueur(int joueur) {
    Commande commande = new Commande(CodeCommande.JOUEUR);
    commande.addArg(Argument.Joueur.JOUEUR_JOUEUR, joueur);
    return commande;
  }

  public static Commande personnage(Personnage personnage) {
    Commande commande = new Commande(CodeCommande.PERSONNAGE);
    commande.addArg(Argument.Personnage.PERSONNAGE_PERSONNAGE, toJson(personnage));
    return commande;
  }

  public static Commande move(int joueur, Lieu l) {
    Commande commande = new Commande(CodeCommande.MOVE);
    commande.addArg(Argument.Move.MOVE_JOUEUR, joueur);
    commande.addArg(Argument.Move.MOVE_LIEU, toJson(l));
    return commande;
  }

  public static Commande de4(int de6) {
    Commande commande = new Commande(CodeCommande.DE);
    commande.addArg(Argument.ArgDe.DE_SIX, de6);
    return commande;
  }

  public static Commande de6(int de4) {
    Commande commande = new Commande(CodeCommande.DE);
    commande.addArg(Argument.ArgDe.DE_QUATRE, de4);
    return commande;
  }

  public static Commande de(int de6, int de4, int total) {
    Commande commande = new Commande(CodeCommande.DE);
    commande.addArg(Argument.ArgDe.DE_SIX, de6);
    commande.addArg(Argument.ArgDe.DE_QUATRE, de4);
    commande.addArg(Argument.ArgDe.DE_TOTAL, total);
    return commande;
  }

  public static Commande resBoussole(int[] res1, int[] res2) {
    Commande commande = new Commande(CodeCommande.RES_BOUSSOLE);
    commande.addArg(Argument.ArgDe.DE_SIX, res1[0]);
    commande.addArg(Argument.ArgDe.DE_QUATRE, res1[1]);
    commande.addArg(Argument.ArgDe.DE_TOTAL, res1[2]);
    commande.addArg(Argument.ArgDe.DE_SIX_2, res2[0]);
    commande.addArg(Argument.ArgDe.DE_QUATRE_2, res2[1]);
    commande.addArg(Argument.ArgDe.DE_TOTAL_2, res2[2]);
    return commande;
  }

  public static Commande resDeChoix(int possibilite1, int possibilite2) {
    Commande commande = new Commande(CodeCommande.DECHOIX);
    commande.addArg(Argument.ArgDe.DE_TOTAL, possibilite1);
    commande.addArg(Argument.ArgDe.DE_TOTAL_2, possibilite2);
    return commande;
  }

  public static Commande resBoussoleChoisi(int choix) {
    Commande commande = new Commande(CodeCommande.RES_BOUSSOLE_CHOISI);
    commande.addArg(Argument.ArgDe.DE_CHOIX_RES_BOUSSOLE, choix);
    return commande;
  }

  public static Commande lieuChoix() {
    return new Commande(CodeCommande.LIEU_CHOIX);
  }

  public static Commande lieuChoix(NomLieu typeLieu) {
    Commande c = CommandeFactory.lieuChoix();
    c.addArg(Argument.LieuChoix.LIEU_CHOIX, toJson(typeLieu));
    return c;
  }

  public static Commande effetLieu(NomLieu typeLieu) {
    Commande c = new Commande(CodeCommande.EFFET_LIEU);
    c.addArg(Argument.EffetLieu.EFFET_LIEU_TYPE, toJson(typeLieu));
    return c;
  }

  public static Commande effetLieu(NomLieu typeLieu, int choix) {
    Commande c = CommandeFactory.effetLieu(typeLieu);
    c.addArg(Argument.EffetLieu.EFFET_LIEU_CHOIX, choix);
    return c;
  }

  public static Commande effetLieu(NomLieu typeLieu, Carte carte) {
    Commande c = CommandeFactory.effetLieu(typeLieu);
    c.addArg(Argument.EffetLieu.EFFET_LIEU_CARTE, toJson(carte));
    return c;
  }

  public static Commande showCardLt(Carte carte) {
    Commande c = new Commande(CodeCommande.SHOW_CARD_LT);
    c.addArg(Argument.ShowCardLt.SHOW_CARD_LT_CARTE, toJson(carte));
    return c;
  }

  public static Commande vision(CarteVision carte, int joueur, List<ActionVision> choix) {
    Commande c = new Commande(CodeCommande.VISION);
    c.addArg(Argument.Vision.VISION_CARTE, toJson(carte));
    c.addArg(Argument.Vision.VISION_JOUEUR, joueur);
    List<String> choixJson = new ArrayList<>();
    for (ActionVision actionVision : choix) {
      choixJson.add(toJson(actionVision));
    }
    c.addArg(Argument.Vision.VISION_CHOIX, toJson(choixJson));
    return c;
  }

  public static Commande vision(
      CarteVision carte, int joueur, List<ActionVision> choix, List<Equipement> equipements) {
    Commande commande = CommandeFactory.vision(carte, joueur, choix);
    List<String> choixJson = new ArrayList<>();
    for (Equipement equipement : equipements) {
      choixJson.add(toJson(equipement));
    }
    commande.addArg(Argument.Vision.VISION_EQUIPEMENTS, toJson(choixJson));
    return commande;
  }

  public static Commande vision(ActionVision choix) {
    Commande c = new Commande(CodeCommande.VISION);

    c.addArg(Argument.Vision.VISION_CHOISI, toJson(choix));
    return c;
  }

  public static Commande vision(ActionVision choix, Equipement equipementChoisi) {
    Commande commande = CommandeFactory.vision(choix);
    commande.addArg(Argument.Vision.VISION_EQUIPEMENT_CHOISI, toJson(equipementChoisi));
    return commande;
  }

  public static Commande cibleVolChoix(ArrayList<Integer> possibilites) {
    Commande c = new Commande(CodeCommande.CIBLE_VOL_CHOIX);
    c.addArg(Argument.Sanctuaire.SANCTUAIRE_JOUEURS_A_VOLER, toJson(possibilites));
    return c;
  }

  public static Commande cibleVol(int choix) {
    Commande c = new Commande(CodeCommande.CIBLE_VOL);
    c.addArg(Argument.Sanctuaire.SANCTUAIRE_JOUEUR_CIBLE, toJson(choix));
    return c;
  }

  public static Commande finPartie() {
    Commande c = new Commande(CodeCommande.FIN_PARTIE);
    return c;
  }

  public static Commande goChoixVol(List<Equipement> possibilites) {
    Commande c = new Commande(CodeCommande.GO_CHOISIR_EQT_A_VOLER);

    ArrayList<String> eqts = new ArrayList<>();
    for (Equipement e : possibilites) {
      eqts.add(toJson(e));
    }

    c.addArg(Argument.Sanctuaire.SANCTUAIRE_EQTS_A_VOLER, toJson(eqts));
    return c;
  }

  public static Commande choixVol(int choix) {
    Commande c = new Commande(CodeCommande.EQT_CHOISI);
    c.addArg(Argument.Sanctuaire.SANCTUAIRE_EQT_CHOISI, toJson(choix));
    return c;
  }

  public static Commande personnageVision(Personnage personnage) {
    Commande commande = new Commande(CodeCommande.PERSONNAGE_VISION);
    commande.addArg(Argument.Personnage.PERSONNAGE_PERSONNAGE, toJson(personnage));
    return commande;
  }

  public static Commande updateJoueur(Joueur joueur) {
    Commande commande = new Commande(CodeCommande.UPDATE_JOUEUR);
    commande.addArg(Argument.UpdateJoueur.UPDATE_JOUEUR_JOUEUR, toJson(joueur));
    return commande;
  }

  public static Commande carteImmediate(List<Equipement> equipements) {
    Commande commande = new Commande(CodeCommande.CARTE_IMMEDIATE);
    List<String> equipementsNames = new ArrayList<>();
    for (Equipement e : equipements) {
      equipementsNames.add(e.getNom());
    }
    commande.addArg(
        Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_EQUIPEMENT, toJson(equipementsNames));
    return commande;
  }

  public static Commande carteImmediate(int choixEqt) {
    Commande commande = new Commande(CodeCommande.CARTE_IMMEDIATE);
    commande.addArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_EQUIPEMENT, choixEqt);
    return commande;
  }

  public static Commande carteImmediate(int choixEqt, int choixJoueur) {
    Commande commande = CommandeFactory.carteImmediate(choixEqt);
    commande.addArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_JOUEUR, choixJoueur);
    return commande;
  }

  public static Commande carteImmediate(long choixJoueur) {
    Commande commande = new Commande(CodeCommande.CARTE_IMMEDIATE);
    commande.addArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_JOUEUR, choixJoueur);
    return commande;
  }

  public static Commande chat(String message) {
    Commande commande = new Commande(CodeCommande.CHAT);
    commande.addArg(Argument.Chat.CHAT_MESSAGE, message);
    return commande;
  }

  public static Commande chat(String message, int joueur) {
    Commande commande = CommandeFactory.chat(message);
    commande.addArg(Argument.Chat.CHAT_JOUEUR, joueur);
    return commande;
  }

  public static Commande sePresenter(String pseudo) {
    Commande commande = new Commande(CodeCommande.SE_PRESENTER);
    commande.addArg(Argument.SePresenter.SE_PRESENTER_PSEUDO, pseudo);
    return commande;
  }

  public static Commande demarrerPartie() {
    return new Commande(CodeCommande.DEMARRER_PARTIE);
  }

  public static Commande mortJoueur(Joueur j) {
    Commande commande = new Commande(CodeCommande.MORT_JOUEUR);
    commande.addArg(Argument.MortJoueur.MORT_JOUEUR_ID, toJson(j));
    return commande;
  }

  private static String toJson(Object obj) {
    try {
      ObjectMapper mapper = new ObjectMapper();

      return mapper.writeValueAsString(obj);
    } catch (JsonProcessingException ex) {
      ex.printStackTrace();
      return null;
    }
  }

}
