package com.bonobows.shadowhunters.common.model;

public enum TypeCarte {
  VISION,
  LUMIERE,
  TENEBRE,
}
