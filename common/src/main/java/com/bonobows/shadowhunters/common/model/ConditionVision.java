package com.bonobows.shadowhunters.common.model;

public enum ConditionVision {
  SHADOW,
  NEUTRE,
  HUNTER,
  SHADOW_NEUTRE,
  SHADOW_HUNTER,
  HUNTER_NEUTRE,
  PV_PLUS_DOUZE,
  PV_MOINS_ONZE,
  NOTEST;

  @Override
  public String toString() {
    switch (this) {
      case SHADOW:
        return "Je pense que tu es SHADOW";
      case NEUTRE:
        return "Je pense que tu es NEUTRE";
      case HUNTER:
        return "Je pense que tu es HUNTER";
      case SHADOW_HUNTER:
        return "Je pense que tu es SHADOW ou HUNTER";
      case HUNTER_NEUTRE:
        return "Je pense que tu es NEUTRE ou HUNTER";
      case SHADOW_NEUTRE:
        return "Je pense que tu es NEUTRE ou SHADOW";
      case PV_MOINS_ONZE:
        return "Je pense que tu es un personnage qui a 11 Points de Vie ou moins";
      case PV_PLUS_DOUZE:
        return "Je pense que tu es un personnage qui a 12 Points de Vie ou plus";
      default:
        return "";
    }
  }
}
