package com.bonobows.shadowhunters.common.model;

public enum EffetVision {
  CONDITIONNEL,
  BOE, // Blessure ou Equipement
  BLESSURE,
  DEUX_BLESSURES,
  SUPREME;

  @Override
  public String toString() {
    switch (this) {
      case BOE:
        return "Si c'est le case tu dois : "
            + "sois me donner une carte equipement sois subir 1 blessure";
      case BLESSURE:
        return "si c'est le cas, subis 1 Blessure !";
      case DEUX_BLESSURES:
        return "si c'est le cas, subis 2 Blessures !";
      case CONDITIONNEL:
        return "Si c'est le cas, soigne 1 Blessure. "
            + "(Toutefois si tu n'avais aucune blessure subis 1 blessure !)";
      case SUPREME:
        return "Montre moi secrètement ta carte personnage";
      default:
        return "";
    }
  }
}
