package com.bonobows.shadowhunters.common.model.lieux;

import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;

import java.util.ArrayList;
import java.util.List;

public class Lieu {
  private NomLieu nom;
  private String description;
  private List<Integer> valeurDe;
  // private String image;
  // atributs d'associations cf diag de classes
  private List<Joueur> joueurs; // entre 0 et 8
  // 1 terrain
  // entre 0 et 3 pioches de types différents

  public Lieu() {}

  public Lieu(NomLieu nom, String description, List<Integer> valeurDe) {
    this.nom = nom;
    this.description = description;
    this.valeurDe = valeurDe;
    this.joueurs = new ArrayList<>();
  }

  public boolean bonneValeurDuDe(int valeurDe) {
    return this.valeurDe.contains(valeurDe);
  }

  @Override
  public String toString() {
    return "Lieu{"
        + "nom='"
        + nom.toString()
        + '\''
        + ", description='"
        + description
        + '\''
        + ", valeurDe="
        + valeurDe
        + ", joueurs="
        + joueurs
        + '}';
  }

  public void addJoueur(Joueur j) {
    this.joueurs.add(j);
  }

  public void removeJoueur(Joueur j) {

    for (Joueur joueur : joueurs) {
      if (joueur.getIdJoueur() == j.getIdJoueur()) {
        this.joueurs.remove(joueur);
        return;
      }
    }
  }

  public boolean hasPersonnage(Personnage p) {
    for (Joueur j : joueurs) {
      if (j.getPersonnage().equals(p)) {
        return true;
      }
    }
    return false;
  }

  public boolean hasJoueur(Joueur j) {
    for (Joueur joueur : joueurs) {
      if (joueur.getIdJoueur() == j.getIdJoueur()) {
        return true;
      }
    }
    return false;
  }

  public List<Joueur> getJoueurs() {
    return joueurs;
  }

  public NomLieu getNom() {
    return nom;
  }

  public String getDescription() {
    return description;
  }

  public List<Integer> getValeurDe() {
    return valeurDe;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Lieu lieu = (Lieu) o;

    if (!getNom().equals(lieu.getNom())) {
      return false;
    }
    if (!getDescription().equals(lieu.getDescription())) {
      return false;
    }
    if (getValeurDe() != null
        ? !getValeurDe().equals(lieu.getValeurDe())
        : lieu.getValeurDe() != null) {
      return false;
    }
    return getJoueurs() != null
        ? getJoueurs().equals(lieu.getJoueurs())
        : lieu.getJoueurs() == null;
  }

  @Override
  public int hashCode() {
    int result = getNom().hashCode();
    result = 31 * result + getNom().hashCode();
    result = 31 * result + getDescription().hashCode();
    result = 31 * result + (getValeurDe() != null ? getValeurDe().hashCode() : 0);
    result = 31 * result + (getJoueurs() != null ? getJoueurs().hashCode() : 0);
    return result;
  }
}
