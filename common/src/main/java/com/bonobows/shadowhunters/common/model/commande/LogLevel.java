package com.bonobows.shadowhunters.common.model.commande;

public enum LogLevel {
  INFORMATION,
  WARNING,
  DEBUG,
  ERROR;

  @Override
  public String toString() {
    switch (this) {
      case INFORMATION:
        return "INFO";
      case WARNING:
        return "WARN";
      case DEBUG:
        return "DEBUG";
      case ERROR:
        return "ERR";
      default:
        return "";
    }
  }
}
