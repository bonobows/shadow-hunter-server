package com.bonobows.shadowhunters.common.model.argument;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

import java.io.IOException;

public class ArgumentDeserializer extends KeyDeserializer {

  @Override
  public Argument deserializeKey(String key, DeserializationContext ctxt) throws IOException {
    if (Argument.contains(Argument.Log.class, key)) {
      return Argument.Log.valueOf(key);
    } else if (Argument.contains(Argument.Attac.class, key)) {
      return Argument.Attac.valueOf(key);
    } else if (Argument.contains(Argument.Terrain.class, key)) {
      return Argument.Terrain.valueOf(key);
    } else if (Argument.contains(Argument.Tour.class, key)) {
      return Argument.Tour.valueOf(key);
    } else if (Argument.contains(Argument.Joueur.class, key)) {
      return Argument.Joueur.valueOf(key);
    } else if (Argument.contains(Argument.Personnage.class, key)) {
      return Argument.Personnage.valueOf(key);
    } else if (Argument.contains(Argument.Move.class, key)) {
      return Argument.Move.valueOf(key);
    } else if (Argument.contains(Argument.ArgDe.class, key)) {
      return Argument.ArgDe.valueOf(key);
    } else if (Argument.contains(Argument.LieuChoix.class, key)) {
      return Argument.LieuChoix.valueOf(key);
    } else if (Argument.contains(Argument.EffetLieu.class, key)) {
      return Argument.EffetLieu.valueOf(key);
    } else if (Argument.contains(Argument.Vision.class, key)) {
      return Argument.Vision.valueOf(key);
    } else if (Argument.contains(Argument.SeReveler.class, key)) {
      return Argument.SeReveler.valueOf(key);
    } else if (Argument.contains(Argument.UtiliserCapacite.class, key)) {
      return Argument.UtiliserCapacite.valueOf(key);
    } else if (Argument.contains(Argument.CapaciteGeorges.class, key)) {
      return Argument.CapaciteGeorges.valueOf(key);
    } else if (Argument.contains(Argument.CapaciteFranklin.class, key)) {
      return Argument.CapaciteFranklin.valueOf(key);
    } else if (Argument.contains(Argument.Sanctuaire.class, key)) {
      return Argument.Sanctuaire.valueOf(key);
    } else if (Argument.contains(Argument.CapaciteLoupGarou.class, key)) {
      return Argument.CapaciteLoupGarou.valueOf(key);
    } else if (Argument.contains(Argument.CarteImmediate.class, key)) {
      return Argument.CarteImmediate.valueOf(key);
    } else if (Argument.contains(Argument.CapaciteEmi.class, key)) {
      return Argument.CapaciteEmi.valueOf(key);
    } else if (Argument.contains(Argument.SendJoueurs.class, key)) {
      return Argument.SendJoueurs.valueOf(key);
    } else if (Argument.contains(Argument.UpdateJoueur.class, key)) {
      return Argument.UpdateJoueur.valueOf(key);
    } else if (Argument.contains(Argument.Chat.class, key)) {
      return Argument.Chat.valueOf(key);
    } else if (Argument.contains(Argument.SePresenter.class, key)) {
      return Argument.SePresenter.valueOf(key);
    } else if (Argument.contains(Argument.ShowCardLt.class, key)) {
      return Argument.ShowCardLt.valueOf(key);
    } else if (Argument.contains(Argument.MortJoueur.class, key)) {
      return Argument.MortJoueur.valueOf(key);
    }
    return null;
  }
}
