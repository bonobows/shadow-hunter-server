package com.bonobows.shadowhunters.common.factory;

import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Immediat;
import com.bonobows.shadowhunters.common.model.TypeCarte;

import java.util.Stack;

/**
 * Tronçonneuse du mal, Si votre attaque inflige des Blessures, la victime subit 1 Blessure en plus.
 * Revolver des ténèbres, Vous pouvez attaquer un joueur présent sur l'un des 4 lieux hors de votre
 * secteur, mais vous ne pouvez plus attaquer un joueur situé dans le même secteur que vous.
 * Mitrailleuse funeste, Votre attaque affecte tous les personnages qui sont à votre portée.
 * Effectuez un seul jet de Blessures pour tous les joueurs concernés. Sabre hanté Masamune, Vous
 * êtes obligé d'attaquer durant votre tour. Lancez uniquement le dé à 4 faces, le résultat indique
 * les Blessures que vous infligez. Hachoir maudit, Si votre attaque inflige des Blessures, la
 * victime subit 1 Blessure en plus. Hâche tueuse, Si votre attaque inflige des Blessures, la
 * victime subit 1 Blessure en plus.
 */
public class CarteTenebreFactory {
  public static Stack<Carte> generateStackLumiere() {
    Stack<Carte> cartes = new Stack<>();
    cartes.push(CarteTenebreFactory.generateRituelDiabolique());
    cartes.push(CarteTenebreFactory.generatePeauBanane());
    cartes.push(CarteTenebreFactory.generatePoupeeDemoniaque());
    cartes.push(CarteTenebreFactory.generateChauveSourisVampire());
    cartes.push(CarteTenebreFactory.generateChauveSourisVampire());
    cartes.push(CarteTenebreFactory.generateSuccubeTentatrice());
    cartes.push(CarteTenebreFactory.generateSuccubeTentatrice());
    cartes.push(CarteTenebreFactory.generateMitrailleuseFuneste());
    cartes.push(CarteTenebreFactory.generateTronconneuseMal());
    cartes.push(CarteTenebreFactory.generateRevolverTenebre());
    cartes.push(CarteTenebreFactory.generateHachoirMaudit());
    cartes.push(CarteTenebreFactory.generateHacheTueuse());
    cartes.push(CarteTenebreFactory.generateSabreHanteMasamune());
    cartes.push(CarteTenebreFactory.generateAraigneeSanguinaire());
    cartes.push(CarteTenebreFactory.generateChauveSourisVampire());
    cartes.push(CarteTenebreFactory.generateDynamite());
    return cartes;
  }

  private static Equipement generateTronconneuseMal() { //done, cf joueur.getmodificateurdegats
    return new Equipement(
        "Tronçonneuse du mal",
        "Si votre attaque inflige des Blessures," + "la victime subit 1 Blessure en plus.",
        TypeCarte.TENEBRE);
  }

  private static Equipement generateRevolverTenebre() { //done (merci Emilien) cf jeu.stepAttac
    return new Equipement(
        "Revolver des ténèbres",
        "Vous pouvez attaquer un joueur présent sur l'un des 4 lieux hors de votre secteur, "
            + "mais vous ne pouvez plus attaquer "
            + "un joueur situé dans le même secteur que vous.",
        TypeCarte.TENEBRE);
  }

  private static Equipement generateMitrailleuseFuneste() { //done cf jeu.stepAttacResponse
    return new Equipement(
        "Mitrailleuse funeste",
        "Votre attaque affecte tous les personnages qui sont à votre portée. "
            + "Effectuez un seul jet de Blessures pour tous les joueurs concernés.",
        TypeCarte.TENEBRE);
  }

  private static Equipement generateSabreHanteMasamune() { //donc cf jeu.stepattacresponse &&
    //                                                      client\commandManager.interpreterAttac
    return new Equipement(
        "Sabre hanté Masamune",
        "Vous êtes obligé d'attaquer durant votre tour. "
            + "Lancez uniquement le dé à 4 faces, le "
            + "résultat indique les Blessures que "
            + "vous infligez.",
        TypeCarte.TENEBRE);
  }

  private static Equipement generateHachoirMaudit() { //done, cf joueur.getmodificateurdegats
    return new Equipement(
        "Hachoir maudit",
        "Si votre attaque inflige des Blessures, " + "la victime subit 1 Blessure en plus.",
        TypeCarte.TENEBRE);
  }

  private static Equipement generateHacheTueuse() { //done, cf joueur.getmodificateurdegats
    return new Equipement(
        "Hâche tueuse",
        "Si votre attaque inflige des Blessures, " + "la victime subit 1 Blessure en plus.",
        TypeCarte.TENEBRE);
  }

  private static Immediat generateSuccubeTentatrice() {
    return new Immediat(
        "Succube tentatrice",
        "Volez une carte équipement au joueur de votre choix.",
        TypeCarte.TENEBRE);
  }

  private static Immediat generateChauveSourisVampire() {
    return new Immediat(
        "Chauve-souris vampiré",
        "Infligez deux blessure au joueur de votre choix puis soignez une de vos blessures.",
        TypeCarte.TENEBRE);
  }

  private static Immediat generatePoupeeDemoniaque() {
    return new Immediat(
        "Poupée démoniaque",
        "Désignez un joueur puis lancez le dé à 6 faces.\n"
            + "1 à 4: infligez-lui 3 blessures."
            + "5 ou 6 : subissez 3 blessures.",
        TypeCarte.TENEBRE);
  }

  private static Immediat generatePeauBanane() {
    return new Immediat(
        "Peau de banane",
        "Donnez une de vos carte équipement à un autre joueur. Si vous n'en possédez aucune, "
            + "vous encaissez 1 blessure.",
        TypeCarte.TENEBRE);
  }

  private static Immediat generateAraigneeSanguinaire() {
    return new Immediat(
        "Araignée sanguinaire",
        "Infligez deux blessure au joueur de votre choix, "
            + "puis vous subissez vous-même 2 blessures.",
        TypeCarte.TENEBRE);
  }

  private static Immediat generateDynamite() {
    return new Immediat(
        "Dynamite",
        "Lancez les deux dés et infligez 3 blessures  à tous les joueurs (vous compris) se "
            + "trouvant dans le secteur désigné par le total des 2 dés. "
            + "Il ne se passe rien si le total est 7",
        TypeCarte.TENEBRE);
  }

  private static Immediat generateRituelDiabolique() {
    return new Immediat(
        "Rituel diabolique",
        "Si vous êtes Shadow et si vous décidez de révéler (ou avez déjà révélé) votre identité,"
            + " soignez toutes vos blessures.",
        TypeCarte.TENEBRE);
  }
}
