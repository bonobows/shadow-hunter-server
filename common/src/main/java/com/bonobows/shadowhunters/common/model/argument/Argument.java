package com.bonobows.shadowhunters.common.model.argument;

import java.util.Arrays;

public interface Argument {

  static <E extends Enum<E>> boolean contains(Class<E> enumClass, String value) {
    return Arrays.stream(enumClass.getEnumConstants()).anyMatch((t) -> t.name().equals(value));
  }

  enum Log implements Argument {
    LOG_LEVEL,
    LOG_MESSAGE
  }

  enum Attac implements Argument {
    ATTAQUE_ATTAQUE,
    ATTAQUE_ATTAQUANT,
    JOUEURS_A_PORTEE,
    POSSEDE_SABRE,
    POSSEDE_MITRAILLEUSE
  }

  enum Terrain implements Argument {
    TERRAIN_TERRAIN
  }

  enum Tour implements Argument {
    TOUR_JOUEUR,
    TOUR_START
  }

  enum Joueur implements Argument {
    JOUEUR_JOUEUR
  }

  enum Personnage implements Argument {
    PERSONNAGE_PERSONNAGE
  }

  enum Move implements Argument {
    MOVE_JOUEUR,
    MOVE_LIEU
  }

  enum ArgDe implements Argument {
    DE_QUATRE,
    DE_SIX,
    DE_TOTAL,
    DE_QUATRE_2,
    DE_SIX_2,
    DE_TOTAL_2,
    DE_CHOIX_RES_BOUSSOLE
  }

  enum LieuChoix implements Argument {
    LIEU_CHOIX
  }

  enum EffetLieu implements Argument {
    EFFET_LIEU_TYPE,
    EFFET_LIEU_CARTE,
    EFFET_LIEU_CHOIX
  }

  enum Vision implements Argument {
    VISION_JOUEUR,
    VISION_CARTE,
    VISION_CHOIX,
    VISION_EQUIPEMENTS,
    VISION_CHOISI,
    VISION_EQUIPEMENT_CHOISI,
  }

  enum Sanctuaire implements Argument {
    SANCTUAIRE_JOUEURS_A_VOLER,
    SANCTUAIRE_JOUEUR_CIBLE,
    SANCTUAIRE_EQTS_A_VOLER,
    SANCTUAIRE_EQT_CHOISI
  }

  enum SeReveler implements Argument {
    SE_REVELER_REPONSE
  }

  enum UtiliserCapacite implements Argument {
    UTILISER_CAPACITE_REPONSE
  }

  enum CapaciteFranklin implements Argument {
    CAPACITE_FRANKLIN_CHOIX
  }

  enum CapaciteGeorges implements  Argument {
    CAPACITE_GEORGES_CHOIX
  }

  enum CapaciteLoupGarou implements  Argument {
    CAPACITE_LOUP_GAROU_ATTAQUANT,
    CAPACITE_LOUP_GAROU,
    CAPACITE_LOUP_GAROU_CHOIX
  }

  enum CapaciteEmi implements Argument {
    CAPACITE_EMI
  }

  enum CarteImmediate implements Argument {
    CARTE_IMMEDIATE_QUESTION,
    CARTE_IMMEDIATE_CHOIX_JOUEUR,
    CARTE_IMMEDIATE_CHOIX_EQUIPEMENT
  }

  enum SendJoueurs implements Argument {
    SEND_JOEURS_JOUEURS
  }

  enum UpdateJoueur implements  Argument {
    UPDATE_JOUEUR_JOUEUR
  }

  enum Chat implements Argument {
    CHAT_MESSAGE,
    CHAT_JOUEUR
  }

  enum SePresenter implements Argument {
    SE_PRESENTER_PSEUDO
  }

  enum ShowCardLt implements  Argument {
    SHOW_CARD_LT_CARTE
  }

  enum MortJoueur implements  Argument {
    MORT_JOUEUR_ID
  }

}
