package com.bonobows.shadowhunters.common.model.commande;

import com.bonobows.shadowhunters.common.model.argument.Argument;
import com.bonobows.shadowhunters.common.model.argument.ArgumentDeserializer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Objet générique d'un bloc d'infos à JSONiser et envoyer au client.
 *
 * @author epsilonk
 */
public class Commande {
  private CodeCommande code;

  @JsonDeserialize(keyUsing = ArgumentDeserializer.class)
  private Map<Argument, Object> args;

  public Commande(CodeCommande code, Map<Argument, Object> args) {
    this.code = code;
    this.args = args;
  }

  public Commande(CodeCommande code) {
    this.code = code;
    this.args = new HashMap<>();
  }

  public Commande() {}

  public static Commande jsonToCommand(String json) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(json, Commande.class);
  }

  public CodeCommande getCode() {
    return code;
  }

  public void setCode(CodeCommande code) {
    this.code = code;
  }

  public Map<Argument, Object> getArgs() {
    return args;
  }

  public void setArgs(Map<Argument, Object> args) {
    this.args = args;
  }

  public Object getArg(Argument argument) {
    return getArgs().get(argument);
  }

  public void addArg(Argument argument, Object value) {
    this.args.put(argument, value);
  }

  /**
   * Transforme une commande en un String.
   *
   * @return this Commande objet en une grosse String JSON.
   */
  public String commandeToJson() {
    try {
      ObjectMapper mapper = new ObjectMapper();
      //      System.out.println(this.args.toString());
      return mapper.writeValueAsString(this);
    } catch (JsonProcessingException ex) {
      ex.printStackTrace();
      return null;
    }
  }
}
