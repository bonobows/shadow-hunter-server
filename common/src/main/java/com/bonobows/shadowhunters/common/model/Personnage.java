package com.bonobows.shadowhunters.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Milihhard on 04/10/2017.
 */
public class Personnage {

  private int pv;
  private String nom;
  private String objectif;
  private String capacite;
  private Faction faction;
  private String image;
  // attributs d'association cf diagramme de classe
  // Jeu
  //    private Joueur joueur;

  public Personnage(int pv, String nom, String capacite, String objectif, Faction faction) {
    this.pv = pv;
    this.nom = nom;
    this.capacite = capacite;
    this.objectif = objectif;
    this.faction = faction;
  }

  public Personnage() {
  }

  @Override
  public String toString() {
    return nom
        + "("
        + faction
        + ", "
        + capacite
        + ", "
        + objectif
        + "), nombre de blessures : "
        + pv;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public int getPv() {
    return pv;
  }

  public void setPv(int pv) {
    this.pv = pv;
  }

  public String getCapacite() {
    return capacite;
  }

  public void setCapacite(String description) {
    this.capacite = description;
  }

  public String getObjectif() {
    return objectif;
  }

  public void setObjectif(String o) {
    this.objectif = o;
  }

  public Faction getFaction() {
    return faction;
  }

  public void setFaction(Faction faction) {
    this.faction = faction;
  }

  @JsonIgnore
  public boolean isHunter() {
    return (this.faction == Faction.HUNTER);
  }

  @JsonIgnore
  public boolean isShadow() {
    return (this.faction == Faction.SHADOW);
  }

  @JsonIgnore
  public boolean isNeutre() {
    return (this.faction == Faction.NEUTRE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Personnage that = (Personnage) o;

    if (getPv() != that.getPv()) {
      return false;
    }
    if (!getNom().equals(that.getNom())) {
      return false;
    }
    if (!getCapacite().equals(that.getCapacite())) {
      return false;
    }
    if (!getObjectif().equals(that.getObjectif())) {
      return false;
    }
    return getFaction() == that.getFaction();
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
