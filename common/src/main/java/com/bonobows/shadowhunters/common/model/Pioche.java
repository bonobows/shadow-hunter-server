package com.bonobows.shadowhunters.common.model;

import com.bonobows.shadowhunters.common.factory.CarteLumiereFactory;
import com.bonobows.shadowhunters.common.factory.CarteTenebreFactory;
import com.bonobows.shadowhunters.common.factory.CarteVisionFactory;

import java.util.Collections;
import java.util.Stack;

public class Pioche {
  protected Stack<Carte> carteDejaTirees;
  protected Stack<Carte> carteDefausses;
  protected TypeCarte typeCartes;
  // att d'association cf diag classes
  // 1 partie
  // 2 lieux
  protected Stack<Carte> carteRestantes;

  public Pioche(TypeCarte typeCartes) {
    this.typeCartes = typeCartes;
    this.reCreer();
    this.melanger();
  }

  public void melanger() {
    Collections.shuffle(this.carteRestantes);
  }

  public Carte piocher() {
    if (carteRestantes.empty()) {
      while (!carteDefausses.empty()) {
        carteRestantes.push(carteDefausses.pop());
      }
      melanger();
    }
    Carte carte = carteRestantes.pop();
    carteDejaTirees.push(carte);
    return carte;
  }

  public void defausser(Carte carte) {
    carteDefausses.push(carte);
  }

  public void reCreer() {
    this.carteDefausses = new Stack<>();
    this.carteDejaTirees = new Stack<>();
    switch (this.typeCartes) {
      case VISION:
        this.carteRestantes = CarteVisionFactory.generatePiocheVision();
        break;
      case TENEBRE:
        this.carteRestantes = CarteTenebreFactory.generateStackLumiere();
        break;
      case LUMIERE:
        this.carteRestantes = CarteLumiereFactory.generateStackLumiere();
        break;
      default:
        this.carteRestantes = new Stack<>();
    }
  }

  @Override
  public String toString() {
    return "Pioche{\n"
        + "typeCartes="
        + typeCartes
        + ",\ncarteDejaTirees="
        + carteDejaTirees
        + ",\ncarteDefausses="
        + carteDefausses
        + ",\ncarteRestantes="
        + carteRestantes
        + "\n}";
  }
}
