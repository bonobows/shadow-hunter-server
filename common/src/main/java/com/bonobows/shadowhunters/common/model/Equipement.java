package com.bonobows.shadowhunters.common.model;

public class Equipement extends CarteLumiereTenebre {

  // attributs d'association cf diag classe
  private Joueur possesseur;

  public Equipement() {}

  @Override
  public String getTypeOfCard() {
    return "Equipement";
  }

  public Equipement(String nom, String effet, TypeCarte t) {
    super(nom, effet, t);
  }

  public boolean nappartientAPersonne() {
    return (possesseur == null);
  }

  @Override
  public String toString() {
    return "Equipement{"
        + ", nom='"
        + nom
        + '\''
        + ", effet='"
        + effet
        + '\''
        + ", type="
        + type
        + '}';
  }
}
