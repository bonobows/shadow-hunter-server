package com.bonobows.shadowhunters.common.model;

/** Created by Milihhard on 04/10/2017. */
public class Utilisateur {
  private String pseudo;
  // attributs d'association cf diagramme de classes
  // 0 ou 1 partie
  // 1 jeu


  public Utilisateur() {  }

  public Utilisateur(String pseudo) {
    this.pseudo = pseudo;
  }

  public String getPseudo() {
    return this.pseudo;
  }

  public void setPseudo(String pseudo) {
    this.pseudo = pseudo;
  }

  @Override
  public String toString() {
    return "Utilisateur{"
        + "pseudo="
        + pseudo
        + "}";
  }
}
