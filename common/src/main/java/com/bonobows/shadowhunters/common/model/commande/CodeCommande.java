package com.bonobows.shadowhunters.common.model.commande;

/**
 * Enumeration de toutes les commandes envoyés et reçu aux clients et au serveur.
 * @author epsilonk
 */
public enum CodeCommande {
  ATTAC,
  LOG,
  TERRAIN,
  TURN,
  JOUEUR,
  PERSONNAGE,
  MOVE,
  DE,
  RES_BOUSSOLE,
  RES_BOUSSOLE_CHOISI,
  DECHOIX,
  LIEU_CHOIX,
  EFFET_LIEU,
  CIBLE_VOL_CHOIX,
  CIBLE_VOL,
  GO_CHOISIR_EQT_A_VOLER,
  EQT_CHOISI,
  VISION,
  SE_REVELER,
  LANCER_DES,
  UTILISER_CAPACITE,
  CAPACITE_FRANKLIN,
  CAPACITE_GEORGES,
  PERSONNAGE_VISION,
  FIN_PARTIE,
  CAPACITE_LOUP_GAROU,
  CAPACITE_EMI,
  CARTE_IMMEDIATE,
  SEND_JOUEURS,
  UPDATE_JOUEUR,
  CHAT,
  SE_PRESENTER,
  DEMARRER_PARTIE,
  SHOW_CARD_LT,
  MORT_JOUEUR
}
