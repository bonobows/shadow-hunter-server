package com.bonobows.shadowhunters.common.model;

public class CarteVision extends Carte {
  private ConditionVision typeCondition;
  private EffetVision typeEffet;

  public CarteVision(
      String nom, String effet, ConditionVision typeCondition, EffetVision typeEffet) {
    super(nom, effet, TypeCarte.VISION);
    this.typeCondition = typeCondition;
    this.typeEffet = typeEffet;
    setEffet();
  }

  public CarteVision() {}

  private void setEffet() {
    this.effet = this.typeEffet.toString();
  }

  public ConditionVision getTypeCondition() {
    return typeCondition;
  }

  public void setTypeCondition(ConditionVision typeCondition) {
    this.typeCondition = typeCondition;
  }

  public EffetVision getTypeEffet() {
    return typeEffet;
  }

  public void setTypeEffet(EffetVision typeEffet) {
    this.typeEffet = typeEffet;
  }

  @Override
  public String toString() {
    return "CarteVision{" + super.toString() + ", typeCondition=" + typeCondition.toString() + '}';
  }
}
