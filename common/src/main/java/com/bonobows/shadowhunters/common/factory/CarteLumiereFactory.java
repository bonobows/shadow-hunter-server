package com.bonobows.shadowhunters.common.factory;

import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Immediat;
import com.bonobows.shadowhunters.common.model.TypeCarte;

import java.util.Stack;


public class CarteLumiereFactory {

  /**
   * Lance de Longinus, Si vous êtes une Hunter et que votre identité est révélée, chaque fois
   * qu'une de vos attaques inflige des Blessures, vous infligez 2 Blessures supplémentaires.
   * Crucifix en argent, Si vous attaquez et tuez un autre personnage, vous récupérez toutes ses
   * cartes équipements. Boussole mystique, Quand vous vous déplacez, vous pouvez lancer 2 fois les
   * dés et choisir quel résultat utiliser. Toge sainte, Vos attaques infligent 1 Blessure de moins
   * et les Blessures que vous subissez sont réduites de 1. Broche de chance, Un joueur dans la
   * Forêt Hantée ne peut pas utiliser le pouvoir du Lieu pour vous infliger des Blessures (mais il
   * peut toujours vous guérir). Amulette, Vous ne subissez aucune Blessure causée par les cartes
   * Ténèbres : Araignée sanguinaire, Dynamite ou Chauve-souris vampire.
   */
  public static Stack<Carte> generateStackLumiere() {
    Stack<Carte> cartes = new Stack<>();
    cartes.push(CarteLumiereFactory.generateBrocheChance());
    cartes.push(CarteLumiereFactory.generateTogeSainte());
    cartes.push(CarteLumiereFactory.generateBoussoleMistyque());
    cartes.push(CarteLumiereFactory.generateCrucifixArgent());
    cartes.push(CarteLumiereFactory.generateLanceLonginus());
    cartes.push(CarteLumiereFactory.generateAmulette());

    cartes.push(CarteLumiereFactory.generateMiroirDivin());
    cartes.push(CarteLumiereFactory.generateEauBenite());
    cartes.push(CarteLumiereFactory.generateEauBenite());
    cartes.push(CarteLumiereFactory.generateAngeGardien());
    cartes.push(CarteLumiereFactory.generateEclairPurificateur());
    cartes.push(CarteLumiereFactory.generatePremiersSecours());
    cartes.push(CarteLumiereFactory.generateBenediction());
    cartes.push(CarteLumiereFactory.generateSavoirEncestral());
    cartes.push(CarteLumiereFactory.generateAvementSupreme());
    cartes.push(CarteLumiereFactory.generateBarreDeChocolat());
    return cartes;
  }

  private static Equipement generateLanceLonginus() { //mechanic done cf joueur.getmodificateurdgt
    return new Equipement(
        "Lance de Longinus",
        "Si vous êtes une Hunter et que  votre "
            + "identité est révélée, chaque fois qu'une de vos attaques inflige des Blessures, "
            + "vous infligez 2 Blessures supplémentaires.",
        TypeCarte.LUMIERE);
  }

  private static Equipement generateCrucifixArgent() { //mecanic done cf jeu.stepmort
    return new Equipement(
        "Crucifix en argent",
        "Si vous attaquez et tuez un autre "
            + "personnage, vous récupérez toutes ses cartes équipements.",
        TypeCarte.LUMIERE);
  }

  private static Equipement generateBoussoleMistyque() { //done cf jeu.stepLieuAvecBoussole
    return new Equipement(
        "Boussole mystique",
        "Quand vous vous déplacez, vous pouvez "
            + "lancer 2 fois les dés et choisir quel résultat utiliser.",
        TypeCarte.LUMIERE);
  }

  private static Equipement generateTogeSainte() { //done cf joueur.getmodifdef
    return new Equipement(
        "Toge sainte",
        "Vos attaques infligent 1 Blessure de moins et "
            + "les Blessures que vous subissez sont réduites de 1.",
        TypeCarte.LUMIERE);
  }

  private static Equipement generateBrocheChance() { //done, cf jeu.stepEffetLieuForet
    return new Equipement(
        "Broche de chance",
        "Un joueur dans la Forêt Hantée ne peut pas "
            + "utiliser le pouvoir du Lieu pour vous infliger des Blessures (mais il peut toujours "
            + "vous guérir).",
        TypeCarte.LUMIERE);
  }

  private static Equipement generateAmulette() { //cf jeuController.stepCarteImmediateChoixJoueur
    //cf CarteController.manageImmediatTenebre
    return new Equipement(
        "Amulette",
        "Vous ne subissez aucune Blessure causée par les "
            + "cartes Ténèbres : Araignée sanguinaire, Dynamite ou Chauve-souris vampire.",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateMiroirDivin() {
    return new Immediat(
        "Miroir Divin",
        "Si vous etes un Shadow autre que Metamorphe,"
            + "vous devez reveler votre identité.",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateEauBenite() {
    return new Immediat(
        "Eau Benite",
        "Vous etes soigne de 2 blessures.",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateAngeGardien() {
    return new Immediat(
        "Ange Gardien",
        "Les attaques ne vous infligent aucune"
            + " Blessure jusqu'a votre prochain tour.",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateEclairPurificateur() {
    return new Immediat(
        "Eclair Purificateur",
        "Chaque personnage, a l'exeption"
            + "de vous-meme, subit 2 blessures",
        TypeCarte.LUMIERE);
  }

  private static Immediat generatePremiersSecours() {
    return new Immediat(
        "Premiers Secours",
        "Placer le marqueur de Blessures"
            + "du joueur de votre choix"
            + "(y conpris vous) sur le 7",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateBenediction() {
    return new Immediat(
        "Benediction",
        "Choisissez un joueur autre que vous"
            + "et lancez le de a 6 faces. Ce joueur guérit"
            + "d'autant de Blessures que le resultat du de.",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateSavoirEncestral() {
    return new Immediat(
        "Savoir Encestral",
        "Lorque votre tour est termine, jouez "
            + "immediatemment un nouvau tour",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateAvementSupreme() {
    return new Immediat(
        "Avement Supreme",
        "Si vous etes un Hunter, vous pouvez reveler"
            + "votre identité. Si vous le faites, ou si vous etes deja revele. Vous soignez toutes"
            + "vos Blessures",
        TypeCarte.LUMIERE);
  }

  private static Immediat generateBarreDeChocolat() {
    return new Immediat(
        "Barre De Chocolat",
        "Si vous étes Allie, Agnes, Emi, Ellen, Momie"
            + "ou Metamorphe et que vous reveler (ou avez deja revele) votre idantité,"
            + "vous soignez toutes vos Blessures.",
        TypeCarte.LUMIERE);
  }
}
