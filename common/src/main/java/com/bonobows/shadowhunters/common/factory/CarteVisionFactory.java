package com.bonobows.shadowhunters.common.factory;

import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.CarteVision;
import com.bonobows.shadowhunters.common.model.ConditionVision;
import com.bonobows.shadowhunters.common.model.EffetVision;

import java.util.Stack;

public class CarteVisionFactory {

  /**
   * Génère la pioche visions.
   *
   * <p>- 2 "Vision furtive"
   *
   * <p>- 2 "Vision mortifère" - 2 "Vision cupide"
   *
   * <p>- 2 "Vision envrante"
   *
   * <p>- 1 "Vision clairvoyante"
   *
   * <p>- 1 "Vision foudroyante"
   *
   * <p>- 1 "Vision suprême"
   *
   * <p>- 1 "Vision lugubre"
   *
   * <p>- 1 "Vision réconfortante"
   *
   * <p>- 1 "Vision destructrice"
   *
   * <p>- 1 "Vision divine"
   *
   * <p>- 1 "Vision purificatrice"
   */
  public static Stack<Carte> generatePiocheVision() {
    Stack<Carte> cartes = new Stack<>();
    cartes.push(CarteVisionFactory.generateVisionPurificratice());
    cartes.push(CarteVisionFactory.generateVisionDivine());
    cartes.push(CarteVisionFactory.generateVisionReconfortante());
    cartes.push(CarteVisionFactory.generateVisionDestructrice());
    cartes.push(CarteVisionFactory.generateVisionLugubre());
    cartes.push(CarteVisionFactory.generateVisionSupreme());
    cartes.push(CarteVisionFactory.generateVisionFoudroyante());
    cartes.push(CarteVisionFactory.generateVisionClairvoyante());
    cartes.push(CarteVisionFactory.generateVisionEnvrante());
    cartes.push(CarteVisionFactory.generateVisionEnvrante());
    cartes.push(CarteVisionFactory.generateVisionCupide());
    cartes.push(CarteVisionFactory.generateVisionCupide());
    cartes.push(CarteVisionFactory.generateVisionFurtive());
    cartes.push(CarteVisionFactory.generateVisionFurtive());
    cartes.push(CarteVisionFactory.generateVisionMortifere());
    cartes.push(CarteVisionFactory.generateVisionMortifere());
    return cartes;
  }

  private static CarteVision generateVisionFurtive() {
    return new CarteVision(
        "Vision furtive",
        "Je pense que tu es Hunter ou Shadow. "
            + "Si c'est le cas tu dois : "
            + "soit me donner une carte équipement soit subir 1 Blessures.",
        ConditionVision.SHADOW_HUNTER,
        EffetVision.BOE);
  }

  private static CarteVision generateVisionMortifere() {
    return new CarteVision(
        "Vision mortifère",
        "Je pense que tu es Hunter. Si c'est le cas subis 1 Blessure !",
        ConditionVision.HUNTER,
        EffetVision.BLESSURE);
  }

  private static CarteVision generateVisionCupide() {
    return new CarteVision(
        "Vision cupide",
        "Je pense que tu es Neutre ou Shadow. Si c'est le cas tu dois : "
            + "soit me donner une carte équipement soit subir 1 Blessure.",
        ConditionVision.SHADOW_NEUTRE,
        EffetVision.BOE);
  }

  private static CarteVision generateVisionEnvrante() {
    return new CarteVision(
        "Vision envrante",
        "Je pense que tu es Neutre ou Hunter. Si c'est le cas tu dois : "
            + "soit me donner une carte équipement soit subir 1 Blessure.",
        ConditionVision.HUNTER_NEUTRE,
        EffetVision.BOE);
  }

  private static CarteVision generateVisionClairvoyante() {
    return new CarteVision(
        "Vision clairvoyante",
        "Je pense que tu es un personnage de 11 Pint de Vie ou moins : (A/B/C/E/M). "
            + "Si c'est le cas subis 1 Blessure !",
        ConditionVision.PV_MOINS_ONZE,
        EffetVision.BLESSURE);
  }

  private static CarteVision generateVisionFoudroyante() {
    return new CarteVision(
        "Vision foudroyante",
        "Je pense que tu es Shadow. Si c'est le cas subis 1 Blessure !",
        ConditionVision.SHADOW,
        EffetVision.BLESSURE);
  }

  private static CarteVision generateVisionSupreme() {
    return new CarteVision(
        "Vision suprême",
        "Montre-moi secrètement ta carte Personnage !",
        ConditionVision.NOTEST,
        EffetVision.SUPREME);
  }

  private static CarteVision generateVisionLugubre() {
    return new CarteVision(
        "Vision lugubre",
        "Je pense que tu es Shadow. Si c'est le cas soigne 1 Blessure. "
            + "(Toutefois si tu n'avais aucune Blessure subis 1 Blessure !)",
        ConditionVision.SHADOW,
        EffetVision.BOE);
  }

  private static CarteVision generateVisionReconfortante() {
    return new CarteVision(
        "Vision réconfortante",
        "Je pense que tu es Neutre. Si c'est le cas soigne 1 Blessure. "
            + "(Toutefois si tu n'avais aucune Blessure subis 1 Blessure !)",
        ConditionVision.NEUTRE,
        EffetVision.CONDITIONNEL);
  }

  private static CarteVision generateVisionDestructrice() {
    return new CarteVision(
        "Vision destructrice",
        "Je pense que tu es un personnage de 12 PV ou plus (D/F/G/L/V). "
            + "Si c'est le cas subis 2 Blessures !",
        ConditionVision.PV_PLUS_DOUZE,
        EffetVision.DEUX_BLESSURES);
  }

  private static CarteVision generateVisionDivine() {
    return new CarteVision(
        "Vision divine",
        "Je pense que tu es Hunter. Si c'est le cas soigne 1 Blessure. "
            + "(Toutefois si tu n'avais aucune Blessure subis 1 Blessure !)",
        ConditionVision.HUNTER,
        EffetVision.CONDITIONNEL);
  }

  private static CarteVision generateVisionPurificratice() {
    return new CarteVision(
        "Vision purificatrice",
        "Je pense que tu es Shadow. " + "Si c'est le cas subis 2 Blessures !",
        ConditionVision.SHADOW,
        EffetVision.DEUX_BLESSURES);
  }
}
