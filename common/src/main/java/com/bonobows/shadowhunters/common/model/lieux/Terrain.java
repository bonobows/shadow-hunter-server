package com.bonobows.shadowhunters.common.model.lieux;

import com.bonobows.shadowhunters.common.factory.LieuFactory;
import com.bonobows.shadowhunters.common.model.Joueur;

import java.util.ArrayList;
import java.util.List;

public class Terrain {

  protected List<Lieu> lieux;

  public Terrain() {
    this.lieux = LieuFactory.genererLieux();
  }

  public Lieu getLieuByLancerDe(int lancerDe) {
    for (Lieu lieu : lieux) {
      if (lieu.bonneValeurDuDe(lancerDe)) {
        return lieu;
      }
    }
    return null;
  }

  public Lieu getLieuJoueur(Joueur j) {
    for (Lieu lieu : lieux) {
      if (lieu.hasJoueur(j)) {
        return lieu;
      }
    }
    return null;
  }

  public Lieu getLieuTypeLieu(NomLieu tp) {
    for (Lieu lieu : lieux) {
      if (tp.equals(lieu.getNom())) {
        return lieu;
      }
    }
    return null;
  }

  public void moveJoueur(Joueur j, int lancerDe) {
    Lieu l = getLieuJoueur(j);
    if (l != null) {
      lieux.get(lieux.indexOf(l)).removeJoueur(j);
    }
    lieux.get(lieux.indexOf(getLieuByLancerDe(lancerDe))).addJoueur(j);
  }

  public void moveJoueur(Joueur j, Lieu l) {
    Lieu lieuJoueur = getLieuJoueur(j);
    if (lieuJoueur != null) {
      lieux.get(lieux.indexOf(lieuJoueur)).removeJoueur(j);
    }
    for (Lieu lieu : lieux) {
      if (lieu.getNom().equals(l.getNom())) {
        lieu.addJoueur(j);
      }
    }
  }

  public void removeDeadJoueur(Joueur j) {
    Lieu lieuJoueur = getLieuJoueur(j);
    if (lieuJoueur != null) {
      lieux.get(lieux.indexOf(lieuJoueur)).removeJoueur(j);
    }
  }

  public Lieu getLieuVoisin(Lieu l) {
    int posLieux = this.lieux.indexOf(l);
    // si le lieu est en position pair (0) alors le lieux voisin et celui d'après (1)
    // si le lieu est en position impair (0) alors le lieux voisin et celui d'avant (0)
    return posLieux % 2 == 0 ? this.lieux.get(posLieux + 1) : this.lieux.get(posLieux - 1);
  }

  public List<Joueur> getJoueursPeutAttaque(Joueur j, boolean hasRevolverDesTenebres) {
    List<Joueur> personnages = new ArrayList<>();
    if (!hasRevolverDesTenebres) {
      personnages.addAll(getLieuJoueur(j).getJoueurs());
      personnages.addAll(getLieuVoisin(getLieuJoueur(j)).getJoueurs());
      personnages.remove(j);
    } else {
      int indexLieux = lieux.indexOf(getLieuJoueur(j));
      int indexLieuxVoisin = indexLieux % 2 == 0 ? indexLieux + 1 : indexLieux - 1;
      for (int i = 0; i < lieux.size(); i++) {
        if (i != indexLieux && i != indexLieuxVoisin) {
          personnages.addAll(lieux.get(i).getJoueurs());
        }
      }
    }
    return personnages;
  }

  @Override
  public String toString() {
    String retour = "";
    for (Lieu l : lieux) {
      retour += l.toString() + "\n";
    }
    return retour;
  }

  public List<Lieu> getLieux() {
    return lieux;
  }

  public void setLieux(List<Lieu> lieux) {
    this.lieux = lieux;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Terrain terrain = (Terrain) o;

    return getLieux().equals(terrain.getLieux());
  }

  @Override
  public int hashCode() {
    return getLieux().hashCode();
  }
}
