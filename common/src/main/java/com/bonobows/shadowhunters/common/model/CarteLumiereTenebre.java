package com.bonobows.shadowhunters.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class CarteLumiereTenebre extends Carte {

  public CarteLumiereTenebre(String nom, String effet, TypeCarte t) {
    super(nom, effet, t);
  }

  public CarteLumiereTenebre() {}

  @JsonIgnore
  public Boolean isLumiere() {
    return (this.getType().equals(TypeCarte.LUMIERE));
  }

  @JsonIgnore
  public Boolean isTenebre() {
    return (this.getType().equals(TypeCarte.TENEBRE));
  }

  @JsonIgnore
  public abstract String getTypeOfCard();
}
