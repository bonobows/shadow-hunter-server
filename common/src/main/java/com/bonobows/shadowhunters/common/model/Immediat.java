package com.bonobows.shadowhunters.common.model;

public class Immediat extends CarteLumiereTenebre {
  public Immediat(String nom, String effet, TypeCarte t) {
    super(nom, effet, t);
  }

  @Override
  public String getTypeOfCard() {
    return "Immediat";
  }
}
