package com.bonobows.shadowhunters.common.factory;

import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LieuFactory {

  private LieuFactory() {}

  public static List<Lieu> genererLieux() {
    List<Lieu> lieux = new ArrayList<>();
    lieux.add(generateCimetiere());
    lieux.add(generateSanctuaire());
    lieux.add(generateForet());
    lieux.add(generateMonastere());
    lieux.add(generatePorteOutremonde());
    lieux.add(generateAntreHermite());

    Collections.shuffle(lieux);
    return lieux;
  }

  public static Lieu generateCimetiere() {
    ArrayList<Integer> valeurDe = new ArrayList<>();
    valeurDe.add(8);
    return new Lieu(NomLieu.CIMETIERE, "Vous pouvez piocher une carte Ténèbres.", valeurDe);
  }

  public static Lieu generateSanctuaire() {
    ArrayList<Integer> valeurDe = new ArrayList<>();
    valeurDe.add(10);
    return new Lieu(
        NomLieu.SANCTUAIRE, "Vous pouvez voler une carte équipement à un autre joueur.", valeurDe);
  }

  public static Lieu generateForet() {
    ArrayList<Integer> valeurDe = new ArrayList<>();
    valeurDe.add(9);
    return new Lieu(
        NomLieu.FORET,
        "Le joueur de votre choix peut subir 2 blessures OU soigner 1 blessure.",
        valeurDe);
  }

  public static Lieu generateMonastere() {
    ArrayList<Integer> valeurDe = new ArrayList<>();
    valeurDe.add(6);
    return new Lieu(NomLieu.MONASTERE, "Vous pouvez piocher une carte Lumière.", valeurDe);
  }

  public static Lieu generatePorteOutremonde() {
    ArrayList<Integer> valeurDe = new ArrayList<>();
    valeurDe.add(4);
    valeurDe.add(5);
    return new Lieu(
        NomLieu.PORTE_OUTREMONDE,
        "Vous pouvez piocher une carte de la pile de votre choix.",
        valeurDe);
  }

  public static Lieu generateAntreHermite() {
    ArrayList<Integer> valeurDe = new ArrayList<>();
    valeurDe.add(2);
    valeurDe.add(3);
    return new Lieu(NomLieu.ANTRE_HERMITE, "Vous pouvez piocher une carte Vision.", valeurDe);
  }
}
