package com.bonobows.shadowhunters.common.model;

import java.awt.Color;

public enum Couleur {
  BLANC,
  BLEU,
  VERT,
  JAUNE,
  ROUGE,
  MARRON,
  VIOLET,
  ORANGE;

  public Color transformToColor() {
    switch (this) {
      case BLANC:
        return Color.white;
      case BLEU:
        return Color.blue;
      case VERT:
        return Color.green;
      case JAUNE:
        return Color.yellow;
      case ROUGE:
        return Color.red;
      case MARRON:
        return Color.decode("0x4F3A28");
      case VIOLET:
        return Color.pink;
      case ORANGE:
        return Color.orange;
      default:
        return Color.pink;
    }
  }
}
