package com.bonobows.shadowhunters.common.model;

import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/** Created by Milihhard on 04/10/2017. */
public class Joueur {

  private Couleur couleur;
  private int nbBlessures;
  private int modificateurDegat;
  private int modificateurDefense;
  private boolean revele;
  private List<Equipement> eqts;
  private Utilisateur utilisateur;
  private Lieu lieuCourrant;
  private Personnage personnage;
  private int idJoueur;
  private boolean haveUseCapaciteSpecial;
  private boolean angeGardien;
  // private Partie partieEnCours;

  public Joueur() {}

  public Joueur(Couleur couleur) {
    this.couleur = couleur;
    this.nbBlessures = 0;
    this.eqts = new ArrayList<>();
  }

  public int getIdJoueur() {

    return idJoueur;
  }

  public void setIdJoueur(int idJoueur) {
    this.idJoueur = idJoueur;
  }

  public boolean getHaveUseCapaciteSpecial() {
    return haveUseCapaciteSpecial;
  }

  public void setHaveUseCapaciteSpecial(boolean haveUseCapaciteSpecial) {
    this.haveUseCapaciteSpecial = haveUseCapaciteSpecial;
  }

  @JsonIgnore
  public String getIdJoueurStringify() {
    return "J" + idJoueur;
  }

  @Override
  public String toString() {
    return "Joueur{"
        + "id="
        + idJoueur
        + ", couleur="
        + couleur
        + ", nbBlessures="
        + nbBlessures
        + ", modificateurDegat="
        + modificateurDegat
        + ", modificateurDefense="
        + modificateurDefense
        + ", aRevele="
        + revele
        + ", eqts="
        + eqts
        + ", utilisateur="
        + utilisateur
        + ", lieuCourrant="
        + lieuCourrant
        + ", personnage="
        + personnage
        + '}';
  }

  public void setNbBlessuresToZero() {
    this.nbBlessures = 0;
  }

  public void setNbBlessures(int blessures) {
    this.nbBlessures = blessures;
  }

  public Couleur getCouleur() {
    return couleur;
  }

  public int getModificateurDegat(int resDe) {
    int total = 0;

    if (this.possedeEqtdeNom("Lance de Longinus")
        && this.isRevele()
        && this.personnage.isHunter()
        && resDe > 0) {
      total += 2;
    }

    if (this.possedeEqtdeNom("Toge sainte")) {
      total += -1;
    }

    if (this.possedeEqtdeNom("Tronçonneuse du mal") && resDe > 0) {
      total += 1;
    }

    if (this.possedeEqtdeNom("Hachoir maudit") && resDe > 0) {
      total += 1;
    }

    if (this.possedeEqtdeNom("Hâche tueuse") && resDe > 0) {
      total += 1;
    }

    modificateurDegat = total;
    return modificateurDegat;
  }

  public int getModificateurDefense() {
    int total = 0;

    if (this.possedeEqtdeNom("Toge sainte")) {
      total += 1;
    }

    modificateurDefense = total;
    return modificateurDefense;
  }

  public boolean isRevele() {
    return this.revele;
  }

  public void revele() {
    this.revele = true;
  }

  public List<Equipement> getEqts() {
    return eqts;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public Lieu getLieuCourrant() {
    return lieuCourrant;
  }

  public void setLieuCourrant(Lieu lieuCourrant) {
    this.lieuCourrant = lieuCourrant;
  }

  public Personnage getPersonnage() {
    return this.personnage;
  }

  public void setPersonnage(Personnage personnage) {
    this.personnage = personnage;
  }

  public void ajouterBlessures(int degats) {
    if (degats >= 0) { // handle cases modificateur defense greater then degats
      this.nbBlessures += degats;
    }
  }

  public void retirerBlessures(int degats) {
    this.nbBlessures -= degats;
    if (this.nbBlessures < 0) {
      this.nbBlessures = 0;
    }
  }

  public int getNbBlessures() {
    return nbBlessures;
  }

  @JsonIgnore
  public boolean isDead() {
    return this.personnage != null && this.nbBlessures >= this.personnage.getPv();
  }

  public void addEquipement(Equipement e) {
    if (this.eqts == null) {
      this.eqts = new ArrayList<>();
    }
    eqts.add(e);
  }

  @JsonIgnore
  public boolean possedeEqtdeNom(String nomEqt) {
    for (Equipement e : this.eqts) {
      if (e.getNom().equals(nomEqt)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof Joueur && ((Joueur) o).idJoueur == this.idJoueur;
  }

  public boolean getAngeGardien() {
    return this.angeGardien;
  }

  public void setAngeGardien(boolean angeGardien) {
    this.angeGardien = angeGardien;
  }

}
