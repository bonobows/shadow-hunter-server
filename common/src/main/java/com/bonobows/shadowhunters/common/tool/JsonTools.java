package com.bonobows.shadowhunters.common.tool;

import com.bonobows.shadowhunters.common.model.argument.Argument;
import com.bonobows.shadowhunters.common.model.commande.Commande;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonTools {

  public static Object toObject(String json, Class className) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(json, className);
  }

  public static Object toObject(Commande c, Argument argument, Class className) throws IOException {
    return toObject(c.getArg(argument).toString(), className);
  }
}
