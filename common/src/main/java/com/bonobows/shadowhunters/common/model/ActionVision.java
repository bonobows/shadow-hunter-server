package com.bonobows.shadowhunters.common.model;

public enum ActionVision {
  PRENDRE_DEGAT("Prendre des dégats"),
  SE_SOIGNER("Se soigner"),
  DONNER_EQUIPEMENT("Donner un équipement"),
  RIEN("Rien"),
  MONTRER_CARTE("Montrer sa carte");

  private String text;

  ActionVision(String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return text;
  }

  public static ActionVision getActionVisionByLetter(char letter) {
    switch (letter) {
      case 'd':
        return PRENDRE_DEGAT;
      case 's':
        return SE_SOIGNER;
      case 'c':
        return MONTRER_CARTE;
      case 'e':
        return DONNER_EQUIPEMENT;
      case 'r':
      default:
        return RIEN;
    }
  }

  public char getLetter() {
    switch (this) {
      case RIEN:
        return 'r';
      case PRENDRE_DEGAT:
        return 'd';
      case SE_SOIGNER:
        return 's';
      case MONTRER_CARTE:
        return 'c';
      case DONNER_EQUIPEMENT:
        return 'e';
      default:
        return ' ';
    }
  }
}
