package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.gui.image.StretchIcon;
import com.bonobows.shadowhunters.client.gui.image.UrlConverter;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.net.URL;

public class JoueurGui extends JFrame {

  private Joueur joueur;
  private boolean isProrietaire;

  public JoueurGui(Joueur j, boolean isProrietaire) {

    this.joueur = j;
    this.isProrietaire = isProrietaire;
  }

  public void init() {
    initFrame();
    initComponent();
  }

  private void initFrame() {
    this.setTitle("Joueur " + this.joueur.getIdJoueur());
    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    this.setResizable(false);
  }

  private void initComponent() {
    setPreferredSize(new Dimension(450, 262));
    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
    mainPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

    StretchIcon icon;
    URL personnage;
    if ((isProrietaire || joueur.isRevele()) && joueur.getPersonnage() != null) {
      personnage = UrlConverter.personnageCardToUrl(joueur.getPersonnage().getNom());
    } else {
      personnage = UrlConverter.personnageCardToUrl("dos");
    }
    icon = new StretchIcon(personnage);
    JLabel image = new JLabel(icon);
    image.setPreferredSize(new Dimension(180, 262));
    image.setMinimumSize(new Dimension(180, 262));
    image.setMaximumSize(new Dimension(180, 262));
    image.setAlignmentY(0f);
    image.setToolTipText("<html><img src=\"" + personnage + "\">");
    mainPanel.add(image);
    mainPanel.add(Box.createRigidArea(new Dimension(5,0)));

    JPanel infoPanel = new JPanel();
    infoPanel.setAlignmentY(0f);
    infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));

    JScrollPane scrollFrame = new JScrollPane(infoPanel);
    scrollFrame.setBorder(null);
    scrollFrame.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    scrollFrame.setOpaque(false);
    infoPanel.setAutoscrolls(true);

    mainPanel.add(scrollFrame);

    infoPanel.add(createText(this.joueur.getIdJoueurStringify()));
    if (this.joueur.isDead()) {
      infoPanel.add(createText("Mort : Oui"));
    } else {
      infoPanel.add(createText("Mort : Non"));
    }

    infoPanel.add(createText("Blessures : " + this.joueur.getNbBlessures()));
    if (this.joueur.getUtilisateur() != null) {
      infoPanel.add(createText("Pseudo : " + this.joueur.getUtilisateur().getPseudo()));
    }
    if ((this.joueur.isRevele() || isProrietaire) && this.joueur.getPersonnage() != null) {
      if (this.joueur.isRevele()) {
        infoPanel.add(createText("Révélé : Oui"));
      } else {
        infoPanel.add(createText("Révélé : Non"));
      }
      infoPanel.add(createText("PV : " + this.joueur.getPersonnage().getPv()));
      infoPanel.add(createText("Perso : " + this.joueur.getPersonnage().getNom()));
      infoPanel.add(createText("Objectif : " + this.joueur.getPersonnage().getObjectif()));
      infoPanel.add(createText("Faction : " + this.joueur.getPersonnage().getFaction()));
      infoPanel.add(createText("Capacité : " + this.joueur.getPersonnage().getCapacite()));
    } else {
      infoPanel.add(createText("Révélé : Non"));
    }
    infoPanel.add(createText("Modificateur défence : " + this.joueur.getModificateurDefense()));
    infoPanel.add(createText("Modificateur dégats : " + this.joueur.getModificateurDegat(1)));
    infoPanel.add(createText("Couleur : " + this.joueur.getCouleur()));
    if (this.joueur.getHaveUseCapaciteSpecial()) {
      infoPanel.add(createText("Capacité spécial utilisée : Oui"));
    } else {
      infoPanel.add(createText("Capacité spécial utilisée : Non"));
    }
    if (this.joueur.getEqts().size() > 0) {
      infoPanel.add(createText("Equipements "
          + "(placer la souris sur le nom pour voir la carte) :"));
      for (Equipement eq : this.joueur.getEqts()) {
        icon = new StretchIcon(UrlConverter.carteToUrl(eq.getNom(), eq.getType()));
        image = new JLabel(icon);
        image.setPreferredSize(new Dimension(180, 400));
        JTextArea label = createText("• " + eq.getNom());
        label.setToolTipText("<html><img src=\""
            + UrlConverter.carteToUrl(eq.getNom(), eq.getType())
            + "\">");
        infoPanel.add(label);
      }
    } else {
      infoPanel.add(createText("Equipements : Aucun"));
    }
    scrollFrame.getViewport().setViewPosition(new Point(0, 0));
    this.setContentPane(mainPanel);
  }

  public void draw() {
    this.pack();
    this.setVisible(true);
  }

  private JTextArea createText(String text) {
    JTextArea textArea = new JTextArea();
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setText(text);
    textArea.setEnabled(false);
    textArea.setBackground(null);
    textArea.setDisabledTextColor(Color.black);
    return textArea;
  }
}
