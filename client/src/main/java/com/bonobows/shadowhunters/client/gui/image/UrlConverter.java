package com.bonobows.shadowhunters.client.gui.image;

import com.bonobows.shadowhunters.common.model.Couleur;
import com.bonobows.shadowhunters.common.model.TypeCarte;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;

import java.net.URL;

public class UrlConverter {

  public static URL pionToUrl(Couleur couleur) {
    String url;

    switch (couleur) {
      case MARRON:
        url = "black";
        break;
      case BLEU:
        url = "blue";
        break;
      case JAUNE:
        url = "yellow";
        break;
      case ORANGE:
        url = "orange";
        break;
      case VERT:
        url = "green";
        break;
      case BLANC:
        url = "white";
        break;
      case ROUGE:
        url = "red";
        break;
      case VIOLET:
        url = "purple";
        break;
      default:
        url = "black";
    }
    return Thread.currentThread().getContextClassLoader().getResource("pions/" + url + ".png");
  }

  public static URL lieuToUrl(NomLieu nomLieu) {
    String url;
    switch (nomLieu) {
      case CIMETIERE:
        url = "cimetiere";
        break;
      case MONASTERE:
        url = "monastere";
        break;
      case ANTRE_HERMITE:
        url = "antre_hermite";
        break;
      case FORET:
        url = "foret_hantee";
        break;
      case SANCTUAIRE:
        url = "sanctuaire_ancien";
        break;
      case PORTE_OUTREMONDE:
        url = "porte_outremonde";
        break;
      default:
        url = "cimetiere";
    }
    return Thread.currentThread().getContextClassLoader().getResource("lieux/" + url + ".png");
  }

  public static URL personnageToUrl(String nom, boolean isRevele) {
    String url = "unrevealed";
    if (isRevele) {
      switch (nom) {
        case "Emi":
          url = "E";
          break;
        case "Georges":
          url = "G";
          break;
        case "Franklin":
          url = "F";
          break;
        case "Loup-Garou":
          url = "L";
          break;
        case "Vampire":
          url = "V";
          break;
        case "Métamorphe":
          url = "M";
          break;
        case "Allie":
          url = "A";
          break;
        case "Bob":
          url = "B";
          break;
        case "Charles":
          url = "C";
          break;
        case "Daniel":
          url = "D";
          break;
        default:
          url = "unrevealed";
      }
    }
    return Thread.currentThread().getContextClassLoader().getResource("joueurs/" + url + ".png");
  }

  public static URL personnageCardToUrl(String nom) {
    String url;
    switch (nom) {
      case "Emi":
        url = "emi";
        break;
      case "Georges":
        url = "georges";
        break;
      case "Franklin":
        url = "franklin";
        break;
      case "Loup-Garou":
        url = "loup_garou";
        break;
      case "Vampire":
        url = "vampire";
        break;
      case "Métamorphe":
        url = "metamorphe";
        break;
      case "Allie":
        url = "allie";
        break;
      case "Bob":
        url = "bob";
        break;
      case "Charles":
        url = "charles";
        break;
      case "Daniel":
        url = "daniel";
        break;
      default:
        url = "dos";
    }
    return Thread.currentThread()
        .getContextClassLoader()
        .getResource("personnages/" + url + "" + ".jpg");
  }

  public static URL carteToUrl(String nomCarte, TypeCarte typeCarte) {
    switch (typeCarte) {
      case VISION:
        return visionToUrl(nomCarte);
      case TENEBRE:
        return tenebreToUrl(nomCarte);
      case LUMIERE:
        return lumiereToUrl(nomCarte);
      default:
        return null;
    }
  }

  private static URL visionToUrl(String nomCarteVision) {
    String url = "";
    switch (nomCarteVision) {
      case "Vision furtive":
        url = "furtive";
        break;
      case "Vision mortifère":
        url = "mortifere";
        break;
      case "Vision cupide":
        url = "cupide";
        break;
      case "Vision envrante":
        url = "enivrante";
        break;
      case "Vision clairvoyante":
        url = "clairvoyante";
        break;
      case "Vision foudroyante":
        url = "foudroyante";
        break;
      case "Vision suprême":
        url = "supreme";
        break;
      case "Vision lugubre":
        url = "lugubre";
        break;
      case "Vision réconfortante":
        url = "reconfortante";
        break;
      case "Vision destructrice":
        url = "destructrice";
        break;
      case "Vision divine":
        url = "divine";
        break;
      case "Vision purificatrice":
        url = "purificatrice";
        break;
      default:
    }
    return Thread.currentThread()
        .getContextClassLoader()
        .getResource("cartes/visions/" + url + "" + ".jpg");
  }

  private static URL tenebreToUrl(String nomCarteTenebre) {
    String url = "";
    switch (nomCarteTenebre) {
      case "Tronçonneuse du mal":
        url = "tronconneuse_du_mal";
        break;
      case "Revolver des ténèbres":
        url = "revolver_des_tenebres";
        break;
      case "Mitrailleuse funeste":
        url = "mitrailleuse_funeste";
        break;
      case "Sabre hanté Masamune":
        url = "sabre_hante_masamune";
        break;
      case "Hachoir maudit":
        url = "hachoir_maudit";
        break;
      case "Hâche tueuse":
        url = "hache_tueuse";
        break;
      case "Succube tentatrice":
        url = "succube_tentatrice";
        break;
      case "Chauve-souris vampiré":
        url = "chauve_souris_vampire";
        break;
      case "Poupée démoniaque":
        url = "poupee_demoniaque";
        break;
      case "Peau de banane":
        url = "peau_de_banane";
        break;
      case "Araignée sanguinaire":
        url = "araignee_sanguinaire";
        break;
      case "Dynamite":
        url = "dynamite";
        break;
      case "Rituel diabolique":
        url = "rituel_diabolique";
        break;
      default:
    }
    return Thread.currentThread()
        .getContextClassLoader()
        .getResource("cartes/tenebres/" + url + "" + ".jpg");
  }

  private static URL lumiereToUrl(String nomCarteLumiere) {
    String url = "";
    switch (nomCarteLumiere) {
      case "Lance de Longinus":
        url = "lance_de_longinus";
        break;
      case "Crucifix en argent":
        url = "crucifix_en_argent";
        break;
      case "Boussole mystique":
        url = "boussole_mystique";
        break;
      case "Toge sainte":
        url = "toge_sainte";
        break;
      case "Broche de chance":
        url = "broche_de_chance";
        break;
      case "Amulette":
        url = "amulette";
        break;
      case "Miroir Divin":
        url = "miroir_divin";
        break;
      case "Eau Benite":
        url = "eau_benite";
        break;
      case "Ange Gardien":
        url = "ange_gardien";
        break;
      case "Eclair Purificateur":
        url = "eclair_purificateur";
        break;
      case "Premiers Secours":
        url = "premiers_secours";
        break;
      case "Benediction":
        url = "benediction";
        break;
      case "Savoir Encestral":
        url = "savoir_ancestral";
        break;
      case "Avement Supreme":
        url = "avenement_supreme";
        break;
      case "Barre De Chocolat":
        url = "barre_de_chocolat";
        break;
      default:
    }
    return Thread.currentThread()
        .getContextClassLoader()
        .getResource("cartes/lumieres/" + url + "" + ".jpg");
  }

  public static boolean isDrawableButton(String choix) {
    switch (choix.toLowerCase()) {
      case "joueur 1":
      case "joueur 2":
      case "joueur 3":
      case "joueur 4":
      case "joueur 5":
      case "joueur 6":
      case "joueur 7":
      case "joueur 8":
      case "oui":
      case "non":
        return true;
      default:
        return false;
    }
  }

  public static URL getUrlButton(String choix) {
    String url;
    switch (choix.toLowerCase()) {
      case "joueur 1":
        url = "j1.jpg";
        break;
      case "joueur 2":
        url = "j2.jpg";
        break;
      case "joueur 3":
        url = "j3.jpg";
        break;
      case "joueur 4":
        url = "j4.jpg";
        break;
      case "joueur 5":
        url = "j5.jpg";
        break;
      case "joueur 6":
        url = "j6.jpg";
        break;
      case "joueur 7":
        url = "j7.jpg";
        break;
      case "joueur 8":
        url = "j8.jpg";
        break;
      case "oui":
        url = "oui.png";
        break;
      case "non":
        url = "non.jpg";
        break;
      default:
        url = "empty.png";
    }
    return Thread.currentThread().getContextClassLoader().getResource("btns/" + url);
  }
}
