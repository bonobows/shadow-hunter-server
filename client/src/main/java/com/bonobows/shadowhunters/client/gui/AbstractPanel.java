package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.gui.component.ObservablePanel;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.util.Observer;

public abstract class AbstractPanel extends JPanel {
  protected ObservablePanel observable;

  public AbstractPanel(Observer parent) {
    super();
    this.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, CharteGraphique.jauneContour));
    observable = new ObservablePanel(parent);

    this.setBackground(CharteGraphique.noirFonce);
  }

  public abstract void init();

  public abstract void draw();
  //  public abstract void update();
}
