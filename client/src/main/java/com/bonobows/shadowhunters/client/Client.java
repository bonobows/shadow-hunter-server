package com.bonobows.shadowhunters.client;

import com.bonobows.shadowhunters.client.controller.PresenterCmd;
import com.bonobows.shadowhunters.client.model.command.CommandeManager;
import com.bonobows.shadowhunters.client.model.socket.SocketManager;
import com.bonobows.shadowhunters.client.model.socket.SocketServer;
import com.bonobows.shadowhunters.client.tool.ReceiveMessage;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

// import sun.audio.AudioPlayer;
// import sun.audio.AudioStream;

/** Created by Milihhard on 02/10/2017. */
public class Client {
  SocketServer socket;
  int nbJoueur;

  public Client() {
    boolean msgDejaAfficher = false;
    do {
      try {
        socket = new SocketServer(new Socket("127.0.0.1", 2009));
        // Si le message s'affiche c'est que je suis connecté
        System.out.println("Connexion établie avec le serveur, authentification :");
      } catch (UnknownHostException e) {
        System.err.println(
            "Impossible de se connecter à l'adresse " + socket.getSocket().getLocalAddress());
      } catch (IOException e) {
        if (!msgDejaAfficher) {
          System.err.println("Aucun serveur à l'écoute ! En attente d'un serveur...");
          msgDejaAfficher = true;
        }
      }
      try {
        TimeUnit.SECONDS.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } while (!(socket instanceof  SocketServer));
  }

  public void start(String pseudo) {
    System.out.println("Vous pouvez écrire des messages ici ");
    Thread t = null;
    SocketManager socketManager = new SocketManager(socket);
    socketManager.addCommandeManager(new CommandeManager(socketManager, new PresenterCmd(),
        pseudo));
    t = new Thread(new ReceiveMessage(socketManager));
    t.start();

    // On lance la musique de fond
    // playSound();
  }

  // private void playSound() {
  //   try {
  //     AudioStream as =
  //         new AudioStream(
  //             Thread.currentThread()
  //                 .getContextClassLoader()
  //                 .getResourceAsStream("sound/musique.wav"));
  //     AudioPlayer.player.start(as);
  //   } catch (Exception e) {
  //     // a special way i'm handling logging in this application
  //     System.out.println("tg");
  //     System.out.println(e);
  //   }
  // }
}
