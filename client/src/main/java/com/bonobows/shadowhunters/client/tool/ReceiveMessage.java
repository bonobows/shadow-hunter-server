package com.bonobows.shadowhunters.client.tool;

import com.bonobows.shadowhunters.client.model.socket.SocketManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Milihhard on 02/10/2017.
 */
public class ReceiveMessage implements Runnable {

  private BufferedReader in;
  private SocketManager socketManager;

  private String message = null;

  public ReceiveMessage(SocketManager socketManager) {

    this.socketManager = socketManager;
    try {
      this.in = new BufferedReader(new InputStreamReader(this.socketManager.getInputStream(),
          "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      this.in = new BufferedReader(new InputStreamReader(this.socketManager.getInputStream()));
    }
  }

  public void run() {

    while (true) {
      try {
        message = in.readLine();
        socketManager.receiveMessage(message);
      } catch (IOException e) {
        System.out.println("Le serveur s'est déconnecté");
        try {
          socketManager.close();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
        return;
      }
    }
  }
}
