package com.bonobows.shadowhunters.client.controller;

import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.commande.LogLevel;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PresenterCmd extends AbstractPresenter {

  Scanner reader;

  public PresenterCmd() {
    reader = new Scanner(System.in);
  }

  @Override
  public void updateJoueurNumber(int i) {
    super.updateJoueurNumber(i);
    System.out.println("Vous êtes le joueur " + i);
  }

  @Override
  public void showLog(LogLevel logLevel, String log) {
    System.out.println(logLevel.toString() + " : " + log);
  }

  @Override
  public void updateTerrain(Terrain terrain) {
    System.out.println("Listes des Lieux:\n" + terrain);
  }

  @Override
  public void moveJoueur(Joueur joueurMove, Lieu lieu) {
    if (this.joueur == joueurMove.getIdJoueur()) {
      System.out.print("Vous vous déplacez en ");
    } else {
      System.out.print("Le joueur " + joueurMove.getIdJoueur() + " se déplace en ");
    }
    System.out.println("\"" + lieu.getNom() + "\"");
  }

  @Override
  public void askForDice() {
    System.out.println("Appuyer sur entree pour lancer les dés…");
    reader.nextLine();
  }

  @Override
  public String askForRevele() {
    String reponse = "";
    while (!reponse.equals("o") && !reponse.equals("n")) {
      System.out.println("Voulez-vous vous révéler ? [O/n]");
      reponse = reader.nextLine().toLowerCase();
    }
    // return reponse;
    return askForQuestionYesNoString("Voulez-vous vous révéler ?");
  }

  @Override
  public String askForQuestionYesNoString(String question) {
    String n;
    do {
      System.out.println(question + " [O/n]");
      n = reader.nextLine().toLowerCase();
      if (!"o".equals(n) && !"n".equals(n)) {
        System.out.println("Mauvaise réponse!");
      }
    } while (!"o".equals(n) && !"n".equals(n));
    return n;
  }

  @Override
  public boolean askForQuestionYesNo(String question) {
    return "o".equals(askForQuestionYesNoString(question));
  }

  @Override
  public int selectPlayer(String question, List<Joueur> joueurPorte) {
    boolean joueurSelectOk = false;
    int joueurSelect;
    System.out.println(question + " [1-8]");
    do {
      joueurSelect = reader.nextInt();
      for (Joueur j : joueurPorte) {
        if (j.getIdJoueur() == joueurSelect) {
          joueurSelectOk = true;
        }
      }

    } while (!joueurSelectOk);
    return joueurSelect;
  }

  @Override
  public int selectPlayer(
      String question,
      int nbJoueur,
      int joueur,
      boolean chooseHimSelf,
      List<Integer> joueursMorts) {
    System.out.println(question + " [1-" + nbJoueur + "]");
    Scanner reader = new Scanner(System.in);
    int choix = 0;
    while (!((choix >= 1 && choix <= nbJoueur) && (choix != joueur || chooseHimSelf))) {
      choix = reader.nextInt();
    }
    System.out.println("Attente de la réponse de l'utilisateur...");
    return choix;
  }

  @Override
  public int selectPlayerInt(String question, List<Integer> joueurPorte) {
    int joueur;
    do {
      System.out.println(question);
      System.out.println(joueurPorte);
      joueur = reader.nextInt();
    } while (!joueurPorte.contains(joueur));
    return joueur;
  }

  @Override
  public char askCapaciteEmi() {
    System.out.println("Voulez-vous vous déplacer sur le lieu adjacent ou lancer les dés ? [a/d]");
    Scanner reader = new Scanner(System.in);
    String n = "";
    while (!n.equals("a") && !n.equals("d")) {
      n = reader.nextLine().toLowerCase();
    }
    return n.charAt(0);
  }

  @Override
  public int askBoussole(int choix1, int choix2) {
    System.out.println("Votre boussole mystique vous permet de  choisir entre ces deux résultats.");
    int choix;
    do {
      System.out.println(choix1 + " ou " + choix2 + " ?");
      choix = reader.nextInt();
    } while (!(choix == choix1 || choix == choix2));
    return choix;
  }

  @Override
  public NomLieu askLieuChoix(Terrain terrain) {
    System.out.println("Choisissez votre lieux :");
    int i = 1;
    for (Lieu lieu : terrain.getLieux()) {
      System.out.println(i + ") " + lieu.getNom());
      i++;
    }
    int choix;
    do {
      choix = reader.nextInt();
    } while (choix < 1 && choix > NomLieu.values().length);
    return terrain.getLieux().get(choix - 1).getNom();
  }

  @Override
  public int choixPorteOutremonde() {
    System.out.println(
        "Voulez-vous piocher une carte vision, lumière, ténèbre ou ne rien faire ? "
            + "[V/L/T/N] (V : vision, L: lumière, T: ténèbre, N: rien)");
    String n = "";
    while (!n.equals("v") && !n.equals("l") && !n.equals("t") && !n.equals("n")) {
      n = reader.nextLine().toLowerCase();
    }
    int choixPioche;
    switch (n) {
      case "v":
        choixPioche = 1;
        break;
      case "l":
        choixPioche = 2;
        break;
      case "t":
        choixPioche = 3;
        break;
      default:
        choixPioche = 0;
    }
    return choixPioche;
  }

  @Override
  public String choixForetHante() {
    System.out.println(
        "Voulez-vous faire subir 2 Blessures, soigner 1 Blessure ou ne rien faire ? "
            + "[D/S/N] (D: dégat, S: soin, N: rien)");
    String r = "";
    while (!r.equals("d") && !r.equals("s") && !r.equals("n")) {
      r = reader.nextLine().toLowerCase();
    }
    return r;
  }

  @Override
  public ActionVision effetVision(List<ActionVision> actionVisionList) {
    System.out.println("Choix :");
    StringBuilder action = new StringBuilder("Entrez une action [");
    List<Character> rightLetters = new ArrayList<>();
    for (ActionVision actionVision : actionVisionList) {
      System.out.println(actionVision.getLetter() + ") " + actionVision);
      // actionVisionList.add(actionVision);
      rightLetters.add(actionVision.getLetter());
      action.append(actionVision.getLetter()).append("/");
    }
    Scanner reader = new Scanner(System.in); // Reading from System.in
    char reponse;
    do {
      System.out.println(action.substring(0, action.length() - 1) + "]");
      reponse = reader.nextLine().toLowerCase().charAt(0);
    } while (!rightLetters.contains(reponse));
    return ActionVision.getActionVisionByLetter(reponse);
  }

  @Override
  public Equipement selectEquipement(String question, List<Equipement> equipements) {
    System.out.println(question);
    int i = 1;
    for (Equipement equipement : equipements) {
      System.out.println(
          i + ") " + equipement.getNom() + " (effet : " + equipement.getEffet() + ")");
      equipements.add(equipement);
      i++;
    }
    Scanner reader = new Scanner(System.in);
    int reponseEquipement;
    do {
      reponseEquipement = reader.nextInt();
    } while (reponseEquipement < 1 && reponseEquipement >= i);
    return equipements.get(reponseEquipement - 1);
  }

  @Override
  public int selectEquipementString(String question, List<String> equipements) {
    int eqt;
    do {
      System.out.println(question);
      int i = 1;
      for (String e : equipements) {
        System.out.println(i + ") " + e);
        i++;
      }
      eqt = reader.nextInt();
    } while (eqt > equipements.size() || eqt <= 0);
    return eqt;
  }

  @Override
  public boolean askStart() {
    return "o".equals(askForQuestionYesNo("Voulez-vous commencer la partie  maintenant ?"));
  }

  @Override
  public void showCard(Carte carte) {
    switch (carte.getType()) {
      case LUMIERE:
        System.out.println("Carte Équipement lumière obtenue : \n\t" + carte);
        break;
      case TENEBRE:
        System.out.println("Carte Équipement ténèbre obtenue : \n\t" + carte);
        break;
      case VISION:
        System.out.println("Carte vision pioché : \n\t" + carte);
        break;
      default:
    }
  }

  @Override
  public void showPersonnageVision(Personnage personnage) {
    System.out.println("Voici le personnage du joueur : \n" + personnage);
  }

  @Override
  public void mortJoueur(Joueur joueur) {}

  @Override
  public void initJoueurs(List<Joueur> joueurs) {}

  @Override
  public void majJoueur(Joueur joueur) {}

  @Override
  public void newTurn(int turn) {}

  @Override
  public void chat(String message, int joueurChat) {
    System.out.println("Joueur " + joueurChat + " : " + message);
  }
}
