package com.bonobows.shadowhunters.client.gui.component;

import com.bonobows.shadowhunters.client.gui.CharteGraphique;
import com.bonobows.shadowhunters.client.gui.JoueurGui;
import com.bonobows.shadowhunters.client.gui.image.StretchIcon;
import com.bonobows.shadowhunters.client.gui.image.UrlConverter;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class JoueurComponent extends JPanel {

  private Joueur joueur;
  private boolean isProrietaire;
  private boolean isJoueurCourrant;

  public JoueurComponent(Joueur j, boolean isProrietaire) {
    super();
    this.joueur = j;
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    this.isProrietaire = isProrietaire;
    this.setBackground(CharteGraphique.noirFonce);
  }

  public void init() {
    addMouseListener(
        new MouseAdapter() {
          public void mousePressed(MouseEvent me) {
            JoueurGui frame = new JoueurGui(joueur, isProrietaire);
            frame.init();
            frame.draw();
          }
        });
    addMouseListener(
        new MouseAdapter() {
          public void mouseEntered(MouseEvent me) {
            setBackground(CharteGraphique.violet);
          }
        });
    addMouseListener(
        new MouseAdapter() {
          public void mouseExited(MouseEvent me) {
            setBackground(getParent().getBackground());
          }
        });
    StringBuilder tooltip = new StringBuilder();
    tooltip.append("<html>");
    tooltip.append(joueur.getIdJoueurStringify());
    tooltip.append("<br>");
    tooltip.append("<br>");
    tooltip.append("Mort : ");
    tooltip.append(joueur.isDead() ? "Oui" : "Non");
    tooltip.append("<br>");
    if (joueur.getEqts().size() > 0) {
      tooltip.append("Equipements : ");
      tooltip.append("<br>");

      for (Equipement equipement : joueur.getEqts()) {
        tooltip.append(equipement.getNom());
      }
    }

    tooltip.append("</html>");
    setToolTipText(tooltip.toString());
  }

  public void setCourrant(boolean isJoueurCourrant) {
    this.isJoueurCourrant = isJoueurCourrant;
  }

  public void draw() {
    this.removeAll();

    String nomPerso = joueur.getPersonnage() != null ? joueur.getPersonnage().getNom() : "";
    StretchIcon icon =
        new StretchIcon(
            UrlConverter.personnageToUrl(nomPerso, this.isProrietaire || joueur.isRevele()));
    JLabel square = new JLabel(icon);
    square.setBackground(Color.magenta);
    square.setBorder(
        BorderFactory.createMatteBorder(2, 2, 2, 2, joueur.getCouleur().transformToColor()));
    square.setPreferredSize(new Dimension(50, 50));
    square.setMaximumSize(new Dimension(50, 50));
    Box topJoeur = Box.createHorizontalBox();
    topJoeur.add(square);
    JLabel pseudoLabel = new JLabel(joueur.getCouleur().toString());
    pseudoLabel.setForeground(CharteGraphique.jauneTexte);
    pseudoLabel.setFont(CharteGraphique.getFont());
    pseudoLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
    Box topRightJoueur = Box.createVerticalBox();
    topRightJoueur.add(pseudoLabel);
    JLabel joueurLabel;
    if (joueur.getUtilisateur() != null) {
      joueurLabel =
          new JLabel(
              joueur.getUtilisateur().getPseudo() + " (" + joueur.getIdJoueurStringify() + ")");
    } else {
      joueurLabel = new JLabel(" (J" + joueur.getIdJoueur() + ")");
    }
    if (joueur.isDead()) {
      joueurLabel.setForeground(Color.gray);
    }
    joueurLabel.setForeground(CharteGraphique.jauneTexte);
    joueurLabel.setFont(CharteGraphique.getFont());
    joueurLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
    topRightJoueur.add(joueurLabel);
    topJoeur.add(topRightJoueur);
    this.add(topJoeur);
    topJoeur.setAlignmentX(Component.LEFT_ALIGNMENT);
    JLabel equipementLabel;
    equipementLabel = new JLabel(joueur.isDead() ? " " : joueur.getEqts().size() + " équipements");
    equipementLabel.setForeground(CharteGraphique.jauneTexte);
    equipementLabel.setFont(CharteGraphique.getFont());
    equipementLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
    this.add(equipementLabel);
    this.setForeground(CharteGraphique.jauneTexte);
    if (isJoueurCourrant) {
      this.setBorder(new CompoundBorder(new EmptyBorder(3, 27, 3, 27),
          BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red)));
    } else {
      this.setBorder(new EmptyBorder(5, 29, 5, 29));
    }
  }

  public void setJoueur(Joueur joueur) {
    this.joueur = joueur;
  }
}
