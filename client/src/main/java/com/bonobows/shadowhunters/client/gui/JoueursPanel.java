package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.gui.component.JoueurComponent;
import com.bonobows.shadowhunters.common.model.Joueur;

import javax.swing.Box;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observer;

public class JoueursPanel extends AbstractPanel {

  List<Joueur> joueurs;
  Map<Integer, JoueurComponent> joueurComponents;
  Box box;
  int idJoueurCourrant;

  public JoueursPanel(Observer observer) {
    super(observer);
    this.setPreferredSize(new Dimension(1300, 90));
  }

  @Override
  public void init() {
    this.joueurs = new ArrayList<>();
    this.joueurComponents = new HashMap<>();
    box = Box.createHorizontalBox();
    this.add(box);
  }

  public void setJoueurs(List<Joueur> joueurs, int proprietaireId) {
    this.joueurs = joueurs;

    createJoueurComponents(proprietaireId);

    draw();
  }

  private void createJoueurComponents(int proprietaireId) {
    joueurComponents.clear();
    box.removeAll();

    for (Joueur joueur : joueurs) {
      boolean isProrietaire = (joueur.getIdJoueur() ==  proprietaireId) ? true : false;
      JoueurComponent joueurComponent = new JoueurComponent(joueur, isProrietaire);
      joueurComponent.init();
      box.add(joueurComponent);
      joueurComponents.put(joueur.getIdJoueur(), joueurComponent);
    }
  }

  public void majJoueur(Joueur newJoueur, int proprietaireId) {
    boolean isNewJoueur = true;
    for (Joueur joueur : joueurs) {
      if (joueur.getPersonnage().getNom().equals(newJoueur.getPersonnage().getNom())) {
        isNewJoueur = false;
        this.joueurs.set(this.joueurs.indexOf(joueur), newJoueur);
      }
    }
    if (isNewJoueur) {
      this.joueurs.add(newJoueur);
      createJoueurComponents(proprietaireId);
    }
    draw();
  }

  public void setJoueurCourrant(int idJoueurCourrant) {
    this.idJoueurCourrant = idJoueurCourrant;
    draw();
  }

  @Override
  public void draw() {
    for (Joueur j : this.joueurs) {
      JoueurComponent joueurComponent = joueurComponents.get(j.getIdJoueur());
      if (joueurComponent != null) {
        joueurComponent.setJoueur(j);
        boolean isJoueurCourrant = (this.idJoueurCourrant == j.getIdJoueur()) ? true : false;
        joueurComponent.setCourrant(isJoueurCourrant);
        joueurComponent.draw();
      }
    }
    observable.pack();
  }
}
