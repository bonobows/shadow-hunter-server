package com.bonobows.shadowhunters.client.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;

public class CharteGraphique {
  public static final Color noirFonce = new Color(51, 51, 51);
  public static final Color noirClair = new Color(127, 127, 127);
  public static final Color jauneContour = new Color(200, 126, 37);
  public static final Color jauneTexte = new Color(248, 226, 62);
  public static final Color violet = new Color(94, 62, 248);

  private static Font font = null;

  public static Font getFont() {
    if (font == null) {
      try {
        font = Font.createFont(Font.TRUETYPE_FONT,
            CharteGraphique.class.getResourceAsStream("/fonts/BLKCHCRY.TTF"));
        GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
        genv.registerFont(font);
        // makesure to derive the size
        font = font.deriveFont(12f);
      } catch (IOException | FontFormatException e) {
        e.printStackTrace();
      }
    }
    return font;
  }

  public static void setFont(Font font1) {
    font = font1;
  }

}
