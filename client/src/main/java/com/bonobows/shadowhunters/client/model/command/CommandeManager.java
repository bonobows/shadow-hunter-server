package com.bonobows.shadowhunters.client.model.command;

import com.bonobows.shadowhunters.client.controller.Presenter;
import com.bonobows.shadowhunters.client.model.socket.SocketManager;
import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.CarteVision;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.argument.Argument;
import com.bonobows.shadowhunters.common.model.commande.Commande;
import com.bonobows.shadowhunters.common.model.commande.LogLevel;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;
import com.bonobows.shadowhunters.common.tool.JsonTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommandeManager {

  private SocketManager socketManager;
  private int joueur = 1;
  private Terrain terrain;
  private Presenter presenter;
  private List<Joueur> joueurs;
  private List<Integer> joueursMortsId;
  private String pseudo;

  public CommandeManager(SocketManager socketManager, Presenter presenter, String pseudo) {
    this.socketManager = socketManager;
    this.presenter = presenter;
    this.pseudo = pseudo;
    joueursMortsId = new ArrayList<>();
  }

  public void interpreterCommande(Commande commandeToInterprete) {
    if (commandeToInterprete != null) {
      switch (commandeToInterprete.getCode()) {
        case LOG:
          interpreterLog(commandeToInterprete);
          break;
        case ATTAC:
          interpreterAttac(commandeToInterprete);
          break;
        case TERRAIN:
          interpreterTerrain(commandeToInterprete);
          break;
        case JOUEUR:
          interpreterJoueur(commandeToInterprete);
          break;
        case TURN:
          interpreterTurn(commandeToInterprete);
          break;
        case PERSONNAGE:
          interpreterPersonnage(commandeToInterprete);
          break;
        case MOVE:
          interpreterMove(commandeToInterprete);
          break;
        case DE:
          interpreterDe(commandeToInterprete);
          break;
        case RES_BOUSSOLE:
          interpreterResBoussole(commandeToInterprete);
          break;
        case DECHOIX:
          interpreterDeChoix(commandeToInterprete);
          break;
        case LIEU_CHOIX:
          interpreterLieuChoix(commandeToInterprete);
          break;
        case EFFET_LIEU:
          interpreterEffetLieu(commandeToInterprete);
          break;
        case VISION:
          interpreterVision(commandeToInterprete);
          break;
        case CIBLE_VOL_CHOIX:
          interpreterJoueurChoix(commandeToInterprete);
          break;
        case GO_CHOISIR_EQT_A_VOLER:
          interpreterGoChoisirEqtAVoler(commandeToInterprete);
          break;
        case SE_REVELER:
          interpreterSeReveler(commandeToInterprete);
          break;
        case LANCER_DES:
          interpreterLancerDes(commandeToInterprete);
          break;
        case PERSONNAGE_VISION:
          interpreterPersonnageVision(commandeToInterprete);
          break;
        case UTILISER_CAPACITE:
          interpreterUtiliserCapacite(commandeToInterprete);
          break;
        case CAPACITE_FRANKLIN:
          interpreterCapaciteFranklin(commandeToInterprete);
          break;
        case CAPACITE_GEORGES:
          interpreterCapaciteGeorges(commandeToInterprete);
          break;
        case FIN_PARTIE:
          interpreterFinPartie();
          break;
        case CAPACITE_LOUP_GAROU:
          interpreterCapaciteLoupGarou(commandeToInterprete);
          break;
        case CARTE_IMMEDIATE:
          interpreterCarteImmediate(commandeToInterprete);
          break;
        case CAPACITE_EMI:
          interpreterCapaciteEmi();
          break;
        case SEND_JOUEURS:
          interpreterSendJoueurs(commandeToInterprete);
          break;
        case CHAT:
          interpreterChat(commandeToInterprete);
          break;
        case UPDATE_JOUEUR:
          interpreterUpdateJoueur(commandeToInterprete);
          break;
        case DEMARRER_PARTIE:
          interpreterDemarrerPartie();
          break;
        case SHOW_CARD_LT:
          interpreterShowCardLt(commandeToInterprete);
          break;
        case MORT_JOUEUR:
          interpreterMortJoueur(commandeToInterprete);
          break;
        default:
          System.out.println("Je ne comprends pas la commande");
          System.out.println(commandeToInterprete.getCode());
      }
      commandeToInterprete = null;
    } else {
      System.out.println("commande null");
    }
  }

  private void interpreterLog(Commande commande) {
    try {
      presenter.showLog(
          (LogLevel) JsonTools.toObject(commande, Argument.Log.LOG_LEVEL, LogLevel.class),
          commande.getArg(Argument.Log.LOG_MESSAGE).toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void interpreterAttac(Commande commande) {
    ArrayList<Joueur> joueursAPortee = new ArrayList<>();
    Boolean possedeSabre = false;
    Boolean possedeMitrailleuse = false;
    try {
      joueursAPortee =
          (ArrayList<Joueur>)
              JsonTools.toObject(commande, Argument.Attac.JOUEURS_A_PORTEE, ArrayList.class);
      possedeSabre =
          (Boolean)
              JsonTools.toObject(
                  commande.getArg(Argument.Attac.POSSEDE_SABRE).toString(), Boolean.class);
      possedeMitrailleuse =
          (Boolean)
              JsonTools.toObject(
                  commande.getArg(Argument.Attac.POSSEDE_MITRAILLEUSE).toString(), Boolean.class);
    } catch (IOException e) {
      System.out.println(e);
    }
    // commande.getArg(Argument.Attac.JOUEURS_A_PORTEE);
    // On affiche au joueur la liste des perso qu'il peut attaquer

    StringBuilder textSafe = new StringBuilder();
    String textListeJoueurs = "[";
    List<Joueur> joueursAPorteeReconstitue = new ArrayList<>();
    for (Object jo : joueursAPortee) {
      Joueur j;
      try {
        j = (Joueur) JsonTools.toObject(jo.toString(), Joueur.class);
        if (joueursAPortee.indexOf(jo) != 0) {
          textSafe.append(", ");
        }
        textSafe
            .append(j.getIdJoueurStringify())
            .append("(")
            .append(j.getNbBlessures())
            .append(" blessures)");
        joueursAPorteeReconstitue.add(j);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (joueursAPortee.size() == 0) {
      System.out.println("Vous ne pouvez attaquer personne !");
      socketManager.sendToServer(CommandeFactory.attac(-1));
    } else {
      String n = "";
      if (possedeSabre) {
        System.out.println("Votre sabre vous oblige à attaquer parmi : " + textSafe);
        // System.out.println("Quel joueur attaquez-vous ? [1-8]");
        selectionJoueur(joueursAPorteeReconstitue, true);
      } else if (possedeMitrailleuse) {
        System.out.println(
            "Votre mitrailleuse vous permet d'attaquer tous les joueurs à votre "
                + "portée : "
                + textSafe);
        boolean question = presenter.askForQuestionYesNo("Voulez-vous tous les attaquer ?");
        if (question) {
          socketManager.sendToServer(CommandeFactory.attac(42)); // 42 is for attac all
        } else {
          socketManager.sendToServer(CommandeFactory.attac(-1));
        }
      } else {
        System.out.println("Vous pouvez attaquer : " + textSafe);
        boolean question = presenter.askForQuestionYesNo("Souhaitez-vous attaquer ?");
        if (question) {
          // System.out.println("Quel joueur souhaitez-vous attaquer ? [1-8]");
          selectionJoueur(joueursAPorteeReconstitue, false);
        } else {
          socketManager.sendToServer(CommandeFactory.attac(-1));
        }
      }
    }
  }

  private void selectionJoueur(List<Joueur> joueursAPorteeReconstitue, boolean hasSabre) {
    int joueurSelect =
        presenter.selectPlayer(
            hasSabre ? "Quel joueur attaquez-vous ?" : "Quel joueur souhaitez-vous" + " attaquer ?",
            joueursAPorteeReconstitue);
    System.out.println("Envoie de la commande attac du joueur " + joueurSelect);

    socketManager.sendToServer(CommandeFactory.attac(joueurSelect));
  }

  private void interpreterSendJoueurs(Commande commande) {
    try {
      ArrayList<Joueur> initJoueurs = new ArrayList<>();
      initJoueurs =
          (ArrayList<Joueur>)
              JsonTools.toObject(
                  commande, Argument.SendJoueurs.SEND_JOEURS_JOUEURS, ArrayList.class);

      ArrayList<Joueur> joueursReconstitue = new ArrayList<>();
      for (Object jo : initJoueurs) {
        Joueur j;
        try {
          j = (Joueur) JsonTools.toObject(jo.toString(), Joueur.class);
          joueursReconstitue.add(j);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      this.joueurs = joueursReconstitue;
      presenter.initJoueurs(joueurs);
    } catch (IOException e) {
      System.out.println(e);
    }
  }

  private void interpreterTerrain(Commande commande) {
    System.out.println("Liste des Lieux :");
    try {
      this.terrain =
          (Terrain) JsonTools.toObject(commande, Argument.Terrain.TERRAIN_TERRAIN, Terrain.class);
      presenter.updateTerrain(terrain);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void interpreterTurn(Commande commande) {
    int turn = (int) commande.getArg(Argument.Tour.TOUR_JOUEUR);
    presenter.newTurn(turn);
    if (turn == joueur) {
      presenter.showLog(LogLevel.INFORMATION, "C'est votre tour !");
    } else {
      presenter.showLog(LogLevel.INFORMATION, "C'est le tour du joueur " + turn + ".");
    }
  }

  private void interpreterLancerDes(Commande commande) {
    presenter.askForDice();
    socketManager.sendToServer(CommandeFactory.turn());
  }

  private void interpreterUtiliserCapacite(Commande commande) {
    String n =
        presenter.askForQuestionYesNoString("Voulez-vous utiliser votre capacité spéciale ?");
    socketManager.sendToServer(CommandeFactory.utiliserCapacite(n));
  }

  private void interpreterCapaciteFranklin(Commande commande) {
    // System.out.println("Sur quel joueur ? [1-8]");
    // Scanner reader = new Scanner(System.in);
    // int choix = 0;
    // while (!(choix >= 1 && choix <= 8)) {
    //   choix = reader.nextInt();
    // }
    int choix =
        presenter.selectPlayer("Sur quel joueur ?", joueurs.size(), joueur, false, joueursMortsId);
    socketManager.sendToServer(CommandeFactory.capaciteFranklin(choix));
  }

  private void interpreterCapaciteGeorges(Commande commande) {
    int choix =
        presenter.selectPlayer("Sur quel joueur ?", joueurs.size(), joueur, false, joueursMortsId);
    socketManager.sendToServer(CommandeFactory.capaciteGeorges(choix));
  }

  private void interpreterCapaciteLoupGarou(Commande commande) {
    int attaquant = (int) commande.getArg(Argument.CapaciteLoupGarou.CAPACITE_LOUP_GAROU_ATTAQUANT);

    String reponse =
        presenter.askForQuestionYesNoString("Voulez-vous contre attaquer J" + attaquant + "?");
    socketManager.sendToServer(CommandeFactory.capaciteLoupGarou(attaquant, this.joueur, reponse));
  }

  private void interpreterCapaciteEmi() {
    socketManager.sendToServer(CommandeFactory.capaciteEmi(presenter.askCapaciteEmi()));
  }

  private void interpreterJoueur(Commande commande) {
    this.joueur = (int) commande.getArg(Argument.Joueur.JOUEUR_JOUEUR);
    presenter.updateJoueurNumber(this.joueur);
  }

  private void interpreterPersonnage(Commande commande) {
    try {
      Personnage p =
          (Personnage)
              JsonTools.toObject(
                  commande, Argument.Personnage.PERSONNAGE_PERSONNAGE, Personnage.class);
      System.out.println("Vous avez le personnage : " + p.toString());
    } catch (IOException e) {
      e.printStackTrace();
      System.out.println("error generating personnage");
    }
  }

  private void interpreterMove(Commande commande) {
    int joueurMove = (int) commande.getArg(Argument.Move.MOVE_JOUEUR);
    try {
      Lieu l = (Lieu) JsonTools.toObject(commande, Argument.Move.MOVE_LIEU, Lieu.class);
      System.out.println("\"" + l.getNom() + "\"");
      presenter.moveJoueur(joueurs.get(joueurMove - 1), l);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void interpreterDe(Commande commande) {
    System.out.println("Valeur dé :");
    if (commande.getArg(Argument.ArgDe.DE_SIX) != null) {
      System.out.println("Dé 6 : " + commande.getArg(Argument.ArgDe.DE_SIX).toString());
    }
    if (commande.getArg(Argument.ArgDe.DE_QUATRE) != null) {
      System.out.println("Dé 4 : " + commande.getArg(Argument.ArgDe.DE_QUATRE).toString());
    }
    if (commande.getArg(Argument.ArgDe.DE_TOTAL) != null) {
      System.out.println("Total : " + commande.getArg(Argument.ArgDe.DE_TOTAL).toString());
    }
  }

  private void interpreterResBoussole(Commande commande) {
    for (Lieu l : terrain.getLieux()) {
      for (Integer i : l.getValeurDe()) {
        System.out.print(i + " ");
      }
      System.out.println("-> " + l.getNom());
    }
    System.out.println("La boussole mystique a généré les résultats suivants :");
    System.out.print("Résultat 1 -> ");
    if (commande.getArg(Argument.ArgDe.DE_SIX) != null) {
      System.out.print("dé 6 = " + commande.getArg(Argument.ArgDe.DE_SIX).toString());
    }
    if (commande.getArg(Argument.ArgDe.DE_QUATRE) != null) {
      System.out.print("; dé 4 = " + commande.getArg(Argument.ArgDe.DE_QUATRE).toString());
    }
    if (commande.getArg(Argument.ArgDe.DE_TOTAL) != null) {
      System.out.print("; total = " + commande.getArg(Argument.ArgDe.DE_TOTAL).toString());
    }

    System.out.print("\nRésultat 2 -> ");
    if (commande.getArg(Argument.ArgDe.DE_SIX_2) != null) {
      System.out.print("dé 6 = " + commande.getArg(Argument.ArgDe.DE_SIX_2).toString());
    }
    if (commande.getArg(Argument.ArgDe.DE_QUATRE_2) != null) {
      System.out.print("; dé 4 = " + commande.getArg(Argument.ArgDe.DE_QUATRE_2).toString());
    }
    if (commande.getArg(Argument.ArgDe.DE_TOTAL_2) != null) {
      System.out.println("; total = " + commande.getArg(Argument.ArgDe.DE_TOTAL_2).toString());
    }
  }

  private void interpreterDeChoix(Commande commande) {
    int total1 = Integer.valueOf(commande.getArg(Argument.ArgDe.DE_TOTAL).toString());
    int total2 = Integer.valueOf(commande.getArg(Argument.ArgDe.DE_TOTAL_2).toString());
    int choix = presenter.askBoussole(total1, total2);
    socketManager.sendToServer(CommandeFactory.resBoussoleChoisi(choix));
  }

  private void interpreterLieuChoix(Commande commande) {
    socketManager.sendToServer(CommandeFactory.lieuChoix(presenter.askLieuChoix(terrain)));
  }

  private void interpreterSeReveler(Commande commande) {
    String reponse = presenter.askForQuestionYesNoString("Voulez-vous vous révéler ?");
    socketManager.sendToServer(CommandeFactory.seReveler(reponse));
  }

  private void interpreterEffetLieu(Commande commande) {
    try {
      NomLieu typeLieu =
          (NomLieu) JsonTools.toObject(commande, Argument.EffetLieu.EFFET_LIEU_TYPE, NomLieu.class);
      switch (typeLieu) {
        case PORTE_OUTREMONDE:
          int choixPioche = presenter.choixPorteOutremonde();
          socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu, choixPioche));
          break;
        case ANTRE_HERMITE:
          CarteVision carteVision =
              (CarteVision)
                  JsonTools.toObject(
                      commande, Argument.EffetLieu.EFFET_LIEU_CARTE, CarteVision.class);
          presenter.showCard(carteVision);
          // int joueur;
          // do {
          //   System.out.println("A qui voulez-vous donner cette carte vision ? [1-4]");
          //   joueur = reader.nextInt();
          // } while (joueur < 1 || joueur > 4 || joueur == this.joueur);
          int choixJoueurVision =
              presenter.selectPlayer(
                  "A qui voulez-vous donner cette carte " + "vision ?",
                  joueurs.size(),
                  joueur,
                  false,
                  joueursMortsId);
          presenter.clearCard();
          socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu, choixJoueurVision));
          break;
        case SANCTUAIRE:
          // tant qu'il n'y a pas de logique metier coté client, on ne peut pas effectuer
          // d'opérations genre verifier qu'un joueur a bien un equipement...
          socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu));
          break;
        case MONASTERE:
          Equipement carteL =
              (Equipement)
                  JsonTools.toObject(
                      commande, Argument.EffetLieu.EFFET_LIEU_CARTE, Equipement.class);
          presenter.showCard(carteL);
          socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu));
          // TODO procéder à l'ajout de l'equipement dans la logique métier (objet joueur
          // adéquat...)
          break;
        case FORET:
          String r = presenter.choixForetHante();
          if (r.equals("d")) {
            int choixJoueur =
                presenter.selectPlayer(
                    "Qui voulez-vous toucher ?", joueurs.size(), joueur, true, joueursMortsId);
            // do {
            //   System.out.println("Qui voulez-vous toucher ? [1-4]");
            //   choixJoueur = reader.nextInt();
            // } while (choixJoueur < 1 || choixJoueur > 4);
            this.socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu, choixJoueur));
          } else if (r.equals("s")) {
            int choixJoueur =
                presenter.selectPlayer(
                    "Qui voulez-vous soigner ?", joueurs.size(), joueur, true, joueursMortsId);
            ;
            // do {
            //   System.out.println("Qui voulez-vous soigner ? [1-4]");
            //   choixJoueur = reader.nextInt();
            // } while (choixJoueur < 1 || choixJoueur > 4);
            socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu, -1 * choixJoueur));
          } else {
            socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu, 0));
          }
          break;
        case CIMETIERE:
          // TODO gérer les cartes "immediat" qui ne sont pas des eqts
          Equipement carteT =
              (Equipement)
                  JsonTools.toObject(
                      commande, Argument.EffetLieu.EFFET_LIEU_CARTE, Equipement.class);
          presenter.showCard(carteT);
          socketManager.sendToServer(CommandeFactory.effetLieu(typeLieu));
          // TODO procéder à l'ajout de l'equipement dans la logique métier (objet joueur
          // adéquat...)
          break;
        default:
          break;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void interpreterVision(Commande commande) {
    System.out.println(
        "Le joueur "
            + commande.getArg(Argument.Vision.VISION_JOUEUR)
            + " vous a donné une carte vision");
    try {
      CarteVision carteVision =
          (CarteVision)
              JsonTools.toObject(commande, Argument.Vision.VISION_CARTE, CarteVision.class);
      System.out.println(carteVision);
      presenter.showCard(carteVision);
      List<String> actionVisions =
          (List) JsonTools.toObject(commande, Argument.Vision.VISION_CHOIX, List.class);
      List<ActionVision> actionVisionList = new ArrayList<>();
      for (String actionVisionStr : actionVisions) {
        ActionVision actionVision =
            (ActionVision) JsonTools.toObject(actionVisionStr, ActionVision.class);
        actionVisionList.add(actionVision);
      }
      ActionVision actionChoisi = presenter.effetVision(actionVisionList);
      if (actionChoisi == ActionVision.DONNER_EQUIPEMENT) {
        List<String> equipementsJson =
            (List) JsonTools.toObject(commande, Argument.Vision.VISION_EQUIPEMENTS, List.class);
        List<Equipement> equipements = new ArrayList<>();
        for (String equipementJson : equipementsJson) {
          Equipement equipement = (Equipement) JsonTools.toObject(equipementJson, Equipement.class);
          equipements.add(equipement);
        }
        Equipement equipement =
            presenter.selectEquipement(
                "Choisissez " + "l'équipement que vous voulez donner :", equipements);
        presenter.clearCard();
        socketManager.sendToServer(CommandeFactory.vision(actionChoisi, equipement));
      } else {
        presenter.clearCard();
        socketManager.sendToServer(CommandeFactory.vision(actionChoisi));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void interpreterPersonnageVision(Commande commande) {
    try {
      presenter.showPersonnageVision(
          (Personnage)
              JsonTools.toObject(
                  commande, Argument.Personnage.PERSONNAGE_PERSONNAGE, Personnage.class));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void interpreterJoueurChoix(Commande commande) {
    List<Integer> possibilites = new ArrayList<>();
    try {
      possibilites.addAll(
          (ArrayList)
              JsonTools.toObject(
                  commande, Argument.Sanctuaire.SANCTUAIRE_JOUEURS_A_VOLER, ArrayList.class));
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Vous pouvez voler un équipement à un joueur.");
    int joueurEquipement =
        presenter.selectPlayerInt("À qui voulez vous voler un équipement ?", possibilites);
    socketManager.sendToServer(CommandeFactory.cibleVol(joueurEquipement - 1));
  }

  private void interpreterGoChoisirEqtAVoler(Commande commande) {
    List<String> eqts = new ArrayList<>();
    List<Equipement> possibilites = new ArrayList<>();
    try {
      eqts.addAll(
          (ArrayList)
              JsonTools.toObject(
                  commande, Argument.Sanctuaire.SANCTUAIRE_EQTS_A_VOLER, ArrayList.class));
    } catch (IOException e) {
      e.printStackTrace();
    }

    for (Object o : eqts) {
      Equipement eq = null;
      try {
        eq = (Equipement) JsonTools.toObject(o.toString(), Equipement.class);
        possibilites.add(eq);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    // for (int i = 0; i < possibilites.size(); i++) {
    //   System.out.println(
    //       "["
    //           + (i + 1)
    //           + "] pour voler l'équipemeent nommé "
    //           + possibilites.get(i).getNom()
    //           + " et dont l'effet est : "
    //           + possibilites.get(i).getEffet());
    // }
    // int eqt;
    // do {
    //   System.out.println("Quel équipement voulez-vous voler ? [indice d'équipement]");
    //   eqt = reader.nextInt();
    // } while (!(eqt <= possibilites.size() && eqt > 0));
    Equipement choixEquipement =
        presenter.selectEquipement("Quel équipement voulez-vous voler ? ", possibilites);
    socketManager.sendToServer(CommandeFactory.choixVol(possibilites.indexOf(choixEquipement)));
  }

  private void interpreterFinPartie() {
    System.out.println("Fin de la partie --> Déconnexion");
    System.exit(0);
  }

  private void interpreterCarteImmediate(Commande commande) {
    if (commande.getArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_EQUIPEMENT) != null) {
      try {
        List<String> equipements =
            (List<String>)
                JsonTools.toObject(
                    commande, Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_EQUIPEMENT, List.class);
        int eqt =
            presenter.selectEquipementString("Choissisez un équipement à donner ?", equipements);
        int choixJoueur =
            presenter.selectPlayer(
                "Choississez un joueur à qui donner l'équipement",
                joueurs.size(),
                joueur,
                false,
                joueursMortsId);
        socketManager.sendToServer(CommandeFactory.carteImmediate(eqt, choixJoueur));
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else if (commande.getArg(Argument.CarteImmediate.CARTE_IMMEDIATE_CHOIX_JOUEUR) != null) {
      int choixJoueur =
          presenter.selectPlayer(
              "Qui voulez vous toucher ?", joueurs.size(), joueur, false, joueursMortsId);
      socketManager.sendToServer(CommandeFactory.carteImmediate((long) choixJoueur));
    }
  }

  public void interpreterUpdateJoueur(Commande commande) {
    try {
      Joueur joueur =
          (Joueur)
              JsonTools.toObject(
                  commande, Argument.UpdateJoueur.UPDATE_JOUEUR_JOUEUR, Joueur.class);
      presenter.majJoueur(joueur);
    } catch (IOException e) {
      System.out.println("Erreur de déjisonification");
    }
  }

  private void interpreterChat(Commande commande) {
    presenter.chat(
        (String) commande.getArg(Argument.Chat.CHAT_MESSAGE),
        (Integer) commande.getArg(Argument.Chat.CHAT_JOUEUR));
  }

  private void interpreterDemarrerPartie() {
    presenter.askStart();
    socketManager.sendToServer(CommandeFactory.demarrerPartie());
  }

  private void interpreterShowCardLt(Commande commande) {

    try {
      Equipement carte =
          (Equipement)
              JsonTools.toObject(
                  commande, Argument.ShowCardLt.SHOW_CARD_LT_CARTE, Equipement.class);
      presenter.showCard(carte);
    } catch (IOException e) {
      System.out.println("Erreur de déjisonification");
    }
  }

  private void interpreterMortJoueur(Commande commande) {
    try {
      Joueur joueur =
          (Joueur) JsonTools.toObject(commande, Argument.MortJoueur.MORT_JOUEUR_ID, Joueur.class);
      joueursMortsId.add(joueur.getIdJoueur());
      this.presenter.mortJoueur(joueur);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public Presenter getPresenter() {
    return presenter;
  }

  public static class AsyncCommand implements Runnable {

    CommandeManager commandeManager;
    Commande commande;

    public AsyncCommand(CommandeManager commandeManager, Commande commande) {
      this.commandeManager = commandeManager;
      this.commande = commande;
    }

    @Override
    public void run() {
      commandeManager.interpreterCommande(commande);
    }
  }
}
