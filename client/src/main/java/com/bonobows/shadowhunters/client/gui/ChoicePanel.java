package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.gui.image.StretchIcon;
import com.bonobows.shadowhunters.client.gui.image.UrlConverter;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ChoicePanel extends AbstractPanel {

  private JPanel container;
  private Box choices;
  private JLabel information;
  private List<JButton> choiceButtons;
  private volatile boolean haveChoosen;
  private int choice;
  private boolean hasPreviousQuestion = false;

  public ChoicePanel(Observer observer) {
    super(observer);
    choiceButtons = new ArrayList<>();
    haveChoosen = false;
  }

  @Override
  public void init() {
    this.setBorder(BorderFactory.createMatteBorder(0, 2, 2, 2, CharteGraphique.jauneContour));
    container = new JPanel();
    container.setBackground(CharteGraphique.noirFonce);
    this.add(container);
    if (hasPreviousQuestion) {
      resetQuestion();
    }
  }

  @Override
  public void draw() {}

  public void changeQuestion(String question, String... choix) {
    if (hasPreviousQuestion) {
      resetQuestion();
    }
    haveChoosen = false;
    choice = -1;
    choices = Box.createHorizontalBox();
    information = new JLabel(question, JLabel.CENTER);
    information.setForeground(CharteGraphique.jauneTexte);
    information.setFont(CharteGraphique.getFont().deriveFont(16f));
    container.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 0;
    container.add(information, c);
    choiceButtons.clear();
    for (String str : choix) {
      System.out.println("choix : " + str);
      System.out.println(UrlConverter.getUrlButton(str));
      if (UrlConverter.isDrawableButton(str)) {
        StretchIcon icon = new StretchIcon(UrlConverter.getUrlButton(str), false);
        JButton button =
            new JButton(
                "<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + "<br><br><br><br><br></html>",
                icon);
        button.setBorder(BorderFactory.createEmptyBorder());
        button.setContentAreaFilled(false);
        button.setPreferredSize(new Dimension(120, 80));

        choiceButtons.add(button);
      } else {
        ImageIcon icon = new StretchIcon(UrlConverter.getUrlButton(str));
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(120, 80, Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        JButton button = new JButton(
            "<html><div style=\"text-align: center\">" + str + "</div></html>",
            icon);
        button.setForeground(new Color(252, 196, 56));
        // button.setFont(button.getFont().deriveFont(Font.BOLD));
        button.setFont(new Font(button.getFont().getFontName(), Font.BOLD,11));
        button.setBorder(BorderFactory.createEmptyBorder());
        button.setContentAreaFilled(false);
        button.setPreferredSize(new Dimension(120, 80));
        button.setVerticalTextPosition(SwingConstants.CENTER);
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        choiceButtons.add(button);
      }
    }
    for (JButton button : choiceButtons) {
      choices.add(button);
      choices.add(Box.createRigidArea(new Dimension(10, 0)));
      button.addActionListener(
          e -> {
            // father.addToChat("Vous avez cliqué sur le bouton : " + choiceButtons.indexOf
            // (button));
            choice = choiceButtons.indexOf(button);
            haveChoosen = true;
            resetQuestion();
          });
    }
    c.gridy = 1;
    container.add(choices, c);
    observable.pack();
    this.hasPreviousQuestion = true;
  }

  private void resetQuestion() {
    haveChoosen = true;
    container.removeAll();
    choices = Box.createHorizontalBox();
    choiceButtons = new ArrayList<>();
    information = new JLabel(" ", JLabel.CENTER);
    container.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 0;
    container.add(information, c);
    choices.add(new JLabel(" ", JLabel.CENTER));
    choices.add(Box.createRigidArea(new Dimension(10, 0)));
    c.gridy = 1;
    container.add(choices, c);
    System.out.println(choiceButtons);
    observable.pack();
  }

  //  public int waitForResponse() {
  //    while (!haveChoosen);
  //    return choice;
  //  }

  public Future<Integer> waitForResponse() throws InterruptedException {
    CompletableFuture<Integer> completableFuture = new CompletableFuture<>();

    Executors.newCachedThreadPool()
        .submit(
            () -> {
              while (!haveChoosen) {}
              haveChoosen = false;
              if (choice == -1) {
                throw new InterruptedException();
              }
              completableFuture.complete(choice);
              return null;
            });

    return completableFuture;
  }
}
