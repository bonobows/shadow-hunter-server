package com.bonobows.shadowhunters.client.gui.component;

import com.bonobows.shadowhunters.client.gui.CharteGraphique;
import com.bonobows.shadowhunters.common.model.Joueur;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

public class Jauge extends JPanel {

  private static final int maxHealth = 14;
  private static final int jaugeSize = 25;
  JPanel jauge;
  Joueur joueur;

  public Jauge(Joueur joueur) {
    super();
    this.joueur = joueur;
  }

  public void init() {
    //    this.setPreferredSize(new Dimension(jaugeSize, this.getHeight()));
    jauge = new JPanel();
  }

  public void draw() {
    // joueur.ajouterBlessures(1);
    this.removeAll();
    this.setBackground(CharteGraphique.noirClair);
    this.setLayout(new GridBagLayout());
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.fill = GridBagConstraints.VERTICAL;
    this.jauge.setBackground(joueur.getCouleur().transformToColor());
    int maxJauge = maxHealth;
    constraints.gridx = 0;
    constraints.gridy = 0;
    constraints.weightx = 1;
    constraints.weighty = 1;
    if ((joueur.isRevele() || joueur.isDead())) {
      if (joueur.getPersonnage().getPv() != 14) {
        constraints.insets = new Insets(0, 0, 0, 0);
        constraints.gridheight = maxHealth - joueur.getPersonnage().getPv();
        // constraints.weighty = 1;
        maxJauge = maxHealth - (maxHealth - joueur.getPersonnage().getPv());
        JPanel transparent = new JPanel();
        this.add(transparent, constraints);
      }
      if (joueur.isDead()) {
        this.jauge.setBackground(Color.gray);
      }
    }
    JPanel nothing = new JPanel();
    nothing.setBackground(CharteGraphique.noirFonce);
    constraints.gridy = maxHealth - maxJauge;
    constraints.gridheight = (maxJauge - joueur.getNbBlessures());
    // constraints.weighty = 1;
    if (joueur.getNbBlessures() > 0) {
      constraints.insets = new Insets(2, 2, 0, 2);
      this.add(nothing, constraints);
      constraints.gridy = maxHealth - maxJauge + constraints.gridheight;
      // constraints.weighty = 1;
      constraints.gridheight = joueur.getNbBlessures();
      constraints.insets = new Insets(0, 2, 2, 2);
      this.add(jauge, constraints);
    } else {
      constraints.insets = new Insets(2, 2, 2, 2);
      this.add(nothing, constraints);
    }

    constraints.gridx++;
    for (int i = 0; i < 14; i++) {
      constraints.gridy = i;
      constraints.gridheight = 1;
      constraints.weighty = 1;
      constraints.insets = new Insets(0, 0, 2, 0);
      JPanel jpanel = new JPanel();
      jpanel.setBackground(CharteGraphique.noirFonce);
      jpanel.setPreferredSize(new Dimension(2, 34));
      this.add(jpanel, constraints);
    }
  }

  public void setJoueur(Joueur joueur) {
    this.joueur = joueur;
  }
}
