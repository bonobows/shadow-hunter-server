package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.model.socket.SocketServer;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class LoginGui extends JDialog {

  private JPanel panel;
  private JTextField tfAddress;
  private JTextField tfPort;
  private JTextField tfUsername;
  private JLabel lbAddress;
  private JLabel lbPort;
  private JLabel lbUsername;
  private JButton btnLogin;
  private SocketServer socket;
  private String pseudo;

  public LoginGui() {

    panel = new JPanel(new GridBagLayout());
    GridBagConstraints cs = new GridBagConstraints();

    cs.fill = GridBagConstraints.HORIZONTAL;


    // Address
    lbAddress = new JLabel("Adresse du serveur : ");
    cs.gridx = 0;
    cs.gridy = 0;
    cs.gridwidth = 1;
    panel.add(lbAddress, cs);

    tfAddress = new JTextField(20);
    cs.gridx = 1;
    cs.gridy = 0;
    cs.gridwidth = 2;
    panel.add(tfAddress, cs);


    // Port
    lbPort = new JLabel("Port du serveur : ");
    cs.gridx = 0;
    cs.gridy = 1;
    cs.gridwidth = 1;
    panel.add(lbPort, cs);

    tfPort = new JTextField(20);
    cs.gridx = 1;
    cs.gridy = 1;
    cs.gridwidth = 2;
    panel.add(tfPort, cs);


    // Username
    lbUsername = new JLabel("Votre nom d'utilisateur : ");
    cs.gridx = 0;
    cs.gridy = 2;
    cs.gridwidth = 1;
    panel.add(lbUsername, cs);

    tfUsername = new JTextField(20);
    cs.gridx = 1;
    cs.gridy = 2;
    cs.gridwidth = 2;
    panel.add(tfUsername, cs);


    // Connection
    btnLogin = new JButton("Connexion");
    cs.gridx = 0;
    cs.gridy = 3;
    cs.gridwidth = 3;
    panel.add(btnLogin, cs);

    btnLogin.addActionListener(e -> {
      if (connection()) {
        MainGui mainGui = new MainGui(socket);
        mainGui.init();
        mainGui.draw();
        mainGui.start(getPseudo());
        dispose();
      }
    });

    panel.setBorder(new LineBorder(Color.GRAY));
    getContentPane().add(panel, BorderLayout.CENTER);
    pack();
    setResizable(false);
    setLocationRelativeTo(null);
  }

  private boolean connection() {
    String errorMessage;
    try {
      String address = tfAddress.getText().trim();
      int port;
      try {
        port = Integer.parseInt(tfPort.getText().trim());
      } catch (NumberFormatException e) {
        port = 2009; // Port par défaut
      }
      socket = new SocketServer(new Socket(address, port));
      // Si le message s'affiche c'est que je suis connecté
      System.out.println("Connexion établie avec le serveur !");
      this.pseudo = tfUsername.getText().trim();
      return true;
    } catch (UnknownHostException e) {
      errorMessage = "Impossible de se connecter à l'adresse :'("
          + socket.getSocket().getLocalAddress();
    } catch (IOException e) {
      errorMessage = "Aucun serveur à l'écoute !";
    } catch (Exception e) {
      errorMessage = "Erreur inconnue !";
    }
    JOptionPane.showMessageDialog(null, errorMessage);
    return false;
  }

  public String getPseudo() {
    return this.pseudo;
  }
}
