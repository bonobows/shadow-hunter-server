package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.gui.component.LieuxComponent;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;

import javax.swing.BorderFactory;
import javax.swing.border.CompoundBorder;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class LieuxPanel extends AbstractPanel {

  Terrain terrain;
  List<LieuxComponent> lieuxComponentList;


  public LieuxPanel(Observer observer) {
    super(observer);

    this.setLayout(new GridLayout(3, 2, 25, 25));
    this.setBorder(
        new CompoundBorder(
            BorderFactory.createMatteBorder(0, 0, 2, 0, CharteGraphique.jauneContour),
            BorderFactory.createEmptyBorder(25, 25, 25, 25)));
    //    this.setMaximumSize(new Dimension(350, 400));
    this.setAlignmentY(Component.TOP_ALIGNMENT);
    this.setBackground(CharteGraphique.noirFonce);

    lieuxComponentList = new ArrayList<>();
  }

  @Override
  public void init() {}

  @Override
  public void draw() {
    this.removeAll();
    lieuxComponentList = new ArrayList<>();
    for (Lieu lieu : terrain.getLieux()) {
      LieuxComponent lieuxComponent = new LieuxComponent(lieu);
      lieuxComponent.init();
      lieuxComponent.draw();
      lieuxComponentList.add(lieuxComponent);
      this.add(lieuxComponent);
    }
    observable.pack();
  }

  public void setTerrain(Terrain terrain) {
    this.terrain = terrain;
    draw();
  }

  public void moveJoueur(Joueur joueur, Lieu lieu) {
    terrain.moveJoueur(joueur, lieu);
    draw();
  }

  public Joueur getJoueur(int i) {
    for (Lieu lieu : terrain.getLieux()) {
      for (Joueur joueur : lieu.getJoueurs()) {
        if (joueur.getIdJoueur() == i) {
          return joueur;
        }
      }
    }
    return null;
  }
}
