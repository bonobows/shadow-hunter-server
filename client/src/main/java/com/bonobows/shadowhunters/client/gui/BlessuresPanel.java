package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.gui.component.Jauge;
import com.bonobows.shadowhunters.common.model.Joueur;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observer;

public class BlessuresPanel extends AbstractPanel {

  private List<Joueur> joueurs;
  private Map<Integer, Jauge> jauges;

  public BlessuresPanel(Observer observer) {
    super(observer);
    this.joueurs = new ArrayList<>();
    this.setLayout(new GridBagLayout());
    init();
  }

  @Override
  public void init() {
    this.setBorder(BorderFactory.createMatteBorder(0, 2, 2, 2, CharteGraphique.jauneContour));
    this.joueurs = new ArrayList<>();
    this.jauges = new HashMap<>();
  }

  public void setJoueurs(List<Joueur> joueurs) {
    this.joueurs = joueurs;
    createJauges();

    draw();
  }

  private void createJauges() {
    jauges.clear();
    this.removeAll();

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;

    c.insets = new Insets(20, 2, 20, 2);
    c.weighty = 1;
    c.gridheight = 15;

    for (Joueur joueur : joueurs) {
      Jauge jauge = new Jauge(joueur);
      jauge.init();
      this.add(jauge, c);
      c.gridx++;
      jauges.put(joueur.getIdJoueur(), jauge);
    }

    c.gridy = 0;
    c.gridheight = 1;
    c.insets = new Insets(10, 2, 10, 2);
    for (int i = 14; i >= 0; i--) {
      JLabel blessureLabel = new JLabel(Integer.toString(i));
      blessureLabel.setForeground(CharteGraphique.jauneTexte);
      this.add(blessureLabel, c);
      c.gridy++;
    }
  }

  public void majJoueur(Joueur newJoueur) {
    boolean isNewJoueur = true;
    for (Joueur joueur : joueurs) {
      if (joueur.getPersonnage().getNom().equals(newJoueur.getPersonnage().getNom())) {
        isNewJoueur = false;
        this.joueurs.set(this.joueurs.indexOf(joueur), newJoueur);
      }
    }
    if (isNewJoueur) {
      this.joueurs.add(newJoueur);
      createJauges();
    }
    draw();
  }

  @Override
  public void draw() {
    for (Joueur j : this.joueurs) {
      Jauge jauge = jauges.get(j.getIdJoueur());
      if (jauge != null) {
        jauge.setJoueur(j);
        jauge.draw();
      }
    }


    observable.pack();
  }
}
