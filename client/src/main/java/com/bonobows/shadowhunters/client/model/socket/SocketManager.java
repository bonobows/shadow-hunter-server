package com.bonobows.shadowhunters.client.model.socket;

import com.bonobows.shadowhunters.client.model.command.CommandeManager;
import com.bonobows.shadowhunters.common.model.commande.Commande;

import java.io.IOException;
import java.io.InputStream;

/** Created by Milihhard on 04/10/2017. */
public class SocketManager {

  private SocketServer socket;
  private CommandeManager commandeManager;

  public SocketManager(SocketServer socket) {
    this.socket = socket;
    this.commandeManager = commandeManager;
  }

  public void sendToServer(Commande commande) {
    socket.sendMessage(commande);
  }

  public void receiveMessage(String message) {
    try {
      Commande commande = Commande.jsonToCommand(message);
      // if (CodeCommande.CHAT.equals(commande.getCode())) {
      Thread t = new Thread(new CommandeManager.AsyncCommand(this.commandeManager, commande));
      t.start();
      // } else {
      // this.commandeManager.interpreterCommande();
      // }
    } catch (IOException e) {
      e.printStackTrace();
      System.out.println("Le serveur a fait de la merde");
    }
  }

  public InputStream getInputStream() {
    try {
      return this.socket.getSocket().getInputStream();
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  public void close() throws IOException {
    socket.getSocket().close();
  }

  public CommandeManager getCommandeManager() {
    return commandeManager;
  }

  public void addCommandeManager(CommandeManager commandeManager) {
    this.commandeManager = commandeManager;
  }
}
