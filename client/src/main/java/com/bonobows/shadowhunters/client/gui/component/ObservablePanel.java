package com.bonobows.shadowhunters.client.gui.component;

import java.util.Observable;
import java.util.Observer;

public class ObservablePanel extends Observable {
  public ObservablePanel(Observer observer) {
    addObserver(observer);
  }

  public void notif(Object arg) {
    setChanged();
    notifyObservers(arg);
  }

  public void pack() {
    setChanged();
    notifyObservers("pack");
  }
}
