package com.bonobows.shadowhunters.client;

import com.bonobows.shadowhunters.client.gui.LoginGui;
import com.bonobows.shadowhunters.client.model.socket.SocketServer;
// import sun.audio.AudioPlayer;
// import sun.audio.AudioStream;

public class ClientGui {

  static SocketServer socket;

  public static void main(String[] args) {
    boolean msgDejaAfficher = false;

    // On lance la musique de fond
    // playSound();

    LoginGui loginGui = new LoginGui();
    loginGui.setVisible(true);
  }

  // private static void playSound() {
  //   try {
  //     AudioStream as =
  //         new AudioStream(
  //             Thread.currentThread()
  //                 .getContextClassLoader()
  //                 .getResourceAsStream("sound/musique.wav"));
  //     AudioPlayer.player.start(as);
  //   } catch (Exception e) {
  //     // a special way i'm handling logging in this application
  //     System.out.println(e);
  //   }
  // }
}
