package com.bonobows.shadowhunters.client.gui;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class ChatPanel extends AbstractPanel {

  private JTextArea textPane = new JTextArea();
  private JScrollPane scroll = new JScrollPane(textPane);
  private JTextField jtf = new JTextField("Entrer le texte ici");
  private JButton sendButton = new JButton("Envoyer");

  public ChatPanel(Observer observer) {
    super(observer);
    // style chat
    textPane.setBackground(CharteGraphique.noirFonce);
    textPane.setForeground(CharteGraphique.jauneTexte);
    textPane.setFont(CharteGraphique.getFont().deriveFont(16f));
  }

  public void addText(String str) {
    textPane.append(str + "\n");
    JScrollBar vertical = scroll.getVerticalScrollBar();
    vertical.setValue(vertical.getMaximum());
  }

  public void chatting() {
    this.addText("Vous : " + jtf.getText());
    List<String> argNotify = new ArrayList<>();
    argNotify.add("chat");
    argNotify.add(jtf.getText());
    observable.notif(argNotify);
    System.out.println("je chat (notif)" + observable.countObservers());
  }

  public void showPlaceholder() {
    jtf.setForeground(Color.GRAY);
    jtf.setText("Entrer le texte ici");
  }

  public void clearInput() {
    jtf.setText("");
    jtf.setForeground(Color.BLACK);
  }

  @Override
  public void init() {}

  @Override
  public void draw() {
    this.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.black));
    this.setLayout(new GridBagLayout());
    this.setPreferredSize(new Dimension(250, 500));
    GridBagConstraints c = new GridBagConstraints();
    //    this.setAlignmentY(Component.TOP_ALIGNMENT);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    c.weighty = 0.99;
    this.add(scroll, c);
    c.gridy++;
    c.weighty = 0.005;
    this.add(jtf, c);
    c.gridy++;
    this.add(sendButton, c);
    textPane.setEditable(false);
    sendButton.addActionListener(
        e -> {
          if (!jtf.getText().isEmpty() && !"Entrer le texte ici".equals(jtf.getText())) {
            chatting();
            showPlaceholder();
          }
        });
    jtf.setForeground(Color.GRAY);
    jtf.addFocusListener(
        new FocusListener() {
          @Override
          public void focusGained(FocusEvent e) {
            if ("Entrer le texte ici".equals(jtf.getText())) {
              clearInput();
            }
          }

          @Override
          public void focusLost(FocusEvent e) {
            if (jtf.getText().isEmpty()) {
              showPlaceholder();
            }
          }
        });
    jtf.addActionListener(
        e -> {
          if (!jtf.getText().isEmpty() && !"Entrer le texte ici".equals(jtf.getText())) {
            chatting();
            clearInput();
          }
        });
    //    this.setMaximumSize(new Dimension(450, 400));
    //    scroll.setAlignmentX(Component.LEFT_ALIGNMENT);
    //    jtf.setMaximumSize(new Dimension(450, 20));
    //    jtf.setAlignmentX(Component.LEFT_ALIGNMENT);
    //    sendButton.setAlignmentX(Component.LEFT_ALIGNMENT);
  }
}
