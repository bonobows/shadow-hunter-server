package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.controller.Presenter;
import com.bonobows.shadowhunters.client.controller.PresenterGui;
import com.bonobows.shadowhunters.client.model.command.CommandeManager;
import com.bonobows.shadowhunters.client.model.socket.SocketManager;
import com.bonobows.shadowhunters.client.model.socket.SocketServer;
import com.bonobows.shadowhunters.client.tool.ReceiveMessage;
import com.bonobows.shadowhunters.common.factory.CommandeFactory;
import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class MainGui extends JFrame implements Observer {

  private JPanel mainPanel;
  private JoueursPanel joueursPanel;
  private ChoicePanel choicePanel;
  private ChatPanel chatPanel;
  private SocketServer socketServer;
  private LieuxPanel lieuxPanel;
  private BlessuresPanel blessuresPanel;
  private InventairePanel inventairePanel;
  private int joueurId;

  private SocketManager socketManager;

  public MainGui(SocketServer socketServer) {
    this.socketServer = socketServer;
  }

  public void init() {
    initFrame();
    initComponent();
  }

  public void start(String pseudo) {
    System.out.println("Vous pouvez écrire des messages ici ");
    Thread t = null;
    socketManager = new SocketManager(socketServer);
    Presenter presenter = new PresenterGui(this);
    CommandeManager commandeManager = new CommandeManager(socketManager, presenter, pseudo);
    socketManager.addCommandeManager(commandeManager);
    t = new Thread(new ReceiveMessage(socketManager));
    t.start();

    socketManager.sendToServer(CommandeFactory.sePresenter(pseudo));
  }

  private void initFrame() {
    Frame frame = this;
    this.setSize(new Dimension(1300, 700));
    this.setTitle("Shadow Hunter by Bonobows");
    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    this.setResizable(false);
    this.setLocationByPlatform(true);
  }

  private void initComponent() {

    mainPanel = new JPanel();
    mainPanel.setLayout(new GridBagLayout());
    this.setContentPane(mainPanel);
    GridBagConstraints c = new GridBagConstraints();

    joueursPanel = new JoueursPanel(this);
    joueursPanel.init();
    c.gridx = 0;
    c.gridy = 0;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1;
    c.gridwidth = 4;
    mainPanel.add(joueursPanel, c);

    c.gridy++;
    c.weightx = 0.05;
    c.gridwidth = 1;
    blessuresPanel = new BlessuresPanel(this);
    blessuresPanel.init();
    mainPanel.add(blessuresPanel, c);
    c.gridx++;
    c.weightx = 0.45;
    lieuxPanel = new LieuxPanel(this);
    mainPanel.add(lieuxPanel, c);
    c.gridx++;
    inventairePanel = new InventairePanel(this);
    mainPanel.add(inventairePanel, c);
    inventairePanel.draw();

    c.gridx++;
    c.weightx = 0.15;
    this.chatPanel = new ChatPanel(this);
    chatPanel.init();
    mainPanel.add(chatPanel, c);
    chatPanel.init();

    choicePanel = new ChoicePanel(this);
    choicePanel.init();
    c.gridx = 0;
    c.gridy++;
    c.weightx = 1;
    c.gridwidth = 4;
    mainPanel.add(choicePanel, c);
  }

  public void draw() {

    joueursPanel.draw();
    chatPanel.draw();
    this.pack();
    this.setVisible(true);
  }

  public void updateNameWindow(int i) {
    this.joueurId = i;
    this.setTitle("Shadow Hunter by Bonobows (Joueur " + i + ")");
  }

  public void addToChat(String str) {
    chatPanel.addText(str);
  }

  public void updateTerrain(Terrain terrain) {
    lieuxPanel.setTerrain(terrain);
    this.pack();
  }

  public void moveJoueur(Joueur joueur, Lieu lieu) {
    lieuxPanel.moveJoueur(joueur, lieu);
  }

  /**
   * change la question et attend que le joueur clique sur un choix.
   *
   * @param question question a afficher
   * @param choix les différents choix pour la question
   * @return l'indice du bouton cliqué (et donc du paramètre choix) (commence à 0, comme un tableau)
   */
  public int newQuestion(String question, String... choix) {
    choicePanel.changeQuestion(question, choix);
    try {
      Future<Integer> completableFuture = choicePanel.waitForResponse();
      return completableFuture.get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
      return -1;
    }
  }

  public int newQuestion(String question, List<String> choix) {
    return newQuestion(question, choix.toArray(new String[choix.size()]));
  }

  public void chatting(String message) {
    socketManager.sendToServer(CommandeFactory.chat(message));
  }

  public void initJoueurs(List<Joueur> joueurs) {
    joueursPanel.setJoueurs(joueurs, this.joueurId);
    blessuresPanel.setJoueurs(joueurs);
    this.pack();
  }

  public void majJoueur(Joueur joueur) {
    joueursPanel.majJoueur(joueur, this.joueurId);
    blessuresPanel.majJoueur(joueur);
    this.pack();
  }

  public void newTurn(int joueur) {
    joueursPanel.setJoueurCourrant(joueur);
    // inventairePanel.removeCarte();
  }

  @Override
  public void update(Observable o, Object arg) {
    if (arg.getClass().equals(String.class)) {
      switch ((String) arg) {
        case "pack":
          this.pack();
          break;
        default:
          break;
      }
    } else if (arg.getClass().equals(ArrayList.class)) {
      List<String> args = (List<String>) arg;
      switch (args.get(0)) {
        case "chat":
          chatting(args.get(1));
          break;
        default:
          break;
      }
    }
  }

  public void showCard(Carte carte) {
    System.out.println("--->>>Carte" + carte);
    inventairePanel.changeCarte(carte);
    inventairePanel.draw();
  }

  public void showVisionPersonnage(Personnage personnage) {
    inventairePanel.visionSupreme(personnage);
    inventairePanel.draw();
  }

  public void mortJoueur(Joueur joueur) {
    lieuxPanel.terrain.removeDeadJoueur(joueur);
    lieuxPanel.draw();
  }
}
