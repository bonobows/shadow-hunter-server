package com.bonobows.shadowhunters.client.gui.component;

import com.bonobows.shadowhunters.client.gui.CharteGraphique;
import com.bonobows.shadowhunters.client.gui.image.StretchIcon;
import com.bonobows.shadowhunters.client.gui.image.UrlConverter;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LieuxComponent extends JPanel {

  JLabel fond;
  private Lieu lieu;
  private Color backgroundColor;
  private Color labelColor;

  public LieuxComponent(Lieu lieu) {
    this.lieu = lieu;
  }

  public void init() {
    // this.setBackground(Color.magenta);
    initColor();
    //    this.setPreferredSize(new Dimension(50, 50));
    this.setLayout(new BorderLayout());
    StretchIcon icon = new StretchIcon(UrlConverter.lieuToUrl(lieu.getNom()).getFile());
    fond = new JLabel(icon, JLabel.CENTER);
    fond.setLayout(new GridLayout(0, 4, 5, 5));
    this.setBackground(CharteGraphique.noirFonce);
    setToolTipText(lieu.getDescription());
    // fond.setPreferredSize(new Dimension(150, 130));
    // fond.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    // this.setPreferredSize(new Dimension(150, 130));
    this.add(fond);
  }

  private void initColor() {
    this.labelColor = Color.black;
    switch (lieu.getNom()) {
      case CIMETIERE:
        this.backgroundColor = Color.darkGray;
        this.labelColor = Color.white;
        break;
      case MONASTERE:
        this.backgroundColor = Color.white;
        break;
      case ANTRE_HERMITE:
        this.backgroundColor = Color.green;
        break;
      case FORET:
        this.backgroundColor = Color.gray;
        this.labelColor = Color.white;
        break;
      case SANCTUAIRE:
        this.backgroundColor = Color.orange;
        break;
      case PORTE_OUTREMONDE:
        this.backgroundColor = Color.magenta;
        break;
      default:
    }
  }

  public void draw() {
    // JLabel label = new JLabel(lieu.getNom().toString());
    // label.setBackground(backgroundColor);
    // label.setForeground(labelColor);
    // label.setOpaque(true);
    fond.removeAll();
    // fond.add(label, BorderLayout.NORTH);
    // JPanel posJoueur = new JPanel();
    // posJoueur.setLayout(new GridLayout(2, 4, 5, 5));
    // posJoueur.setPreferredSize(new Dimension(260,120));
    // fond.add(posJoueur, BorderLayout.CENTER);
    for (int i = 0; i < 4; i++) {
      JPanel squarePlayer = new JPanel();
      //      squarePlayer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
      //      squarePlayer.setBackground(Color.blue);
      squarePlayer.setBackground(new Color(0, 0, 0, 0));
      fond.add(squarePlayer);
    }
    for (Joueur joueur : lieu.getJoueurs()) {
      StretchIcon icon = new StretchIcon(UrlConverter.pionToUrl(joueur.getCouleur()).getFile());
      JLabel image = new JLabel(icon, JLabel.CENTER);
      JPanel squarePlayer = new JPanel();
      // image.setPreferredSize(new Dimension(25, 25));
      squarePlayer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
      squarePlayer.setBackground(joueur.getCouleur().transformToColor());
      fond.add(image);
    }

    for (int i = 0; i < 8 - lieu.getJoueurs().size(); i++) {
      JPanel squarePlayer = new JPanel();
      //      squarePlayer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
      //      squarePlayer.setBackground(Color.blue);
      squarePlayer.setBackground(new Color(0, 0, 0, 0));
      fond.add(squarePlayer);
    }
  }

  public List<Integer> getPlayer() {

    List<Integer> list = new ArrayList<>();
    for (Joueur j : lieu.getJoueurs()) {
      list.add(j.getIdJoueur());
    }
    list.sort(Comparator.comparingInt(o -> o));
    return list;
  }
}
