package com.bonobows.shadowhunters.client.controller;

import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.commande.LogLevel;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;

import java.util.List;

public interface Presenter {

  void showLog(LogLevel logLevel, String log);

  void newTurn(int turn);

  void updateJoueurNumber(int i);

  void updateTerrain(Terrain terrain);

  void moveJoueur(Joueur joueur, Lieu lieu);

  void askForDice();

  void initJoueurs(List<Joueur> joueurs);

  void majJoueur(Joueur joueur);

  void chat(String message, int joueurChat);

  String askForRevele();

  boolean askForQuestionYesNo(String question);

  String askForQuestionYesNoString(String question);

  int selectPlayer(String question, List<Joueur> joueurPorte);

  int selectPlayer(
      String question, int nbJoueur, int joueur, boolean chooseHimSelf, List<Integer> joueursMorts);

  int selectPlayerInt(String question, List<Integer> joueurPorte);

  char askCapaciteEmi();

  int askBoussole(int choix1, int choix2);

  NomLieu askLieuChoix(Terrain terrain);

  int choixPorteOutremonde();

  String choixForetHante();

  ActionVision effetVision(List<ActionVision> actionVisionList);

  Equipement selectEquipement(String question, List<Equipement> equipements);

  int selectEquipementString(String question, List<String> equipements);

  boolean askStart();

  void showCard(Carte carte);

  void clearCard();

  void showPersonnageVision(Personnage personnage);

  void mortJoueur(Joueur joueur);
}
