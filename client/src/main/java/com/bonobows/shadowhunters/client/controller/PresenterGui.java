package com.bonobows.shadowhunters.client.controller;

import com.bonobows.shadowhunters.client.gui.MainGui;
import com.bonobows.shadowhunters.common.model.ActionVision;
import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Equipement;
import com.bonobows.shadowhunters.common.model.Joueur;
import com.bonobows.shadowhunters.common.model.Personnage;
import com.bonobows.shadowhunters.common.model.commande.LogLevel;
import com.bonobows.shadowhunters.common.model.lieux.Lieu;
import com.bonobows.shadowhunters.common.model.lieux.NomLieu;
import com.bonobows.shadowhunters.common.model.lieux.Terrain;

import java.util.ArrayList;
import java.util.List;

public class PresenterGui extends AbstractPresenter {

  MainGui mainGui;

  public PresenterGui(MainGui mainGui) {
    this.mainGui = mainGui;
  }

  @Override
  public void updateJoueurNumber(int i) {
    super.updateJoueurNumber(i);
    mainGui.updateNameWindow(i);
    mainGui.addToChat("Vous êtes le joueur " + i);
    System.out.println("Vous êtes le joueur " + i);

  }

  @Override
  public void showLog(LogLevel logLevel, String log) {
    mainGui.addToChat(logLevel.toString() + " : " + log);
  }

  @Override
  public void updateTerrain(Terrain terrain) {
    mainGui.updateTerrain(terrain);
  }

  @Override
  public void moveJoueur(Joueur joueur, Lieu lieu) {
    mainGui.moveJoueur(joueur, lieu);
  }

  @Override
  public void askForDice() {
    mainGui.newQuestion("Vous devez lancer les dés", "Lancer les dés");
  }

  @Override
  public String askForRevele() {
    int choice = mainGui.newQuestion("Voulez vous vous révéler ?", "Oui", "Non");
    return choice == 0 ? "o" : "n";
  }

  @Override
  public boolean askForQuestionYesNo(String question) {
    int choice = mainGui.newQuestion(question, "Oui", "Non");
    return choice == 0;
  }

  @Override
  public String askForQuestionYesNoString(String question) {
    return askForQuestionYesNo(question) ? "o" : "n";
  }

  @Override
  public int selectPlayer(String question, List<Joueur> joueurPorte) {
    List<String> reponse = new ArrayList<>();
    for (Joueur j : joueurPorte) {
      reponse.add("Joueur " + j.getIdJoueur());
    }
    int choice = mainGui.newQuestion(question, reponse);
    return joueurPorte.get(choice).getIdJoueur();
  }

  @Override
  public int selectPlayer(String question, int nbJoueur, int joueur, boolean chooseHimSelf,
                          List<Integer> joueursMorts) {
    List<String> reponses = new ArrayList<>();
    List<Integer> reponsesJoueur = new ArrayList<>();
    for (int i = 1; i <= nbJoueur; i++) {
      if ((i != joueur || chooseHimSelf) && !joueursMorts.contains(i)) {
        reponses.add("Joueur " + i);
        reponsesJoueur.add(i);
      }
    }
    int choix = mainGui.newQuestion(question, reponses) + 1;
    return reponsesJoueur.get(choix - 1);
  }

  @Override
  public int selectPlayerInt(String question, List<Integer> joueurPorte) {
    List<String> reponse = new ArrayList<>();
    for (Integer j : joueurPorte) {
      reponse.add("Joueur " + j);
    }
    int choice = mainGui.newQuestion(question, reponse);
    return joueurPorte.get(choice);
  }

  @Override
  public char askCapaciteEmi() {
    int choix =
        mainGui.newQuestion(
            "Voulez-vous vous déplacer sur le lieu adjacent ou lancer les dés ?",
            "Lieu adjacent",
            "lancer les dés");
    return choix == 0 ? 'a' : 'd';
  }

  @Override
  public int askBoussole(int choix1, int choix2) {
    int choix =
        mainGui.newQuestion(
            "Choisissez entre les deux résultats",
            Integer.toString(choix1),
            Integer.toString(choix2));
    return choix == 0 ? choix1 : choix2;
  }

  @Override
  public NomLieu askLieuChoix(Terrain terrain) {
    List<String> reponses = new ArrayList<>();
    for (Lieu lieu : terrain.getLieux()) {
      reponses.add(lieu.getNom().toString());
    }
    int choix = mainGui.newQuestion("Choisissez votre lieux", reponses);
    return terrain.getLieux().get(choix).getNom();
  }

  @Override
  public int choixPorteOutremonde() {
    return mainGui.newQuestion(
        "Quelle type de carte voulez vous piochez ?", "Aucune", "Vision", "Lumière", "Ténèbre");
  }

  @Override
  public String choixForetHante() {
    int choix =
        mainGui.newQuestion(
            "Voulez-vous faire subir 2 Blessures, soigner 1 Blessure ou " + "ne rien faire ?",
            "Dégats",
            "Soin",
            "Rien");
    return choix == 0 ? "d" : choix == 1 ? "s" : "n";
  }

  @Override
  public ActionVision effetVision(List<ActionVision> actionVisionList) {
    List<String> reponses = new ArrayList<>();
    for (ActionVision actionVision : actionVisionList) {
      reponses.add(actionVision.toString());
    }
    int choix = mainGui.newQuestion("Choissisez un action", reponses);
    return actionVisionList.get(choix);
  }

  @Override
  public Equipement selectEquipement(String question, List<Equipement> equipements) {
    List<String> reponses = new ArrayList<>();
    for (Equipement equipement : equipements) {
      reponses.add(equipement.getNom());
    }
    int choix = mainGui.newQuestion(question, reponses);
    return equipements.get(choix);
  }

  @Override
  public int selectEquipementString(String question, List<String> equipements) {
    return mainGui.newQuestion(question, equipements) + 1;
  }

  @Override
  public void showCard(Carte carte) {
    this.mainGui.showCard(carte);
  }

  @Override
  public void clearCard() {
    this.mainGui.showCard(null);
  }

  @Override
  public void showPersonnageVision(Personnage personnage) {
    mainGui.showVisionPersonnage(personnage);
  }

  @Override
  public void mortJoueur(Joueur joueur) {
    this.mainGui.mortJoueur(joueur);
  }

  @Override
  public boolean askStart() {
    int choice = mainGui.newQuestion("Voulez-vous commencer la partie  maintenant ?", "Oui");

    return choice == 0;
  }

  @Override
  public void initJoueurs(List<Joueur> joueurs) {
    mainGui.initJoueurs(joueurs);
  }

  @Override
  public void majJoueur(Joueur joueur) {
    mainGui.majJoueur(joueur);
  }

  @Override
  public void chat(String message, int joueurChat) {
    mainGui.addToChat("Joueur " + joueurChat + " : " + message);
  }

  @Override
  public void newTurn(int turn) {
    mainGui.newTurn(turn);
  }


}
