package com.bonobows.shadowhunters.client.gui;

import com.bonobows.shadowhunters.client.gui.image.StretchIcon;
import com.bonobows.shadowhunters.client.gui.image.UrlConverter;
import com.bonobows.shadowhunters.common.model.Carte;
import com.bonobows.shadowhunters.common.model.Personnage;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observer;

public class InventairePanel extends AbstractPanel {

  private Carte carte;
  private Personnage personnage;
  private boolean isVisionSupreme;

  public InventairePanel(Observer observer) {
    super(observer);
    this.setLayout(new BorderLayout());
    this.isVisionSupreme = false;
  }

  @Override
  public void init() {}

  @Override
  public void draw() {
    this.setBorder(BorderFactory.createMatteBorder(0, 2, 2, 2, CharteGraphique.jauneContour));

    this.removeAll();
    //    this.setMaximumSize(new Dimension(300, 400));
    this.add(new JLabel("Zone d'inventaire"), BorderLayout.NORTH);
    this.setAlignmentY(Component.TOP_ALIGNMENT);
    boolean cardNotNull = false;
    StretchIcon icon = null;
    if (carte != null) {
      cardNotNull = true;
      icon = new StretchIcon(UrlConverter.carteToUrl(carte.getNom(), carte.getType()));
    } else if (personnage != null) {
      cardNotNull = true;
      icon = new StretchIcon(UrlConverter.personnageCardToUrl(personnage.getNom()));
    }
    if (cardNotNull) {
      JLabel image = new JLabel(icon, JLabel.CENTER);
      image.setPreferredSize(new Dimension(100, 200));
      image.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, CharteGraphique.jauneContour));
      this.add(image, BorderLayout.CENTER);
      InventairePanel self = this;
      image.addMouseListener(
          new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
              super.mouseReleased(e);
              self.remove(image);
              self.observable.pack();
              self.removeCarte();
            }
          });
    }
    observable.pack();
  }

  public void changeCarte(Carte carte) {
    removeCarte();
    this.carte = carte;
  }

  public void visionSupreme(Personnage personnage) {
    removeCarte();
    this.personnage = personnage;
    this.isVisionSupreme = true;
    System.out.println("la vision supreme");
  }

  public void removeCarte() {
    carte = null;
    personnage = null;
    this.isVisionSupreme = false;
    draw();
  }
}
