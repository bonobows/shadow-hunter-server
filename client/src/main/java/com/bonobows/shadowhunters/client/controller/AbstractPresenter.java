package com.bonobows.shadowhunters.client.controller;


public abstract class AbstractPresenter implements Presenter {
  protected int joueur;

  @Override
  public void updateJoueurNumber(int i) {
    joueur = i;
  }

  @Override
  public void clearCard() {

  }
}
